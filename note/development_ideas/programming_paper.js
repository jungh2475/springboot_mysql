/**
 * vartxt programming paper
 * 단순히 문서안에 코딩을 해서 동적인 문서를 만들어 내는 법 : angular-directive+templating
   
   사용
   <a href='' onClick='javascript function x...return false'>
   	<vartxt verb='vu' value='vt("kewtea.com/api/1").upper()'>Jungh Lee</vartxt> </a>
   
   vartxt sub-object_x command_y object =>이렇게 단순히 적으면 문서편집기에서 위처럼 적어 준다 
   
   [유용한 함수들] 
   http.get/post/put/delete(url,jsonObject), .head(user='',password='') or (auth="./secret.json) => 
   .xpath('pattern'), [anyString].cut("tag_pattern",1,-)
   page.place('url or file'), page.place('.........')
   excel.....(tag_name). : excel의 모든 function을 사용함 
   insert html markup as macro : <vtxt import('url')> 헤더부분 </vtxt> => load js/css/contentHtml dynamiccally
   
   [문서 구조화 빠르게 하기]
   (Emmet of Bracket에서 영감받음 아니면 twitter_bootstrap방식으로)
   	#page>div.logo+ul#navigation>li*5>a{Item $}
   	div#id, div.class >(lower dimension), +(sibling) 
   	div[ div#page1.classx + div#page2.classx[div.y+div.y+div.y] ]
    div[ div#page1.classx + div#page2.classx[div.y*3] ]
    
    div[
    row > col + col + col
    row > col + col 
    ]
    
    
    사용례
    오늘의 방문자수는 <a href='abc.com/pagex'><vtx verb='http.head(auth='secret.json').get($company_api)'>10명</vtx></a>입니다. 
    오늘의 매출은 $<vtx>100</vtx>입니다.
    오늘 연락할 사람들 전화번호와 이메일 주소는 다음과 같습니다.
    <vtx table(list)....>
    
   vgit add-push . 'message' (all-> to repo-branch), vgit pull from(repo-branch), vgit status, vgit checkout @tag or branch 
     
    
 */

function addScript(type,url){  //html, css, js
	var headObject=document.getElementByTagName("head")[0];
	var jsObject = document.createElement('script');
	//jsObject.setAttribute() // txt, js, src=url
	headObject.appendChild(jsObject);
}


