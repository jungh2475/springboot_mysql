please check this note for initial setup
##################

	1.intro & references
	2.install (git & backup)
	3.run & test with maven
	4.db setup
	5.html/js/css & their test
	

##################



##################

1. intro & references

	- 이 코드는 kewtea의 company landing_pages, journal, spot2, urban(app), kewrator_v2, (kewChloe)를 포함함 
	- webPage와 rest_api를 가지고 있고 mysql에 연동되어져 동작함. (java 8, mysql 5.7, ubuntu16 기반) 
	- references
		1. Kew DB & API Server: https://docs.google.com/a/kewtea.com/document/d/1HeD5vsZYW8QwruNx7hAI3l1QZxR2XKF8WCgqsDLziF0/edit?usp=sharing
		2. excel to-do-list: https://docs.google.com/a/kewtea.com/spreadsheets/d/1d14PdghxFNK3RfgF0tIMHV80Mc-VpwCMdlvxj4eb9VM/edit?usp=sharing
		3.sys config: https://drive.google.com/open?id=1h5vxC7JpiONK-MLkJ_e04zGEPMztngJPsNqpLIgfSdQ
	
##################

2. install (git & backup)

	- use this script: 
	- git: 
		production: https://jungh2475@bitbucket.org/kewtea_sangsoo/kewtea-spring.git
	- backup is at gcloud: diskId: gStorage:mysql

##################

3. run & test with maven

	- 실행 명령어 예시들 
		mvn clean install
		mvn spring-boot:run
		localhost:8080/resources
		mvn clean install or mvn package (pom.xml에서 파일명을 바꾸어 주던가, linux로 compile된것의 파일명을 바꾸어 주자, kew3-1.0.0 -> kew3-1.16.0607a)
		java -jar target/kew3-1.0.0-SNAPSHOT.jar --server.port=8080 &

	- 체크 및 주의 사항들 
		개발중에는 db를 가까로 대치해서 매번 새로 쓰고 지원고 하게 한다. 
		JPA에서 다 자동으로 세팅하나, mysql 언어만 manual로 직접 로그인 하여 조정해 준다 
		ALTER DATABASE mydatabasename charset=utf8;
		컴파일 혹은 런에서 에러가 날때의 증상/해결법: some compile error - check file ownership 
		

sql db를 copy하고 변형하는 명령어들 
schema.sql
create table kewnote (name VARCHAR(100) NOT NULL PRIMARY KEY, value VARCHAR(255));
insert into kewnote (name, value) values ('hello', 'iam hello’);
INSERT INTO new_database.table1 (id, product, is_default) 
SELECT id, product, IF(is_default='Y',1,0) as isdefault FROM old_database.table1
CREATE TABLE SALARY AS SELECT ID, SALARY FROM CUSTOMERS;
only to create a new empty table with the same column structure, add a WHERE clause with a falsy expression:
SELECT * INTO ABC_1 FROM ABC WHERE 1 <> 1;
SELECT * INTO ABC_1 FROM ABC;  //ABC table copy to ABC_1






data extraction using gSheet
https://docs.google.com/spreadsheets/d/1q5oqSSInTD1fY1yk3EoWizQ_etEeVn9G9IkRhj_a8xU/edit?usp=sharing

[gcloud SSH]
console.cloud.google.com
gcloud compute ssh --zone "us-central1-a" "jungh-instance-1"
ssh(pass_phrase:s101100) log in (web or console)
change user name : whoami, sudo su - jungh_lee, gcloud auth list
gcloud sdk install - gcloud init, adding path?(source .bash_profile : source '/usr/local/gcloud/google-cloud-sdk/path.bash.inc'
)
check process & kill : ps -u user_name, sudo kill, ps aux | grep java
instance default : us central-1a, (146.148.32.67(10.240.0.2)

[MySql]
create mysql database:CREATE DATABASE `kew1db` CHARACTER SET utf8 COLLATE utf8_general_ci;
dump(backup) only kew1db database ->table_name:
mysqldump db_name table_name > table_name.sql
sql file name: kew1db_{date}_gcloud.sql, kew1db_{date}_osx.sql
restore: mysql -u username -p1234 db_name < /path/to/table_name.sql
mysql -u root -p1234 < custom_db_only2016.sql
sudo service mysql restart


[Git & Maven]
mkdir & git init
git remote add origin https://jungh2475@bitbucket.org/jungh2475/springboot_mysql.git (or git@bitbucket.org:jungh2475/springboot_mysql.git)

git pull origin master



[Testing Apis]
/resources/
kewtea/

[gCloud Java & mysql install]
sudo apt-get update, sudo apt-get install openjdk-7-jdk,vim….source /etc/environment,JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64/bin”,echo $JAVA_HOME,sudo apt-get install maven,sudo apt-get install git-all,sudo apt-get install mysql-server,
add sftp connection with filezilla

