##################

	1.intro & references
	2.install (git & backup)
	3.[development] run & test with maven
	4.db setup
	5.websrc- html/js/css & their test
	6.[production] apache and gcloud
	

##################



##################

1. intro & references

	- 하기의 서비스들을 담고 있음: journal(+crx), kewtea, (narrative impressions-naim, urban, kewratorNew,..)
	- 전체 시스템 구성: https://drive.google.com/open?id=1S5Ca5I_RAx1xMQ6xWpdVXPFvQWH2kI7GVv98CpSGjqA
	- profiles: dev-local, dev-svr, prod-st1, prod-st2 (/resources/application.properties)

##################

2. install (git & backup)
	- requirements: java 8, mysql 5.7, , hdd/ram/cpu-class
	- git: bitbucket.org/kewtea_sangsoo/kewtea-spring.git, https://jungh2475@bitbucket.org/jungh2475/springboot_mysql.git
	- install script: kew-images-docker / scripts / kew-vm-install-spring-mysql-elk-2016.sh => 이것은 spring-cloud-registry로 변환 예정 
	- gStorage: bucketName

##################

3. [development] run & test with maven

	- select development profile
	- mvn commands

##################

4.db setup

	- mysql remote:
	- 절대로 production id/password는 commit하지 말것 
	
##################

5.websrc- html/js/css & their test

	- node, npm, webpack(bundle.js)
	- jasmine, selenium, eslint


##################

6.[production] apache and gcloud

	- dns : 
	- apache revserse proxy, loadbalancer, ssl
	- logging, filebeat
	- (checklist)