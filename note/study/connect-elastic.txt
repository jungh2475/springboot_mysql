source: http://www.baeldung.com/spring-data-elasticsearch-tutorial
https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples/spring-boot-sample-data-elasticsearch
https://www.javacodegeeks.com/2015/03/head-first-elastic-search-on-java-with-spring-boot-and-data-features.html
http://aoruqjfu.fun25.co.kr/index.php/post/792   //using ElasticsearchTemplate
https://examples.javacodegeeks.com/enterprise-java/spring/spring-data-elasticsearch-example/

1.maven

<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-elasticsearch</artifactId>
    <version>2.0.1.RELEASE</version>
</dependency>

2.repository interface
public interface ArticleRepository extends ElasticsearchRepository<Article, String> {
 
    Page<Article> findByAuthorsName(String name, Pageable pageable);
 
    @Query("{\"bool\": {\"must\": [{\"match\": {\"authors.name\": \"?0\"}}]}}")
    Page<Article> findByAuthorsNameUsingCustomQuery(String name, Pageable pageable);
}

3. config
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.baeldung.spring.data.es.repository")
@ComponentScan(basePackages = {"com.baeldung.spring.data.es.service"})
public class Config {
 
    @Bean
    public NodeBuilder nodeBuilder() {
        return new NodeBuilder();
    }
 
    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        Settings.Builder elasticsearchSettings = 
          Settings.settingsBuilder()
          .put("http.enabled", "false") // 1
          .put("path.data", tmpDir.toAbsolutePath().toString()) // 2
          .put("path.home", "PATH_TO_YOUR_ELASTICSEARCH_DIRECTORY"); // 3
 
        logger.debug(tmpDir.toAbsolutePath().toString());
 
        return new ElasticsearchTemplate(nodeBuilder()
          .local(true)
          .settings(elasticsearchSettings.build())
          .node()
          .client());
    }
}

4. model
@Document(indexName = "blog", type = "article")
public class Article {
 
    @Id
    private String id;
     
    private String title;
     
    @Field(type = FieldType.Nested)
    private List<Author> authors;
     
    // standard getters and setters
}


5. add
	elasticsearchTemplate.createIndex(Article.class);
	
	Article article = new Article("Spring Data Elasticsearch");
	article.setAuthors(asList(new Author("John Smith"), new Author("John Doe")));
	articleService.save(article);
	
6. query
	String nameToFind = "John Smith";
	Page<Article> articleByAuthorName
  		= articleService.findByAuthorName(nameToFind, new PageRequest(0, 10));
  	
  	SearchQuery searchQuery = new NativeSearchQueryBuilder()
  .withFilter(regexpQuery("title", ".*data.*"))
  .build();
List<Article> articles = elasticsearchTemplate.queryForList(searchQuery, Article.class);


7. backup and restore
https://www.elastic.co/blog/introducing-snapshot-restore
https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-snapshots.html
https://www.elastic.co/guide/en/elasticsearch/guide/current/backing-up-your-cluster.html
http://chrissimpson.co.uk/elasticsearch-snapshot-restore-api.html
