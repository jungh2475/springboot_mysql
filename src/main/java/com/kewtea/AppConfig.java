package com.kewtea;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class AppConfig {  //spring-boot-mysql
	
	
	//to use @Autowired private MessageSource messageSourceJournal; ->messageSourceJournal.getMessage("app.name", param ,localeConst) 
	//Locale localeCosnt = Locale.FRANCE; or new Locale("ENGLISH", "US"); Locale.getDefault()); ->Locale.US
	// param {0} {1} {2} -> final String[] params = {service.getAwesomeWebsite()....};  or new Object[]{"Joe"}
	/*
	 * <h1 th:text="#{welcome}">Welcome</h1>
	 * <a href="?language=en">English</a>
	 */
	@Bean
    public MessageSource messageSourceJournal () {
		Locale.setDefault(new Locale("English","US"));  //이것이 계속 적용되어져 있을까? 
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages/journal"); //(folder_name/prefix_name)  
        //messageSource.setBasenames("messages/msg", "messages/msg2").setParentMessageSource(parentMessageSource).setDefaultEncoding("UTF-8").setCacheMillis(500);
        return messageSource;
    }
	
	//to use at Controller @Autowired private MessageSource messageSourceKewtea;
	@Bean
    public MessageSource messageSourceKewtea () {
		Locale.setDefault(new Locale("English","US"));
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages/kewtea"); //(folder_name/prefix_name)  
        //messageSource.setBasenames("messages/msg", "messages/msg2").setParentMessageSource(parentMessageSource).setDefaultEncoding("UTF-8").setCacheMillis(500);
        return messageSource;
    }
	
	/*
	
	extends WebMvcConfigurerAdapter -> @Override
        public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(localeChangeInterceptor());
        }
	
	@Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
            LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
            localeChangeInterceptor.setParamName("lang");
            return localeChangeInterceptor;
    }
	
	
	
	@Bean(name = "localeResolver")
    public CookieLocaleResolver localeResolver() {
            CookieLocaleResolver localeResolver = new CookieLocaleResolver();
            Locale defaultLocale = new Locale("en");
            localeResolver.setDefaultLocale(defaultLocale);
            return localeResolver;
    }
    
    */
	
}
