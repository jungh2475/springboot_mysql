package com.kewtea;


import java.lang.reflect.Method;
import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


//https://github.com/spring-projects/spring-boot/blob/master/spring-boot-samples/spring-boot-sample-web-static/src/main/java/sample/web/staticcontent/SampleWebStaticApplication.java
//check service status using actuator : localhost:8080/actuator, /info, /health /metrics

@SpringBootApplication
@EnableAsync
public class Application extends AsyncConfigurerSupport  {

	public static void main(String[] args) {
		
		//Locale.setDefault(Locale.US);
		
		SpringApplication.run(Application.class, args);
	}
	
	@Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(50);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("spring-boot-mysql-");
        executor.initialize();
        return executor;
    }
	
	
	@Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new CustomAsyncExceptionHandler();
		//return new SimpleAsyncUncaughtExceptionHandler();
        //A default AsyncUncaughtExceptionHandler that simply logs the exception...다른일 하고 싶으면 custom class bean  만들어야..
        //Interface AsyncUncaughtExceptionHandler -handleUncaughtException(java.lang.Throwable ex, java.lang.reflect.Method method, java.lang.Object... params)
	}
	
	
	public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
	 
		@Override
		public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
			System.out.println("Exception message - " + throwable.getMessage());
	        System.out.println("Method name - " + method.getName());
	        for (Object param : obj) {
	            System.out.println("Parameter value - " + param);
	        }
			
		}

	     
	}
	
	/* force UTF-8 for http form post  from http://theeye.pe.kr/archives/2206
	//possible conflict with spring security - so needs to be executed first of all
	
	
	@Bean
    public HttpMessageConverter<String> responseBodyConverter() {
        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
    }
 	
 	//TODO: SpringSecurity 사용시. CsrfFilter 앞에 CharacterEncodingFilter를 놓아야 한다
 	@Order(Ordered.HIGHEST_PRECEDENCE)  //가장 먼져 적용되어야 함. security보다도 더 먼져 http://stackoverflow.com/questions/20863489/characterencodingfilter-dont-work-together-with-spring-security-3-2-0/23051264#23051264
    @Bean
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }
	*/

}