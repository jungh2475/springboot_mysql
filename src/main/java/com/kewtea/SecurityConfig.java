package com.kewtea;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.kewtea.services.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    DataSource dataSource;
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
		.antMatchers("/css/**","/js/**")      //static resources
		.antMatchers("/webpage/**")   // see WebPageController.java
		.antMatchers("/test2/**")		// see TestApi.java
		.antMatchers("/abc/**")	 // see	User2Repository.java - RepositoryRestResource
		.antMatchers("/resources/**");  //see application.properties/rest.basePath
		//super.configure(web);  //이거 빼야하나?
		
	}
	
	//rememberme : http://websystique.com/spring-security/spring-security-4-remember-me-example-with-hibernate/
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/kewtea/testbypass", "/kewtea/loginForm", "/journal/**").permitAll()  //TODO :임시테스트 코드임 
			.anyRequest().authenticated();
		http.formLogin().loginProcessingUrl("/login")
						.loginPage("/kewtea/loginForm").failureUrl("/kewtea/loginForm?error")
		.usernameParameter("username").passwordParameter("password")
		//.and().rememberMe().rememberMeParameter("remember-me").tokenRepository(persistentTokenRepository()).tokenValiditySeconds(86400)
		;
		//super.configure(http);
	}
	
	@Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }

	
	@Configuration
	static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter{
		
		@Autowired
		UserService userDetailsService;
		@Bean
		PasswordEncoder passwordEncoder(){
			return new BCryptPasswordEncoder();
		}
		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			// TODO Auto-generated method stub
			auth.userDetailsService(userDetailsService)
				.passwordEncoder(passwordEncoder());			
			//super.init(auth); //이거빼야 하나?
		}
		
		
		
	}

}
