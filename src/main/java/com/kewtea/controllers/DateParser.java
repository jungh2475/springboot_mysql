package com.kewtea.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * 날짜값을 만들거나, 패턴을 읽어 낼때 사용하면 되는 util 함수
 *  
 */

public class DateParser {
	
	private static final Logger log = Logger.getLogger(DateParser.class.getName());
		
	public DateParser(){}
	
	public ArrayList<String> parseDate(String dateValue){
		/*
		 * example List<String> => dateTwo,20160601,20160603
		 * 여기나온 String날짜는 validate해봐야 함. 
		 * 
		 */
		ArrayList<String> aList = new ArrayList<>();
		// valid한 날짜 인지를 확인하고, 맞으면 차례로 값을 쌓아서 전달한다 
		// [mode,fromDate,toDate] //mode=1 fromDate_only, 2=>from-to, 3=>summary,from,to
		
		String[] regxs = new String[5];
		regxs[0] ="(\\d{8})"; //20160601 "(d{8})"
		regxs[1] ="(\\d{8})\\-(\\d{8})"; //20160601-20160602 "(\\d+)\\-(\\d+)";  //(d{8})\\-(d{8})
		regxs[2] ="s(\\d{8})"; //s20160601 "s(d+)"
		regxs[3] ="s(\\d{8})\\-(\\d{8})"; //s20160601-20160603 "s(\\d+)\\-(\\d+)"
		regxs[4] ="s(\\d{8})\\-(\\d{8})\\-(\\d)"; //s20160601-20160603-1 "s(\\d+)\\-(\\d+)\\-d"
		//Java doesn’t have raw strings, so backslashes must be doubled, but the resultant string is the same
		
		Pattern[] pats = new Pattern[5];
		Matcher[] matchers= new Matcher[5];
		for(int i=0; i<5; i++){
			
			pats[i] = Pattern.compile(regxs[i]); //s or S ,Pattern.CASE_INSENSITIVE
			matchers[i]=pats[i].matcher(dateValue);
			if(matchers[i].find()){  //or find() matches()
				log.info("found match on pattern["+i+"]");
				switch(i){
				case 0:
					aList.add(0,"dateOne");
					aList.add(1,matchers[i].group(1));
					break;
				case 1:
					aList.add(0,"dateTwo");
					aList.add(1,matchers[i].group(1));
					aList.add(2,matchers[i].group(2));
					break;
				case 2:
					aList.add(0,"summaryOne");
					aList.add(1,matchers[i].group(1));
					break;
				case 3:
					aList.add(0,"summaryTwo");
					aList.add(1,matchers[i].group(1));
					aList.add(2,matchers[i].group(2));
					break;
				case 4:
					aList.add(0,"summaryThree");
					aList.add(1,matchers[i].group(1));
					aList.add(2,matchers[i].group(2));
					aList.add(3,matchers[i].group(3));
					break;
				default:
					aList.add(0,"error"); //no match
					break;
				}				
				log.info("parsed["+aList.get(0)+"]"+aList.size());
			}
		}
		return aList;
	}
	
	public boolean validateDate(String dateTest){
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
		try {
			Date date=format.parse(dateTest);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	//두날짜값이 적합한 숫자인지 확인하고, from이 더 작은지도 확인한다 
	public boolean validateDate(String fromDate, String toDate){
		if(!validateDate(fromDate)){return false;}
		if(!validateDate(toDate)){return false;}
		if(Integer.parseInt(fromDate)>Integer.parseInt(toDate)){return false;}
		return true;
	}
	
	public boolean validateDate(ArrayList<String> aList){
		
		return true;
	}
	
	//몇일뒤의 값을 String으로 받음 -하면 과거값을 가져옴 
	public String getNextDate(String curDate, int offset) throws ParseException{
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
		Date date=format.parse(curDate);
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, offset);
		return format.format(calendar.getTime());
	}

}
