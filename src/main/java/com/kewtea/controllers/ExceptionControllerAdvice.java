package com.kewtea.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

//read: https://www.javacodegeeks.com/2016/01/exception-handling-spring-restful-web-service.html
@ControllerAdvice
public class ExceptionControllerAdvice {
	
	@ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        return new ResponseEntity<Object>(
          "Access denied message here", new HttpHeaders(), HttpStatus.FORBIDDEN);
  }

	
	
	//@ResponseStatus(HttpStatus.CONFLICT)  // 409
	 @ExceptionHandler(Exception.class)
	 public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
	      ErrorResponse error = new ErrorResponse();
	      error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
	      error.setMessage("Please contact your administrator");
	      return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	 }
	 
	 
}



