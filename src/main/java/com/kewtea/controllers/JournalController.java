package com.kewtea.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.kewtea.models.CommentJournal;
import com.kewtea.models.ContentJournal;
import com.kewtea.models.User2;
import com.kewtea.models.json.Hgroup;
import com.kewtea.repositories.UserRepository;
import com.kewtea.service.User2Details;
import com.kewtea.service.User2Service;
import com.kewtea.services.JournalService;

/*
 * 
 *   home
 *   today
 *   /@username/20160401, @username
 *   login
 *   about
 *   
 *   create User, resetPassword, resendActivationLink
 * 
 */
@Controller
@RequestMapping("/journal")
@EnableAutoConfiguration
public class JournalController {

	private static final Logger log = Logger.getLogger(JournalController.class.getName());
	
	/*
	 *  showDatePage 
	 */
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private User2Service userService;
	@Autowired
	private JournalService journalService;
	@Autowired   
	private MessageSource messageSourceJournal; 
	//1)Locale userLocale 2)messageSourceJournal.getMessage("app.name", param=null, default,userLocale);
	
	private Locale setLocale(User2Details user, String reqHead_acceptLang, Locale locale){
		Locale result=locale;
		if(!(reqHead_acceptLang.isEmpty())) result=new Locale(reqHead_acceptLang);
		//if(!(user.locale.isEmpty())) result=new Locale(user.locale);//여기는 model에 user.locale를 추가하고 적용하자 
		return result;
	}
	
	
	@RequestMapping(value={"/","", "/about"}, method = RequestMethod.GET)
	String home(@AuthenticationPrincipal User2Details user2Details, HttpServletRequest request){
		
		//TODO: locale 적용  Example
		String localeString = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);//복수인경우 처리 필요.첫번째만 취한다. fr,es,en
		if(localeString.contains(",")) localeString=localeString.substring(0,localeString.indexOf(","));
		Locale userLocale = setLocale(user2Details, localeString, Locale.getDefault());
		String myMsg=messageSourceJournal.getMessage("1st.2nd", null,"default", userLocale);
		System.out.println(myMsg);
		
		
		//TODO: 여기서 landingPage로 갈지, 로그인되있으면 실제 내용으로 간다
		if(user2Details !=null){
			log.info("redirecting as you are logged in");
			User2 user= userService.getUserByEmailname(user2Details.getUsername());
			//User2 user= userRepository.findByName("jungh").get(0);
			String userName=user.uniquename; //"jungh";
			if(userName==null){throw new IllegalArgumentException("userName is wrong");}
			String baseUrl="@"+userName;
			log.info("redirecting /journal/"+baseUrl);
			return "redirect:/journal/"+baseUrl;			
		} else {
			//TODO: 나중에 community 버젼에서 보여줄것...회사 및 공개 일기 .... /today
			return "journal/about";
		}
	}
		
	
	//TODO: regex /{userName:@\[a-z]{1,}}/{dateValue}"  //이건 에러가 나는 표현임 여기에 :\\d //regex : +(앞의반복1이상{1,}) *(앞의반복0이상{0,}) ?(앞의반복 0or 1회만 {0,1}) .(임의글자*), t$(end) 
	//Today
	@RequestMapping(value={"/@{userName}","/@{userName}/"}, method = RequestMethod.GET)
	String showDatePage(@PathVariable String userName){
		log.info("userName["+userName+"]");
		String baseUrl="@"+userName;
		SimpleDateFormat sdf= new SimpleDateFormat("yyyyMMdd");
		Date today = Calendar.getInstance().getTime();
		String url=sdf.format(today);//"20160101";
		return "redirect:/journal/"+baseUrl+"/"+url; //read http://www.baeldung.com/spring-redirect-and-forward 
		//return showDatePage(userName,Long.toString(System.currentTimeMillis()), model);
	}
	
	//TODO: sw-under-test : sut
	@RequestMapping(value="/@{userName}/@{dateValue}", method = RequestMethod.GET)
	String showDatePageNew(@PathVariable String userName, @PathVariable String dateValue, Model model) throws ParseException {
		
		User2 user;	
		List<ContentJournal> contentDatePage=null;
		List<CommentJournal> comments=null;

		//1.check userName & dateValue
		if (dateValue == null || dateValue =="") { throw new IllegalArgumentException("dateValue is wrong1");}
		log.info("userName["+userName+"] dateValue["+dateValue+"]");
		user=userService.getUserByUniqueName(userName);
		if(user ==null || userName.equals("error")){throw new IllegalArgumentException("userName is wrong");}
				
		//2. validate dateValue by pattern matching & getData
		DateParser dateParser= new DateParser();
		ArrayList<String> aList = dateParser.parseDate(dateValue);
		
		if(!dateParser.validateDate(aList)){ 
				log.info("no matching dateValue");
				throw new IllegalArgumentException("dateValue is wrong2");
		}		
		log.info("aList"+aList.size()+"]");
		
		if(aList.size()>1){
			////해당날짜 기준으로 앞뒤로 1일씩 총 1~3장 전송해야해서, 자료를 획득 (해당 날짜 내용 없으면? 빈 내용으로.새로 만들기) 
			String from = dateParser.getNextDate(aList.get(1), -1);
			String to = dateParser.getNextDate(aList.get(1), 1);
			log.info("getDataBetween["+from+"]["+to+"]");
			contentDatePage = journalService.getDatePages(user.id, Integer.parseInt(from), Integer.parseInt(to), 0);
		}
		if(contentDatePage.size()!=0){
			log.info("contentDatePage["+contentDatePage.size()+"]");}
		
		
		
		return "journal/diary";
		
	}
	@RequestMapping(value="/@{userName}/{dateValue}", method = RequestMethod.GET)
	String showDatePage(@PathVariable String userName, @PathVariable String dateValue, Model model) throws ParseException {
		
		User2 user;	
		List<ContentJournal> contentDatePage=null;
		List<CommentJournal> comments=null;
		
		//1.check userName & dateValue
		if (dateValue == null || dateValue =="") { throw new IllegalArgumentException("dateValue is wrong1");}
		log.info("userName["+userName+"] dateValue["+dateValue+"]");
		user=userService.getUserByUniqueName(userName);
		if(user ==null || userName.equals("error")){throw new IllegalArgumentException("userName is wrong");}
		
		//2. validate dateValue by pattern matching & getData
		DateParser dateParser= new DateParser();
		ArrayList<String> aList = dateParser.parseDate(dateValue);
		
		if(!dateParser.validateDate(aList)){ 
			log.info("no matching dateValue");
			throw new IllegalArgumentException("dateValue is wrong2");
		}
		
		log.info("aList"+aList.size()+"]");
		
		if(aList.size()>1){
			////해당날짜 기준으로 앞뒤로 1일씩 총 1~3장 전송해야해서, 자료를 획득 (해당 날짜 내용 없으면? 빈 내용으로.새로 만들기) 
			String from = dateParser.getNextDate(aList.get(1), -1);
			String to = dateParser.getNextDate(aList.get(1), 1);
			log.info("getDataBetween["+from+"]["+to+"]");
			contentDatePage = journalService.getDatePages(user.id, Integer.parseInt(from), Integer.parseInt(to),0);
			if(contentDatePage.size()!=0){
				log.info("contentDatePage["+contentDatePage.size()+"]");
				model.addAttribute("contentDatePage",contentDatePage);
			}
			//TODO: 오늘 날짜는?
			//commentid들을 추출해서 목록 만들기 
			List<Long> idallList = new ArrayList<>();
			
			for(ContentJournal content: contentDatePage){
				List<String> commentsStr=null;
				if(content.comments!=null){
					
					log.info("coments{"+content.comments+"}");
					commentsStr=Arrays.asList(content.comments.replaceAll("\\s","").split(","));
				}
				log.info("ids["+commentsStr.size()+"]");
				for(String commentStr: commentsStr){
					log.info(commentStr);
					log.info(":"+Long.parseLong(commentStr));
					idallList.add(Long.parseLong(commentStr));
				}				
			}
			log.info("comments to pass["+idallList.size()+"]");
			if(idallList.size()>0){
				log.info("hi1");
				long[] ids = new long[idallList.size()];
				int count=0;
				for(Long id: idallList){
					ids[count++]=id.longValue();
				}
				log.info("adding comments");
				comments=journalService.getComments(ids);
				if(comments!=null){log.info("adding comments["+comments.size()+"]");}
				model.addAttribute("comments", comments);
				log.info("comments["+comments.size()+"]");
				
			}
			log.info("hi2");
			
			
		}
		
		log.info("completed getdata"); //passing contentDatePage, comments to model
		
		//Javascript+HTML에 값을 전달하려면, jsonString으로 전달하세요
		/*
		Gson.gson=newGson();
		String jsonString=gson.toJson(Object)
		model.addAttribute("varName",jsonString);
		
		//그럼 받는 쪽(thymeleaf html)에서는 
		 * 
		 * <script th:inline="javascript">
		 * var tempString=[[${varName}]];
		 * var finalValue=JSON.parse(tempString);
		 * </script>
		 * 
		
		*/
		
		/*
		 *  add today dateValue
		 *  add datePages
		 *  add comments (what about error comments Ids?)
		 *  
		 *  dateValue에 있지만 datePage가 없을 경우 -> 새로 만드는 화면
		 *  
		 */
		
		String jsonString1="abc";
		model.addAttribute("jsonString1", jsonString1);
		String jsonString2="I\'m \"abc\"";
		model.addAttribute("jsonString2", jsonString2);
		log.info("passing test data:jsonString[1,2,3]");
		Hgroup hGroup = new Hgroup("이정환",jsonString2,"label\'label\'","https://static.pexels.com/photos/3948/field-meadow-flower-pink.jpg");
		//Hgroup hGroup = new Hgroup("이정환","subTitle","label","httpsUrl");
		Gson gson = new Gson();
		List<Hgroup> hGroups = new ArrayList<>();
		hGroups.add(hGroup);
		hGroups.add(hGroup);
		hGroups.add(hGroup);
		String jsonString3=gson.toJson(hGroup);
		model.addAttribute("jsonString3", jsonString3);
		String jsonString4=gson.toJson(hGroups);
		model.addAttribute("jsonString4", jsonString4);
		
		return "journal/diary";
	}
	
	
	//로그인에도 쓰이고 signp에도 쓰인다 
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(@RequestParam(required=false, defaultValue = "0") String error, Model model){    ///login?error=abc @PathVariable, @RequestParam
		log.info("error:"+error);
		model.addAttribute("note", "");
		return "journal/login";
	}
	
	
	
	
	
	//주소가 맞지 않는 경우, method는 실행을 했는데 안에서 error가 발생한 경우 
	@RequestMapping(value="/*", method = RequestMethod.GET)
	String error(){
		log.log(Level.INFO, "url argument is wrong");
		throw new IllegalArgumentException("url is wrong");
	} 
	
	
	@ExceptionHandler
	String handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
	    //response.sendError(HttpStatus.BAD_REQUEST.value(), "Please try again and with a non empty ");
		log.log(Level.INFO, "calling local journal error page");
	    return "journal/error";
	    
	}
	
	
}
