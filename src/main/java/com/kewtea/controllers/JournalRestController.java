package com.kewtea.controllers;


import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kewtea.models.User2;
import com.kewtea.repositories.UserRepository;
import com.kewtea.service.MailService;
import com.kewtea.service.User2Service;
import com.kewtea.services.JournalService;

/*
 * 구현해야할 journal rest api들 topUrl(/journalapi)
 * [Content]
 * getDatePage   //이건 rest default로 가능 (id, or uid+fromDate+toDate+type(inclusive)), put/post/delete 
 * getSummaryPage  //이건 service로 구현, 
 * getComment    //이건 rest default로 가능, 기간 및 사용자 설정해서 받아 오자  , put/post/delete
 * getTimeline   //이건 rest default로 가능 , put/post/delete
 * Int.id mergePages(from, to)
 * 
 * [User]사용자 관련된 api들  //이건 User2Service로 구현하세요 (User2Repository rest default는 쓰지 말자) 
 * createUser
 * sendActivationlinkMail
 * resetPassword
 * checkUser
 * 
 * 원래 repository가 직접 제공하는 rest api는 무엇인가?
 * user2
 * contentjournal    
 * commentjournal   : 
 * 
 * 그외는?  이기간동안 이사용자의 comments들 반환 where collectionName=x, keyword=x, 
 * 
 * url(/content/{uid}/id) => getContent(uid,from,to), put(update), post, delete   
 * url(/content/{uid}/{collection},/content/{uid}/{keyword}) => rest-repository 
 * url(/comment/{uid}/{collection},/comment/{uid}/{keyword}) => rest-repository
 * url(/collection/{uid}/?fromDate=x&toDate=y
 * 
 * 
 * 
 
 * 
 */

//user-related
//TODO: 동시접속중인 상태인가? 그러면 체크 주기가 짧다 - 20초마다?, 평소에는 , 최근 2분간 다른곳에서 접속을 했는가? 쿠키값으로 판단?
/*마지막 접속시간 및 쿠키값을 적는다. 다른 쿠키이고, 10분전에 접속했다면 동시접속으로 보고, 빠른 싱크를, 최근 접속이 10분 이상이면 동시적속이 아닌것으로 
* 접속할때마다 마지막 접속시간을 적어 놔야 하는가? 
* 
*  Input @PathVariable("firstName"), @RequetParam(value="empId", required=false, defaultValue="0") @RequestBody Bookmark input 
*  		=> Output : @ResponseBody Object or ResponseEntity<T> : return new ResponseEntity<Car>(car, HttpStatus.OK);
*/

@RestController
@RequestMapping("/api/journal")
public class JournalRestController {
	
	private static final Logger log = Logger.getLogger(JournalRestController.class.getName());
	
	@Autowired
	private JournalService service;

	@Autowired
	private User2Service userservice;
	@Autowired
	private UserRepository userrepository; 
	@Autowired
	private MailService mailservice;
	
	
	
	@RequestMapping(value = "/summary/", method=RequestMethod.POST)
	public long makeSummaryPage(){
		return 1L;
	}
	
	
	
	//check User uniquename, emailname, name, & active, status
	@RequestMapping(value = "/user/{useremail}", method=RequestMethod.GET)
	@ResponseBody
	public int userstatus(@PathVariable String useremail){
		
		// -1 not exists
		// 0 exists but not activated
		// 1 active User
		
		//if(service.check(passstring)==-1) {
		//	//오래 되서 다시 보내야 하는가?
			//그런 링크가 없음 
		//}
		
		return 1;
	}
	
	//sendActivationLink
	@RequestMapping(value = "/sendactivationlink/{userid}", method=RequestMethod.GET)
	@ResponseBody
	public boolean sendActivationLink(@PathVariable long userid){
		User2 user = userrepository.findOne(userid);
		if(user.email == null) return false;
		String url2click = userservice.registerActLink(user.id);
		try {
			mailservice.sendMail(user.email, "hello testing", "testing from kewtea journal including activationlink: " + url2click);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			log.log(Level.WARNING, "mail send failed");
			e.printStackTrace();
		}
		return true;
	}
	
	@RequestMapping(value = "/checklink/{passstring}", method=RequestMethod.GET)
	@ResponseBody
	public boolean validActivationLink(@PathVariable String passstring){
		
		//if(service.check(passstring)==-1) {
		//	//오래 되서 다시 보내야 하는가?
			//그런 링크가 없음 
		//}
		
		return true;
	}
	
	/*	
	 	//if() throw new EmployeeException("Invalid employee name requested");
	 	
	  	@ExceptionHandler(EmployeeException.class)
    	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        	ErrorResponse error = new ErrorResponse();
        	error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        	error.setMessage(ex.getMessage());
        	return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);  
        }

	  
	  
	 
	  
	 */

}




/*  How to resolve Rest Exception?
https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc
@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFoundException extends RuntimeException {
	public UserNotFoundException(String userId) {
		super("could not find user '" + userId + "'.");
	}
}

*/