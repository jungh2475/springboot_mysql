package com.kewtea.controllers;

import java.util.logging.Logger;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kewtea.service.User2Details;

/*
 *  home
 *  login, signup(register User), update User(changePassword), resetPassword, sendActivationLink
 *  
 *  
 */


@Controller
@RequestMapping("/kewtea")
public class KewteaController {
	
	private static final Logger log = Logger.getLogger(KewteaController.class.getName());
	
	
	@RequestMapping(value="/loginForm", method = RequestMethod.GET)
	String loginForm(){//@RequestParam String param, model...){
		log.info("calling login Form");
		return "kewtea/login";
	}
	
	@RequestMapping(value={"","/","/test"}, method = RequestMethod.GET)
	String home1(@AuthenticationPrincipal User2Details user2Details){
		if(user2Details!=null){
			log.info("authPrincipal["+user2Details.getUsername()+"]");
		}else{log.info("authPrincipal[]");}
		return "kewtea/about";
	}
	
	@RequestMapping(value="/testbypass", method = RequestMethod.GET)  //headers = "Accept=application/json", value에 여러주소 mapping 가능,  
	String home2(@AuthenticationPrincipal User2Details user2Details, Model model){  //{var_name:regex} -> @PathVariable, @RequestParam("id")
		//
		//login되어 있으면 redirect, 안되어 있으면 about landingPage 보여주
		if(user2Details!=null){
			log.info("authPrincipal["+user2Details.getUsername()+"]");
		}else{log.info("authPrincipal[]");}
		
		return "kewtea/about";
		//return "redirect:today";
	}
	
	//login
	
	
	//
	
	
	//fallback, see http://www.baeldung.com/spring-requestmapping
	@RequestMapping(value = "*", method = { RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getFallback() {
	    return "Fallback for GET Requests";
	}
	
	

}
