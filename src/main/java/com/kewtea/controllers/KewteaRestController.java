package com.kewtea.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kewtea.models.SysRecord;
import com.kewtea.repositories.SysRecordRepository;
import com.kewtea.service.SystemService;

/*
 *  
 * 
 * 
 * 
 */

@RestController
@RequestMapping("/api/kewtea")
public class KewteaRestController {
	
	private static final Logger log = Logger.getLogger(JournalRestController.class.getName());
	
	@Autowired
	private SystemService systemService;
	private SysRecordRepository sysrecordRepository;
	
	
	@RequestMapping(value = "/report/", method=RequestMethod.POST)  //with valid api key
	@ResponseStatus(HttpStatus.CREATED)
	public SysRecord reportLog(@RequestHeader(value="api_key") String api_key1, @RequestParam("api_key") int api_key2, 
									@RequestBody SysRecord sRecord){
		if(Integer.parseInt(api_key1) !=3847 && api_key2 !=3847) return null;//두군데중에 하나라도 맞으면 기록을 허가한다 
		if(sRecord ==null || sRecord.id != 0L) return null;
		sysrecordRepository.save(sRecord);
		return sRecord;  
		//or return new ResponseEntity<SysRecord>(sysRecord, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/report/count_errors", method=RequestMethod.GET) 
	public int countErrors(@RequestParam(value="weekNumber", defaultValue="0") int w_num){
		List<SysRecord> sysRecords=listErrors(w_num);
		return sysRecords.size();
	}
	
	@RequestMapping(value = "/report/errors", method=RequestMethod.GET) 
	public List<SysRecord> listErrors(@RequestParam(value="weekNumber", defaultValue="0") int w_num){
		//정해진 주간동안의 에러들만 추출해서 알려 준다 
		List<SysRecord> sysRecords =new ArrayList<>();
		if(w_num==0) w_num=1; //이번주를 의미함, 아니면  대신에..required=false 
		long from, to;
		from=getTimeByWeek(2017, w_num);  //
		to=getTimeByWeek(2017, w_num+1);
		sysRecords = sysrecordRepository.findByLevelAndTimestampBetween("e", from, to);
		return sysRecords;
	}
	
	private long getTimeByWeek(int year, int week_num){  //(2016,23), (2017,01)
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.WEEK_OF_YEAR, week_num);   //23     
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		//String yearmonthday=cal.getTimeInMillis(); //"20161103";
		/*
		DateFormat dfm = new SimpleDateFormat("yyyyMMdd");
		//dfm.setTimeZone(TimeZone.getTimeZone("GMT+5:30")
		long unixtime;
		try{
			unixtime = dfm.parse(yearmonthday).getTime();  // milliseconds since Jan 1, 1970  / 1000
		}catch (ParseException e) 
	    {
	        e.printStackTrace();
	    }
		return 1L;
		*/
		return cal.getTimeInMillis();
	}
	
}
