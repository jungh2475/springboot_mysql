package com.kewtea.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
  	TODO: product register (register Product, register product api source) => aggregate Reviews
  	TODO: sync user account ( update userAccounts)
 
 */

@RestController
@RequestMapping("/api/sphouse")
public class SphouseRestController {
	
	private static final Logger log = Logger.getLogger(SphouseRestController.class.getName());
	
	//@Autowired
	
	//@RequestMapping(value = "/summary/", method=RequestMethod.POST)
	
	//call by: throw new NumberFormatException("number format exception");
	@ExceptionHandler(value = NumberFormatException.class)  
    public String nfeHandler(NumberFormatException e){  
        return e.getMessage();  
    }
	
//read more at : http://www.ekiras.com/2016/02/how-to-do-exception-handling-in-springboot-rest-application.html
	
}
