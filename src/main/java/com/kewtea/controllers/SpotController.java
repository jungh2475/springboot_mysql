package com.kewtea.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/*
 * 
 * 
 *  /showcase or demo 
 	
 	@brandname
 	@shopname, @brandname/@shopname
 	
 	@brandname/@shopName/display_id
 	
 	
 */

@Controller
@RequestMapping("/spot")
public class SpotController {
	
	private static final Logger log = Logger.getLogger(SpotController.class.getName());
	
	
	//spot.kewtea.com/showcase ->spot.kewtea.com/showcasereceiver 
	@RequestMapping(value="/showcase", method = RequestMethod.GET)
	String shwocase(Model model){
		
		//		
		return "spot/showcase";
		
	}
	
	@RequestMapping(value="/showcasereceiver", method = RequestMethod.GET)
	String showcasereceiver(Model model){
		
		//		
		return "spot/showcasereceiver";
		
	}
	
	/*
	 * illegalargument : 
	 */
	//call by: throw new NumberFormatException("number format exception");
	@ExceptionHandler(value=IllegalArgumentException.class)  //{}
	String SomeException(IllegalArgumentException e){
		e.printStackTrace();
		log.log(Level.WARNING, e.getMessage());
		return "spot/error";
	}
	

}
