package com.kewtea.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kewtea.models.SysRecord;
//import com.kewtea.GlobalConstants;

@RestController
@RequestMapping("/api/v1")  //"/api/report", value=API_PREFIX
public class SystemRestController {
/*
	1. reportLog, /report/log
	2. reportWebhook, report/webhook/
	3. reportStatus, /reportAll
	4.
	
	
*/
	
	//원래는 kewteaRestController에 있는 것을 이관했음. 
	//@Value("${api.path}")  //v1
	//private String apiPath;
	
	@RequestMapping(value = "/report/", method=RequestMethod.POST)  //with valid api key
	@ResponseStatus(HttpStatus.CREATED)
	public SysRecord reportLog(@RequestHeader(value="api_key") String api_key1, @RequestParam("api_key") int api_key2, 
									@RequestBody SysRecord sRecord){
		if(Integer.parseInt(api_key1) !=3847 && api_key2 !=3847) return null;//두군데중에 하나라도 맞으면 기록을 허가한다 
		if(sRecord ==null || sRecord.id != 0L) return null;
		//sysrecordRepository.save(sRecord);
		return null; //sRecord;  
		//or return new ResponseEntity<SysRecord>(sysRecord, HttpStatus.OK);
	}
	
	@Value("${url.webhook}")
	private final String webhook="webhook";
	
	@RequestMapping(value="/"+webhook+"/{appName}", method = RequestMethod.POST)
	public ResponseEntity<?> reportWebhook(@PathVariable String appName, @RequestBody String input){
	//외부에서 여기로 callback을 해서 내부 서비스로 처리를 하고 그쪽에는 response를 정상적으로 넣어 준다 	
		HttpHeaders responseHeaders = new HttpHeaders();
		HttpStatus httpStatus = null;
		String temp;
		
		//여기서 service를 호출해서  db에 자료를 저장해 둔다 
		switch(appName){
			case "2checkout":
				//
				httpStatus=HttpStatus.OK;
				break;
			case "iamport":
				//
				break;
			case "slack":
				//
				break;
			default:
				responseHeaders.set("MyResponseHeader", "MyValue");
				//error or exception " no such webhook exist 
				httpStatus=HttpStatus.NOT_FOUND; //404 Not Found.
				
				break;
		}
		
		
		temp="";
		
		return new ResponseEntity<>(temp,responseHeaders,httpStatus); //Enum: HttpStatus.CREATED,FOUND
	}
	
	
	
	@ExceptionHandler(Exception.class)   //({ CustomException1.class, CustomException2.class })
	 public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
	    error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
	    error.setMessage(ex.getMessage());
	    return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}

	
}


/*
 * ----------------------
 * @controller annotation summary at http://javakorean.com/?p=2451
 * ----------------------
 * @Controller는 다음 파라미터들이 가능함 - HttpServletRequest/HttpServletResponse/ServletRequest/ServletResponse
 * 
 * 
 * @RequestMapping(value="/user/{userId}/roles, ...) ->> @PathVariable("userId"),,...int,long,date,string
 * @RequestParam(value = "date", required = false) ......defaultValue = "anonymous
 * 
 * 
 * public String hello(HttpServletResponse response,
 * 			@CookieValue("foo", defaultValue = "hello") String or Long fooCookie) {
 * 			response.addCookie(new Cookie("foo", "bar"));
 * @RequestHeader("Host") or ("Keep-Alive")
 * 
 * 모델을 파라미터로 정의해서 핸들러 어댑터에서 미리 만들어 제공해주는 것을 사용하면 편리
 * Model(Map) model.addAttribute("key",objectValue); 
 * 
 * @ModelAttribute("name"),, post로 객체가 들어온 
 * 메소드 파라미터에 부여하는 경우로 사용하자(파라미터는 별도의 설정 없이도 자동으로 뷰에 전달)
 * 요청 파라미터가 많은 경우 @RequestParam대신에,@ModelAttribute 가 붙은 파라미터를 처리할 때는 @RequestParam 과 달리 Validation 작업이 추가
 * 클래스 반환은 String이고(대신 파라미터에 Model model을 넣어줌), ModelAndView는 안써도 됨 ,string return -> "redirect:/results";
 * 
 * @Valid, 보통 @ModelAttribute 와 함께 사용 
 * https://spring.io/guides/gs/validating-form-input/ 
 * 
 * public String addNewPost(@Valid Post post, BindingResult bindingResult, Model model) {
	if (bindingResult.hasErrors()) {
		return "index";
	}
 * 
 * 
 */
