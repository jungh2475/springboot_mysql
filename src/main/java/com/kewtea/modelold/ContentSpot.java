package com.kewtea.modelold;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;


/*
   	
   	content : , MeasurementRegistry, Analytics, Content(SlideshowProgram), Station
 	comment :
 	entity : Shop(?)  //where located device, shop?
 	sysrecord: 
 	
 	
 	
 */

@Entity
@Data
@Table(name = "contentsspot")
public class ContentSpot {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String hgroup;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;     // campaignTable=>List<PlayUnit>이 들어갈수있음 
	//analyticView (dimension, metric)
	//campaign(type,url,from-to,actionEnd)
	//timeline(campaign id, timePeriod)
	
	@Column(nullable = false)
	public long uid;
	@Column(nullable = true)
	public String uniquename; //사용자 id안에서만 unique하여야 한다 , 
	public String contenttype; //analyticview, campaign, timeline	
	
	@Column(nullable = true)
	public String url;   //   optional for urlPage
	
	@Column(nullable = true)
	public String collections;
	@Column(nullable = true)
	public String keywords;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(nullable = true)
	public long parentid;
	
	@Column(nullable = true)
	public long createdby;
	@Column(nullable = true)
	public long createdwhen;
	@Column(nullable = true)
	public long lastupdated;
	@Column(nullable = true)
	public int timezone;         //+9 Seoul, 0 London
	
	public ContentSpot(){}
	
}
