package com.kewtea.modelold;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

/*
 * 
 *  api, 및 디바이스 그리고 사용자  collections, keywords등을 기록해둔다
 *  UserInventory로 할만한것은 이곳에 다 적어 둔다 
 *  keywords,  collections는 기간별로  snaptshot으로 저장해 둔
 * 
 */
@Entity
@Data
@Table(name = "entitiesjournal")
public class EntityJournal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@Column(unique=true, nullable=true)
	public String uniquename;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;  //jsonString
	@Column(nullable = true)
	public int contentsize;
	
	@Column(nullable = false)
	public String reference;
	@Column(nullable = false)
	public long uid;  //owner, 소유주 
	@Column(nullable = false)
	public String contenttype; //keywords,collections, extApiToken, creditBalance, devicedLinked
	@Column(nullable = true)
	public int datefrom;    //optional : 1개월 단위로 정리하면 좋다 - snapshot 저장 , 다시 copy해서 생성 
	@Column(nullable = true)
	public int dateto;      //
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false, 1 is true
	public boolean active;
	@Column(nullable = true)
	public long createdwhen;
	@Column(nullable = true)
	public long lastupdated;
	@Column(nullable = true)
	public int timezone;         //+9 Seoul, 0 London
	
	
	public EntityJournal(){}
	
	public EntityJournal(String bText, String cType, long u ){
		this.body=bText;
		this.contenttype=cType;
		this.uid=u;
		this.createdwhen=System.currentTimeMillis();
		this.lastupdated=System.currentTimeMillis();
	}
	
}
