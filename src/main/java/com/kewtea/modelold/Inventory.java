package com.kewtea.modelold;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
 *  json + key-value pair로 저장된 모든 값들 ....
 *  이것과 registry table하고 중복이 되는 것은 아닌지?
 *  device, customer, machine-server, iot-device들도 여기에 저장해야 하지 않을까? 
 *  
 *  
 *  type: customer, user(extra), device, app, server(cloud), url
 *  (project config, 설정값들은 registry에 가서 저장하고 활용하도록 할까?)
 *  
 *  
 *  
 */


@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "inventory")
public class Inventory {
	
	/*
	 	userExtra
	 	configs - dev-local, dev-cloud, production-stage1, production-stage2
	 	urls
	 
	 
	 */
	
	
}
