package com.kewtea.modelold;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

/*
 * 
     여기에 기록하는 것은 project 설정 내용들, inventory와 중복이 되는 가?
  		- project 등록, 제거 
  		- 하위 설정 값들을 기록
  		- application unique name
  		- 개발 단계 dev-local, dev-server, production-cloud, staging-cloud
 */
@Entity
@Data
@Table(name = "registry")
public class Registry {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean draft;
	
	@Column(nullable = true)
	public long timestamp;
	
	//구분/정렬 조건들 
	@Column(nullable = true)
	public long parent_id;
	@Column(nullable = true)
	public long project_id;
	@Column(nullable = true)
	public String type_class;  //config, objects, config-server_url, 
	
	//실제 내용은 다음의 두군데에 다 기록 됨 
	@Column(nullable = true)
	public String name_key;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String jsonbody;

}
