package com.kewtea.modelold;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

//https://github.com/royclarkson/spring-rest-service-oauth/
//되도록이면 table이름을 안건드리면 그대로 쓸수 있다 

@Entity
public class Role implements GrantedAuthority {  //this needs spring security
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotEmpty
	private String name;
	
	@JsonIgnore
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="roles")  //다음줄 표현과 연동되어 User2 Table의 roles 컬럼을 mapping 해준다 
	private Set<User2> users= new HashSet<User2>();
	
	@Override
	public String getAuthority() {
		return name;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User2> getUsers() {
		return users;
	}

	public void setUsers(Set<User2> users) {
		this.users = users;
	}

}

//이것도 값을 직접 입력해 준다 insert into role(id, name) values (1,'ROLE_USER'); insert into role(id, name) values (2,'ROLE_ADMIN');
//insert into role(id, name) values (3,'ROLE_GUEST'); 
