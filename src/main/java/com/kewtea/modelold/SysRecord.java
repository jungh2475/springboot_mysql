package com.kewtea.modelold;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "sysrecord")
public class SysRecord {
	
	//public static final String tagMysql="mysql";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)
	public int timezone;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long timestamp;
	
	@Column(nullable = true)
	public long uid;
	
	@Column(nullable = true)
	public String record_template_id;  // optional , log format matching
	
	@Column(nullable = true)
	public long did;  // device id, optional
	
	@Column(nullable = true)
	public long aid;  // application id or service id(journal, spot..), mandatory for client app 
	
	@Column(nullable = true)
	public long pid;   // process id, mandatory for server applciation
	
	@NotEmpty
	@Column(nullable=false)
	public String level;  //log level: e, i, w, d(developer)
	
	@NotEmpty
	@Column(nullable=false)
	public String type; // like title: type: client-ip, cookie(expire,authority-or-role), clientid(=deviceid+ serviceid-or-swappid)	
	
	@Column(nullable = true)
	public String tag; //like keywords
	
	@NotEmpty
	@Column(nullable = false)
	public String msg;  //value here
	
	@Column(nullable = true)
	public double ref_id;  //value here
	
	@Column(nullable = true)
	public String url;
	
	public SysRecord(String level, String type, String msg, int timezone, int timestamp ){
		this.level=level; this.type=type; this.msg=msg; this.timezone=timezone; this.timestamp=timestamp;
	}
	
	public SysRecord(long uid, long aid, String level, String type, String msg, int timezone, int timestamp ){
		this.uid=uid; this.aid=aid; this.level=level; this.type=type; this.msg=msg; this.timezone=timezone; this.timestamp=timestamp;
	}
	
	
	
}
