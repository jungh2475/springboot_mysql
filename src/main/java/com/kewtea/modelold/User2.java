package com.kewtea.modelold;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
	Constructor는 반드시 직접 선언할것 (@NoArgsConstructor 잘 안먹음)
	long, boolean은 notnullable로 자동 매핑이 된다 왜?
	Using columnDefinition is DB-specific
	
	bean validation (이런것들을 사용할수가 있음) 
	@NotNull(""),@NotEmpty(size>0),@NotBlank(String, size>0), @Max @Min @Size(min=0, max=10) @Pattern("RegEx") @Length @Range @Email @URL
	@JsonIgnore

	@Column(name = "title", nullable = false, length = 100)
	@Column(name="Price", columnDefinition="Decimal(10,2) default '100.00'")

	see http://www.objectdb.com/api/java/jpa/Column

	mapping UserDetails in Spring : https://docs.spring.io/spring-security/site/docs/3.2.6.RELEASE/apidocs/org/springframework/security/core/userdetails/UserDetails.html
	isEnabled() -> active  (email verification된 경우)
	여기 값을 읽어서 accountstatus 다음의 경우들에 대응을 한다   //-1:deleted or cancelled, 0:accountLocked, 1:accountNormal
		isAccountNonExpired() ->   (일정 기간 안쓰면 소멸_)
		isAccountNonLocked() ->  (이상한 접근이 많은 경우 일정시간  lock 시킴)
		isCredentialsNonExpired() -> 현재는 안쓰는 기능 (password_expire)
*/


@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "users")
public class User2 {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty 
	@Email
	@Column(unique=true, nullable=false)
	public String email;	
	
	//@JsonIgnore  //Rest에서 패스워드 노출되는 것을 막을려면, 이렇게 한다 .., EncodedPassword면 60char이상 값이 저장될것임 
	@Size(min = 4)
	public String password;
	
	@Column(nullable = true)
	public String name;  //John Smith
	
	@Column(unique=true, nullable=true)
	public String uniquename;   // url-mapped
	
	//public String api_version;
	
	@Column(nullable = true)
	public long creatoruid;   //이건 모지? 누가 이 계정을 생성했는가 - groupid 경 
	
	@Column(nullable = true)
	public long createdwhen;
	
	@Column(nullable = true)
	public long lastupdated;
	
	
	@Column(nullable = true)
	public String serviceids;   //activated된 서비스들:  journal,spot,
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean active;
	@Column(nullable = true)
	public int accountstatus;  //-1:deleted or cancelled, 0:accountLocked, 1:accountNormal
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;  //이 사용자 요주의 인물+문제있다고 보고한 경우 on
	
	
	@Column(nullable = true)
	public String accessrights;  //superadmin, journaladmin,...(참고용, 아래의 roles가 더 정확한 항목임)
	@JsonIgnore
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="user_role", joinColumns={ @JoinColumn(name="user_id") }, inverseJoinColumns={ @JoinColumn(name="role_id") })
	public Set<Role> roles = new HashSet<Role>();
	//이 테이블은 직접 만들어서 넣어 준다 insert into user_role(user_id, role_id) values (1,1); insert into user_role(user_id, role_id) values (1,2);
	//http://fruzenshtein.com/hibernate-join-table-intermediary/
	
	@Column(nullable = true)
	public String language;
	@Column(nullable = true)
	public String country;
	@Column(nullable = true)
	public String timezone;  //e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@Column(nullable = true)
	public String membership;    //유료멤버, 기타 회원 등급을 표시 할수 있음
	@Column(nullable = true)
	public long creditid;   // from entity table :  internal financial balance  돈관련 연결 계좌 id 
	
	@Column(nullable = true)
	public String affiliatedids;  //facebook ,twitter, google에서 쓸 변환 id 저장  
	//TODO: 여기는 각 사이트의 token값등 혹은 id 연동 값을 적어 두기 위해 Blob확장 보다는 다른 테이블을 만들어서 거기서 저장하게 하자 
	
	@Lob
	@Column(columnDefinition = "TEXT",nullable = true)
	public String bio;
	@Column(nullable = true)
	public String tags;  //사용자 별병, 기타 search에서 쓸 힌트들 
	
	
	
	public User2() { }
	
	public User2(User2 user) {
		// TODO Auto-generated constructor stub
	}
	
	public User2(long id){
		this.id = id;
	}
	
	public User2(String email, String name, String password){
		this.email = email;
		this.name = name;
		this.password = password;
		
		//나머지는 자동으로 만들어 준다 boolean, long
		
	}
	
}



/*
 * 
 * insert into users (email, password, name) values ('jungh.lee@kewtea.com','1234','jungh'),
    ('admin@kewtea.com','1000','john smith'), ('developer@kewtea.com','1111','andrew');

 * 
 */
