package com.kewtea.modelold;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "userrecord")
public class UserRecord {
	/*
	 * types:
	  	- 	transaction 
	 	-	(order)-(task-action)
	 * 
	 */
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)
	public int timezone;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long timestamp;
	
	@Column(nullable = true)
	public int lastupdatedtimezone;
	
	@Column(nullable = true)
	public long lastupdated;
	
	@Column(nullable=true)
	public String modelVersion;
	
	
	
	
}
