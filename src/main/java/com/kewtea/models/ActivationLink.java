package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "useractlinks")
public class ActivationLink {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@Column(nullable = true)
	public long uid;
	@Column(nullable = true)
	public String url;
	@Column(nullable = true)
	public long createdTime;
	@Column(nullable = true)
	public int status;
	
	public ActivationLink(long uid, String url, long createdTime){
		this.uid=uid;
		this.url=url;
		this.createdTime=createdTime;
	}
	
	public ActivationLink(long uid, String url){
		this.uid=uid;
		this.url=url;
		this.createdTime=System.currentTimeMillis();
	}
	
	//over 6 hours invalidated
	/*
	public long compare(long time1, long time2, long gap){
		long result = time1-time2;
		if(result>gap){}
		return result;
	}
	*/
}
