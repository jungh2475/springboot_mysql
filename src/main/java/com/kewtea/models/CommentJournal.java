package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "commentsjournal")
public class CommentJournal extends Content {
	
	//추가 되는 항목들
	@Lob
	@Column(columnDefinition = "TEXT")
	public String hgroup;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body; 
	
	
	@Column(nullable = true)
	public String sourcefrom;   //sourceFrom: journal(default), facebook,....
	@Column(nullable = true)
	public String collections;
	@Column(nullable = true)
	public String keywords;
	@Column(nullable = true)
	public String referencetype;
	@Column(nullable = true)
	public String referenceurl;
	
	@Column(nullable = true)
	public String contenttype;  //? 현재는 필요 없음 journal_dataPage?

}
