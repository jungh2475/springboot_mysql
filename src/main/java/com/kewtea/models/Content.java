package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Content {
/*
 *  common fields
 		- id(long)
 	[relationship]
 		- parentid
 	[about recoding 상태기록]
 		- tzone(int), timestamp(long) : created,updated
 		- locale*(String: en-US, enUS)
 		- modelver*(Str)
 	[about publishing/visibility/editibility]
 		- active(bool), draft(bool), flag(bool), snapshot*(bool)
 		- readaccess(Str), writeaccess(Str)
 	
 	실제 중요한 내용물은 해당 객체에서 정의하도록 한다 : name, body, type, class, format, next, before, urlid....	
 	1+3+7+4+2=17개 항목들 
 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	//3 where it belongs?
	@Column(nullable = true)
	public String modelver;
	@Column(nullable = true)
	public long parentid;
	@Column(nullable = true)
	public String projectscope;
	
	//7  when-how created?
	@Column(nullable = true)
	public int createdtzone;
	@Column(nullable = true)
	public long createdtimestamp;
	@Column(nullable = true)
	public long createdby;
	@Column(nullable = true)
	public int updatedtzone;
	@Column(nullable = true)
	public long updatedtimestamp;
	@Column(nullable = true)
	public long updatedby;
	@Column(nullable = true)
	public String locale;
	
	//4 content visibility 
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean snapshot;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean flag;
	//2 editibility
	@Column(nullable = true)
	public String readaccess;
	@Column(nullable = true)
	public String writeaccess;
	
}
