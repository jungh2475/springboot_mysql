package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data  //@Getter @Setter
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "contentsjournal")
public class ContentJournal extends Content {
/*
 * 
 부모로부터 물려받은 매핑 정보를 재정의 : http://yellowh.tistory.com/123
 		@AttributeOverrides({
	        @AttributeOverride(name = "id", column = @Column(name = "MEMBER_ID")),
	        @AttributeOverride(name = "name", column = @Column(name = "MEMBER_NAME"))
		})

*/	
	//추가 되는 항목들 
	@Lob
	@Column(columnDefinition = "TEXT")
	public String hgroup;
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	
	/*
	 * extended elements : 	contenttype,datefrom,dateto,url
	 * 						visibility, aliasurl
	 * 
	 */
	@Column(nullable = false)
	public long uid;
	@Column(nullable = true)
	public String uniquename; //사용자 id안에서만 unique하여야 한다 , 날짜 기준 값으로 할당함 e.g. 20160601, s20160601-20160603-2
	public String contenttype; //datePage, urlPage, summaryPage(type)
	@Column(nullable = true)
	public int datefrom;    //optional for datePage and summaryPage
	@Column(nullable = true)
	public int dateto;      //대부분 날짜형식 데이터에서는 비어 있음. 기간이 있는 summary만 현재 필요함. 향후 병합모드에서 필요 할수도 있음 
	@Column(nullable = true)
	public String url;   //optional for urlPage
	@Column(nullable = true)
	public long next;   //nextPage id
	@Column(nullable = true)
	public long prev;   //previousPage id
	@Column(nullable = true)
	public String collections;
	@Column(nullable = true)
	public String keywords;
	
	
	
}
