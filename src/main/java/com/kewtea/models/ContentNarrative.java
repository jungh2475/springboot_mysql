package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data  //@Getter @Setter
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "contentsnarrative")
public class ContentNarrative extends Content {
	
	//String type = analytic(chart), episode, timeplan, scene(url, html)
	
	@Column(nullable = true)
	public String type;
	
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	
	
}


/*

	--------------------------------
	1)theme_json
	
	"_id": //서버에서 할당해 줄 것임
	"type":"theme",	
	"manifest_version": "1" ,
	 
	"name":   ,  //theme_name
	"description":  ,
	"theme_version":   ,	
	"preview_url":   ,  //예시화면 
	"author":  ,
	"require": , //의존하는 외부 라이브러리 : jquery, d3-minVersion포함하여 선언 ["jquery":"1.3+", "d3js":"2.0"] 
	"scope": "free",  //유료, 혹은 사용 가능한 조건 
	"data-min-max":  , //허용되는 데이터 크기 (최소량 혹은 최대량?)
	"render_config_sample":   ,  //나중에 주입될 파라미터 예시 
	
	
	---------------------------------
	
	2)content_json (사용자가 theme 스펙에 따라서 작성)
	
	"type":"content",
	"manifest_version": "1" ,
	
	"_id": //서버에서 할당해 줄 것임 
	"datasrc_type":  ,//gSheet, jsonfile
	"datasrc_url":  ,
	"render_theme":  ,  //_id or(theme-name,theme_version)
	"duration": , 
	"render_config":  , //여기에 원하는 custom propeties값들을 넣어서 전달 하면 된다 {a:"", b:"".....}
	
	
	-----------------------------
	3)실제 renderer에 전달되는 render_json 
	  (theme_renderer가 받아서 동작시키는 설정 값)
	
	(공통)
	jsondata
	duration, steps
	.....
	추가 옾션들을 자유자재로 넣어 주세요 
	
	

*/