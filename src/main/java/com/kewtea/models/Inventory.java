package com.kewtea.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data  //@Getter @Setter
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "inventories")
public class Inventory extends Content {
	
	public String type; //project, collection, keyword, url, word
	public long uid;
	public long urlid;
	public String url;
	public String name;
	public String uniquename;
	
}
