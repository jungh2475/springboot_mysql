package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Record {
	/*
	 * record와 비교해서 없는 것은 updatedtzone, updatedtimestamp, updatedby, draft,
	 * 실제로 쓸때 본인이 추가로 type field를 넣어서 하부 데이터 타입들을 정해 주시면 됨 
	 * 
	 * 
	*/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	//3 where it belongs?
	@Column(nullable = true)
	public String modelver;
	@Column(nullable = true)
	public long parentid;
	@Column(nullable = true)
	public String projectscope;
	
	//7  when-how created? ...no model for update
	@Column(nullable = true)
	public int createdtzone;
	@Column(nullable = true)
	public long createdtimestamp;
	@Column(nullable = true)
	public long createdby;
	@Column(nullable = true)
	public String locale;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean snapshot;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean flag;
	//2 editibility
	@Column(nullable = true)
	public String readaccess;
	@Column(nullable = true)
	public String writeaccess;

}
