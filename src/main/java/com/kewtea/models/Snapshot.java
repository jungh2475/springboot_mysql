package com.kewtea.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data  //@Getter @Setter
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "snapshots")
public class Snapshot extends Content {
	
	public String table; //tableName or modelName
	public long in;
	public long out;
	//public int value;


}
