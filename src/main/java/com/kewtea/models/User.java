package com.kewtea.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor  
@NoArgsConstructor
@Table(name = "users")
public class User extends org.springframework.security.core.userdetails.User implements UserDetails {

	private static final long serialVersionUID = 1L;

/*
 * 
 * 새로이 추가된 것은 직접 spring의 userClass, userdetailsInterface 포함 
 * locale, tzone/timestamp, modelver 추가 
 * credit,거래내역(trasaction)은 userRecord에서 참조할것 
 * 
 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty 
	@Email
	@Column(unique=true, nullable=false)
	public String email;
	
	//@JsonIgnore  //Rest에서 패스워드 노출되는 것을 막을려면, 이렇게 한다 .., EncodedPassword면 60char이상 값이 저장될것임 
	@Size(min = 4)
	public String password;
	
	@Column(nullable = true)
	public String name;  //John Smith
	
	@Column(unique=true, nullable=true)
	public String uniquename;   // url-mapped
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot; 
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;  //이 사용자 요주의 인물+문제있다고 보고한 경우 on
	
	//extuserjson:  (user:pass)
	
	public User(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() { 
		//interface GrantedAuthority -> String getAuthority()
		return null;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

}
