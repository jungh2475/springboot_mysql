package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "userrecords")
public class UserRecord extends Content {
	
	//추가할 항목들 type, tag, msg, reference_int 
	
	@Column(nullable = true)
	public long uid;
	
	@Column(nullable = true)
	public String record_template_id;  // optional , log format matching
	
	@Column(nullable = true)
	public long did;  // device id, optional
	
	@NotEmpty
	@Column(nullable=false)
	public String level;  //log level: e, i, w, d(developer)
	
	@NotEmpty
	@Column(nullable=false)
	public String type; // like title: type: client-ip, cookie(expire,authority-or-role), clientid(=deviceid+ serviceid-or-swappid)	
	
	@Column(nullable = true)
	public String tag; //like keywords
	
	@NotEmpty
	@Column(nullable = false)
	public String msg;  //value here
	
	@Column(nullable = true)
	public double ref_id;  //value here
	

}
