package com.kewtea.models.json;

public class Hgroup {
	
	public String title;
	public String label;
	public String subTitle;
	public String imgUrl;
	
	public Hgroup(String title, String subTitle, String label, String imgUrl){
		this.title=title;
		this.subTitle=subTitle;
		this.label=label;
		this.imgUrl=imgUrl;
	}

}
