package com.kewtea.models.json;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class JsonBox<T> {
	// 이 클래스는 parent에서 json이 들어 있는 member를 추출하여  Map<Long, T>안에 저장해둔다(load(List<ParentObject>, parentObject, jsonFieldName). 
	// 그리고 그안의 구성요소를 요청하면 추출해서 준다(extrat(fieldName) -return Map<Long,String>) 
	// K is Content, T is contetExtra
	
	private Map<Long,T> map;
	private T t;  //public Class<T>  tClass;
	private Gson gson;
	
	
	public <T> JsonBox(){
		map=new HashMap<>();
	}
	
	public <K,S> int load(List<K> parents,Class<K> kClass, String memberName){  //k.getClass()
		try {
			if(parents.size()<1) { return -1;} //print log and return
			if(parents.get(0).getClass()!=kClass) {return -1;}  //print log and return
			
			Field id=kClass.getField("id");  //k.getClass()
			Field field=kClass.getField(memberName);
			String temp=null;
			map.clear();//clear map
			
			for(K item : parents){
				Long temp_id=id.getLong(item);
				temp=(String) field.get(item);//item
				t=(T) gson.fromJson(temp, t.getClass());  //Class<T>, t.getClass()
				map.put(temp_id, t);
			}
			
			
			
		} catch (Exception e) {   //NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
			e.printStackTrace();
		} 
		
		return map.size();
	}
	
	
	public Map<Long,String> extract(String fieldName){
		
		Map<Long, String> result = new HashMap<>();
		Field field = null;
		String temp=null;
		try {
			field = t.getClass().getField(fieldName); //fieldName을 가지고 있는가?
			for(Long key: map.keySet()){
				temp=field.get(map.get(key)).toString();
				result.put(key,temp); //key, map.get(key)
			}
			
		} catch(Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return result;
	}
	
	
	
	
}
