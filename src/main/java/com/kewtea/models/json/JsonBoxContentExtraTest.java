package com.kewtea.models.json;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.kewtea.models.ContentJournal;
import com.kewtea.repositories.ContentJournalRepository;

import unittest.SubjectClass;


//spring-cli :    onosx : brew install springboot 
//spring test app.groovy tests.groovy, spring version, spring run hello.groovy

//javac -cp .:"/Applications/IntelliJ IDEA 13 CE.app/Contents/lib/*" SetTest.java
//java -cp .:"/Applications/IntelliJ IDEA 13 CE.app/Contents/lib/*" org.junit.runner.JUnitCore SetTest

//java double -> mysql @Column(precision=8, scale=2) 
//private float hourlyRate;

//export classpath : /usr/share/java/ or /Library/Java/Extensions

//@Test(expected = IllegalArgumentException.class)


public class JsonBoxContentExtraTest {
	
	@Autowired
	ContentJournalRepository  jc_repo;
	
	@Test
	public void simpletest(){
		//to run within eclipse unit test
		SubjectClass sc= new SubjectClass("iamnow");
		
		
		System.out.println("i am now....");  //1
		assertEquals(sc.printMessage(), "iamnow1");  //2
		
	}
	
	@Test
	public void test_jsonbox_contentjournal_extra(){
				
		List<ContentJournal> journalContents= new ArrayList<>();
		//journalContents=jc_repo.findAll();
		journalContents.add(new ContentJournal());
		System.out.println("inputsize is "+journalContents.size());
		
		JsonBox<JsonContentExtra> jsContentExtra = new JsonBox<>();
		jsContentExtra.load(journalContents, ContentJournal.class, "jsonextra");
		Map<Long, String> result = jsContentExtra.extract("hello");
		System.out.println("extracted size:"+result.size());
		assertEquals("onetwo", "onetwo");
	}
	
	

}
