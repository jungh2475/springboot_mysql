package com.kewtea.models.json;

public class JsonContentExtra {
	
	public String reference;
	public String hello;
	public double longitute;
	public double latitude;
	public double height;
	public float accuracy;
	public int ver=1;
	
	public JsonContentExtra(String r, String h, double longi, float acc){
		this.reference=r;
		this.hello=h;
		this.longitute=longi;
		this.accuracy=acc;
	}

}
