package com.kewtea.models.json;

public class JsonNaRenderTheme {
	public String name;
	public String description;
	public int theme_version;
	public String preview_url;
	public String author;
	public String require;
	public String scope;
	public String data_type;
	public String data_range;
	public String render_config;
}
