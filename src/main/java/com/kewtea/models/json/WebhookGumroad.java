package com.kewtea.models.json;

public class WebhookGumroad {
//https://gumroad.com/webhooks
	
	public String email;
	//email (the email of the buyer)
	//full_name (if present, the name of the buyer)
	//price (the price paid, in cents)
	//variants (if present, a dictionary {'size' : 'large', 'color' : 'red'})
	//offer_code (if present)
	//test (if you are buying your own product, for testing purposes)
	//custom fields (if present, a dictionary {'name' : 'john smith', 'spouse name' : 'jane smith'})
	//shipping information (if present, a dictionary)
	//quantity (if the product is a physical product)
	//product_id
}


/*

reply: return an HTTP status code of 200,  text/plain content type, contain only a URL (in the body)

*/