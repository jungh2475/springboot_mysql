package com.kewtea.models.wordpress;

public class WpLink {

}

/*
	describe wp_links;
+------------------+---------------------+------+-----+---------------------+----------------+
| Field            | Type                | Null | Key | Default             | Extra          |
+------------------+---------------------+------+-----+---------------------+----------------+
| link_id          | bigint(20) unsigned | NO   | PRI | NULL                | auto_increment |
| link_url         | varchar(255)        | NO   |     |                     |                |
| link_name        | varchar(255)        | NO   |     |                     |                |
| link_image       | varchar(255)        | NO   |     |                     |                |
| link_target      | varchar(25)         | NO   |     |                     |                |
| link_description | varchar(255)        | NO   |     |                     |                |
| link_visible     | varchar(20)         | NO   | MUL | Y                   |                |
| link_owner       | bigint(20) unsigned | NO   |     | 1                   |                |
| link_rating      | int(11)             | NO   |     | 0                   |                |
| link_updated     | datetime            | NO   |     | 0000-00-00 00:00:00 |                |
| link_rel         | varchar(255)        | NO   |     |                     |                |
| link_notes       | mediumtext          | NO   |     | NULL                |                |
| link_rss         | varchar(255)        | NO   |     |                     |                |
+------------------+---------------------+------+-----+---------------------+----------------+
13 rows in set (0.00 sec)

*/