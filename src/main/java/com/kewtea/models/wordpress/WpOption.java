package com.kewtea.models.wordpress;

public class WpOption {

}

/*
describe wp_options;
+--------------+---------------------+------+-----+---------+----------------+
| Field        | Type                | Null | Key | Default | Extra          |
+--------------+---------------------+------+-----+---------+----------------+
| option_id    | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| option_name  | varchar(191)        | NO   | UNI |         |                |
| option_value | longtext            | NO   |     | NULL    |                |
| autoload     | varchar(20)         | NO   |     | yes     |                |
+--------------+---------------------+------+-----+---------+----------------+

select option_id, option_name, autoload  from wp_options;
+-----------+---------------------------------------+----------+
| option_id | option_name                           | autoload |
+-----------+---------------------------------------+----------+
|         1 | siteurl                               | yes      |
|         2 | home                                  | yes      |
|         3 | blogname                              | yes      |
|         4 | blogdescription                       | yes      |
|         5 | users_can_register                    | yes      |
|         6 | admin_email                           | yes      |
|         7 | start_of_week                         | yes      |




*/