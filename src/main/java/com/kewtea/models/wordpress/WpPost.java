package com.kewtea.models.wordpress;

public class WpPost {

}

/*

 Field                 | Type                | Null | Key | Default             | Extra          |
+-----------------------+---------------------+------+-----+---------------------+----------------+
| ID                    | bigint(20) unsigned | NO   | PRI | NULL                | auto_increment |
| post_author           | bigint(20) unsigned | NO   | MUL | 0                   |                |
| post_date             | datetime            | NO   |     | 0000-00-00 00:00:00 |                |
| post_date_gmt         | datetime            | NO   |     | 0000-00-00 00:00:00 |                |
| post_content          | longtext            | NO   |     | NULL                |                |
| post_title            | text                | NO   |     | NULL                |                |
| post_excerpt          | text                | NO   |     | NULL                |                |
| post_status           | varchar(20)         | NO   |     | publish             |                |
| comment_status        | varchar(20)         | NO   |     | open                |                |
| ping_status           | varchar(20)         | NO   |     | open                |                |
| post_password         | varchar(255)        | NO   |     |                     |                |
| post_name             | varchar(200)        | NO   | MUL |                     |                |
| to_ping               | text                | NO   |     | NULL                |                |
| pinged                | text                | NO   |     | NULL                |                |
| post_modified         | datetime            | NO   |     | 0000-00-00 00:00:00 |                |
| post_modified_gmt     | datetime            | NO   |     | 0000-00-00 00:00:00 |                |
| post_content_filtered | longtext            | NO   |     | NULL                |                |
| post_parent           | bigint(20) unsigned | NO   | MUL | 0                   |                |
| guid                  | varchar(255)        | NO   |     |                     |                |
| menu_order            | int(11)             | NO   |     | 0                   |                |
| post_type             | varchar(20)         | NO   | MUL | post                |                |
| post_mime_type        | varchar(100)        | NO   |     |                     |                |
| comment_count         | bigint(20)          | NO   |     | 0            
 
 select ID, post_author, post_date, post_title, post_status, post_type, post_mime_type, comment_count from wp_posts;
+----+-------------+---------------------+--------------+-------------+-----------+----------------+---------------+
| ID | post_author | post_date           | post_title   | post_status | post_type | post_mime_type | comment_count |
+----+-------------+---------------------+--------------+-------------+-----------+----------------+---------------+
|  1 |           1 | 2017-04-22 00:48:42 | Hello world! | publish     | post      |                |             1 |
|  2 |           1 | 2017-04-22 00:48:42 | Sample Page  | publish     | page      |                |             0 |
+----+-------------+---------------------+--------------+-------------+-----------+----------------+---------------+
2 rows in set (0.00 sec)
 
*/