package com.kewtea.models.wordpress;

public class WpPostmeta {

}

/*
	
	| Field      | Type                | Null | Key | Default | Extra          |
+------------+---------------------+------+-----+---------+----------------+
| meta_id    | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| post_id    | bigint(20) unsigned | NO   | MUL | 0       |                |
| meta_key   | varchar(255)        | YES  | MUL | NULL    |                |
| meta_value | longtext            | YES  |     | NULL    |                |
+------------+---------------------+------+-----+---------+----------------+

	select * from wp_postmeta;
+---------+---------+-------------------+------------+
| meta_id | post_id | meta_key          | meta_value |
+---------+---------+-------------------+------------+
|       1 |       2 | _wp_page_template | default    |
+---------+---------+-------------------+------------+
	
	
*/