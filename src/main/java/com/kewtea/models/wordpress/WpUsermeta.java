package com.kewtea.models.wordpress;

public class WpUsermeta {

}

/*
	describe wp_usermeta;
+------------+---------------------+------+-----+---------+----------------+
| Field      | Type                | Null | Key | Default | Extra          |
+------------+---------------------+------+-----+---------+----------------+
| umeta_id   | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| user_id    | bigint(20) unsigned | NO   | MUL | 0       |                |
| meta_key   | varchar(255)        | YES  | MUL | NULL    |                |
| meta_value | longtext            | YES  |     | NULL    |                |
+------------+---------------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> select * from wp_usermeta;
+----------+---------+-----------------------+---------------------------------+
| umeta_id | user_id | meta_key              | meta_value                      |
+----------+---------+-----------------------+---------------------------------+
|        1 |       1 | nickname              | wpadmin                         |
|        2 |       1 | first_name            |                                 |
|        3 |       1 | last_name             |                                 |
|        4 |       1 | description           |                                 |
|        5 |       1 | rich_editing          | true                            |
|        6 |       1 | comment_shortcuts     | false                           |
|        7 |       1 | admin_color           | fresh                           |
|        8 |       1 | use_ssl               | 0                               |
|        9 |       1 | show_admin_bar_front  | true                            |
|       10 |       1 | locale                |                                 |
|       11 |       1 | wp_capabilities       | a:1:{s:13:"administrator";b:1;} |
|       12 |       1 | wp_user_level         | 10                              |
|       13 |       1 | dismissed_wp_pointers |                                 |
|       14 |       1 | show_welcome_panel    | 1                               |
+----------+---------+-----------------------+---------------------------------+
14 rows in set (0.00 sec)

*/