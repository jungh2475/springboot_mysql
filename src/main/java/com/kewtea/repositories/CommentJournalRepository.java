package com.kewtea.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.kewtea.models.CommentJournal;


public interface CommentJournalRepository extends JpaRepository<CommentJournal, Long> {
	List<CommentJournal> findById(long id);
	CommentJournal findOne(Long id);
	List<CommentJournal> findByCreatedbyAndDatereferenceBetween(@Param("uid") long uid, @Param("from") int datestart, @Param("to") int dateend);
	List<CommentJournal> findByCreatedbyAndCollectionsContainingAndDatereferenceBetween(@Param("uid") long uid, @Param("collection") String collection, @Param("from") int datestart, @Param("to") int dateend);
	
}
