package com.kewtea.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.kewtea.models.CommentSphouse;

/*
 * 
 	findBy - productId, 
 	add with(productId)
 	
 */

@RepositoryRestResource
public interface CommentSphouseRepository extends JpaRepository<CommentSphouse, Long> {
	
	
}
