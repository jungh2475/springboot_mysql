package com.kewtea.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.kewtea.models.ContentJournal;

/*	read this ; http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods
	http://localhost:8080/api/repository/contentJournals/search/findByUidAndDatefromBetween?uid=1&from=20160531&to=20160602
	get: curl -i -X GET -H "Accept:application/json" http://localhost:8080/resources/contentJournals/
	post : curl -i -X POST -H "Accept:application/json" http://localhost:8080/resources/contentJournals/ -d '{"field":"value"}'
	
*/

@RepositoryRestResource  //(collectionResourceRel = "journal", path = "journal")
public interface ContentJournalRepository extends JpaRepository<ContentJournal, Long> {
	

	//@Query("SELECT x.id FROM ContentJournal WHERE LOWER(x.uid) = 'jungh' ORDER BY x.from")  
	@Query("SELECT x.id FROM ContentJournal x WHERE x.uid = :userid AND x.datefrom BETWEEN :fromdatestart AND :fromdateend ORDER BY x.datefrom")
	List<Long> findGetIdOnly(@Param("userid") String userid, @Param("fromdatestart") long fromdatestart, @Param("fromdateend") long fromdateend);
	
	@Query("SELECT t FROM ContentJournal t")
	public List<ContentJournal> findAllIdOnly();
	
	ContentJournal findById(long id);
	
	List<ContentJournal> findByUidAndDatefromBetween(@Param("uid") long uid, @Param("from") int fromdatestart, @Param("to") int fromdateend);
	
	List<ContentJournal> findByContenttypeAndCreatedby(String contenttype, long createdby);
}


/*	
	@RepositoryRestResource (collectionResourceRel = "journal", path = "journal")
	(기본이 Default이어서 쓰던 안쓰던 모든 Repository가 노출된다. 바꾸고 싶으면 RepositoryDetectionStrategy에서 Annotation(@)으로 해준다
	@RestResource(path = "findallpage", rel = "findallpage")
 	
 	http://docs.spring.io/spring-data/rest/docs/current/reference/html/
		application.properties => spring.data.rest.basePath=/api
	201 Created - for POST and PUT requests, 204 No Content - for PUT, PATCH, and DELETE
					HATEOAS, search 
 	
 	
 	spring-data-rest also use Param() to directly map on rest api url
	//List<ContentJournal> findByUidAndDatefromBetween(@Param("uid") long uid, @Param("from") int fromdatestart, @Param("to") int fromdateend);
 	
 	built-int default implementations 
 	 
	 * boolean exists(ID)
	 * long count()
	 * Iterable<T> findAll() or (Iterable<ID>)
	 * findOne(ID)
	 * ContentJournal findOne(Long id); //정의 안해 주어도 동작함
	 * S save(S)  //create or update
	 * void delete(ID)
	 * save(T), findOne(Id;T), findAll(List|Page(Pageable or PageRequest)), count(Long) delete(void), exists(boolean),
	List<T> findXBy, Page<T> findX2By (둘이 이름이 달라야지 RestApi자동생성시에 충돌을 안함) 
	
	자동 method 만들기 : http://docs.spring.io/spring-data/jpa/docs/current/reference/html/  go to Table 4. Supported keywords inside method names Keyword
	
	findByFirstnameIgnoreCase, OrderByLastnameDesc, ByActiveTrue
	//Page하고 List의 method명이 같아서 충돌시에는 path를 다르게 해서 호출해 준다
	@Query("select u from User u where u.emailAddress = ?1")
	@Query("select u from User u where u.firstname like %?1")
	@Query("select u from User u where u.firstname = :firstname or u.lastname = :lastname")
	User findByLastnameOrFirstname(@Param("lastname") String lastname, @Param("firstname") String firstname);
	이런식으로 이름으로 자동 query를 만들기때문에 JPA Model.property에 대문자 및 _ 그리고 키워드들을 쓰지 말것!
	
 */

