package com.kewtea.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.kewtea.models.ContentNarrative;

/*
 * 
 	analyticView
 	campaign  : crud
 	display -timeline 
 
 */

@RepositoryRestResource
public interface ContentNarrativeRepository extends JpaRepository<ContentNarrative, Long> {

}
