package com.kewtea.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.kewtea.models.SysRecord;
import com.kewtea.service.MailService;

/*
 * 
 	linux command에서 crontab edit로 명령을 날려주어도 되나. 이 서비스로 자동화를 해본다. 이 경우 장점은 문제가 있으면 이메일을 보낸다는 것이고,
 	이 spring 서비스가 죽으면 mysql backup도 죽기때문에, 이를 위해 api ping을 때리는 서비스를 만들어놓아야 한다 
 	(google apps script에서 실행한다 - 맨 아례에 예제 코드가 있음 or /src/main/resources/gappsscript/apitest.js(gs))
 	
 	
 	crontab -u userid -e  // -l	(list user's crontab),	-r	(delete user's crontab)
 		=>분 시 일 월 (요일 혹은 주번호) min,hour,dayOfMonth,month,dayOfWeek
 		=>1 13 * * Mon-Wed
 		=>0 0 * * * /path/to/mysqldump -u rootoradmin -p1234 kew1db or –all-databases ... > /path/to/backup/mydata_$( date +"%Y_%m_%d" ).sql 
 			which mysqldump =>/usr/local/mysql/bin//mysqldump  ~/backup/mysql/mysql_$( date +"%Y_%m_%d" ).sql
 			0 1 * * * /usr/local/mysql/bin/mysqldump -h mysql.host.com -uusername -ppassword --opt database > /path/to/directory/filename.sql
 			
 		=>0 5 1 * * /home/al/bin/mailWebSitesToKim.sh
 			=>#!/bin/sh, mail -s "Current customer directories" kim@example.com < listOfWebSites.txt
 	
 		mac osx mailserver: http://www.developerfiles.com/how-to-send-emails-from-localhost-mac-os-x-el-capitan/
 		ubuntu sendmail ; sudo apt-get install mailutils => sudo nano /etc/postfix/main.cf : inet_interfaces = loopback-only
 			=>or inet_interfaces = localhost => sudo service postfix restart
 			=> echo "This is the body of the email" | mail -s "This is the subject line" user@example.com
 			sudo aptitude install postfix or sudo apt-get install sendmail
 			
 	mysql restore & restart
 	stop & restart service: /etc/init.d/mysql stop, /etc/init.d/mysql stop 
 	기존것을 삭제하고 다시 덮어 쓰기 
 		LOGIN => mysql -u [user] -p
 		mysql> drop database <db_name>;
 		mysql>CREATE DATABASE mydatabase CHARACTER SET utf8 COLLATE utf8_general_ci;
 		exit mysql
 		mysql -u [new_user] -p [database_name] kew1db < [file_name].sql  아니면 
 		mysql> source kew1db myfile.sql
 		
 	-------------------------
 	
 	당분간은 주1회 fullbakup database=>kew1db.all.us1.20160926.sql
 	기록은 SysRecord type=backup tag=mysql,localhost msg=fileName int=size(?)
 	
 	--------------------------
 	주기적으로 system db file을 backup해 두고, 원할때 restore한다 =>at main @SpringBootApplication + @EnableScheduling 
 	using the Runtime class and the Process class as a separate process
 	->getInputStream() and getErrorStream()
 	
 	- commands[0] : backup whole MySQL,
 		mysqldump --single-transaction --all-databases > dump-$( date '+%Y-%m-%d_%H-%M-%S' ).sql -u root -p
		or crontab: 0 1 * * * /usr/bin/mysqldump --all-databases >......
		which mysqldump to confirm the correct path to the command
		For InnoDB tables, it is possible to perform an online backup that takes no locks on tables using the --single-transaction option		
 		--flush-logs --master-data=2 // --master-data option causes mysqldump to write binary log information to its output
 			
 	- commands[1]* : backup some databse MySQL,
 		mysqldump -u [username] -p[password] -h [host] [databaseName] > [backup-name].sql
 		mysqldump -u username -ps3cr1t -h localhost danceLeaders > 1266861650-danceLeaders.sql

 	- commands[2]* : incremental database backup MySQL
 		incremental backup by flushing the logs to begin a new binary log file. For example, executing a mysqladmin flush-logs command creates gbichot2-bin.000008.
 		
 		mysqldump --single-transaction --flush-logs --master-data=2 --all-databases --delete-master-logs > backup_sunday_1_PM.sql
 		(서버가 로그를 기록하도록하고 로그를 저장한다 : --log-bin : so the MySQL server should always be started with the --log-bin option to enable that log.)
 		로그생성 명령어: mysqladmin flush-logs 
 		
 	- commands[3] : backup some tables MySQL,
 		mysqldump -u [username] -p[password] -h [host] [databaseName] [tableName] > [backup-name].sql

 	- commands[4] : stop >backup >restart MySQL
 		1. /etc/init.d/mysqld stop  //or search location using 'find / -name mysql'
 		2. copy or backup
 		3. /etc/init.d/mysql start
 	
 	- commands[5] : restore MySQL
 		mysql -u [username] -p [password] < backupFile.sql
 		
 		//DROP DATABASE IF EXISTS tutorial_database;
 		//CREATE DATABASE mydatabase CHARACTER SET utf8 COLLATE utf8_general_ci;
 		
 	
 	- commands[8] : intsall, start, stop ElasticSearch
 
 	backup file 이름 정책 : dataName.tableName.regionId.backupDate-1a/1b.sql
 	string[] = string.split('.')
 
 */

@Component
public class DatabaseBackupRestore {
	
	private static final Logger log = LoggerFactory.getLogger(DatabaseBackupRestore.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Autowired
	private MailService mail;
	@Autowired
	private SysRecordRepository sysrecordRepository;
	
	public String[] commands;
	public int index;
	public String sql_ip_port;
	public String sql_user;
	public String sql_password;
	public String sql_database;
	public String sql_path;
	public int backup_frquency=2;
	public boolean backup_runFlag=false;
	public boolean restore_runFlag=false;
	//public String sql_tableName;
	
	private String runCommand(String command){
		
		String s = null;
		StringBuilder msg_normal = new StringBuilder();
		StringBuilder msg_error = new StringBuilder();
		
		try{
			Process p = Runtime.getRuntime().exec(command);  //p.waitFor();
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			
			while ((s = stdInput.readLine()) != null) {
                System.out.println(s);  //Here is the standard output of the command:\n
                msg_normal.append(s).append("::");
            }
			stdInput.close();
			
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);  //Here is the standard error of the command (if any):
                msg_error.append(s).append("::");
            }
            stdError.close();
            
			System.exit(0);
			
			
		}catch(IOException e){
			System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
            System.exit(-1);
		}
		
		return msg_normal.append(msg_error.toString()).toString(); //return null;
	}
	
	//constructor
	public DatabaseBackupRestore(){
		sql_ip_port="localhost";
		sql_database="kew1db";
		sql_user="root";
		sql_password="1234";
	}
	
	public DatabaseBackupRestore(String ip_port,String databaseName, String uid, String password){
		sql_ip_port=ip_port;
		sql_database=databaseName;
		sql_user=uid;
		sql_password=password;
	}
	/*
	public DatabaseBackupRestore(String[] init_commands){
		commands = new String[init_commands.length];
		for(int i=0; i<commands.length;i++){
			commands[i]=init_commands[i];
		}
	}
	*/
	
	public void setBackupSchedule(int option){
		
		if(option == 0 || option == 1 || option == 2) { backup_frquency = option;}
		
		//0=every 24hours, 03:30 AM
		//1=every Monday Early Morning, 3:30 AM
		//2=once a Month (2nd Day of every Month)
		//defaultFileName = mysql_kew_[dbName]_[date]
		//String fName=fileName;
		//if(fName.equals("")) fName="mysql_kew_1.sql";
		//String commandRun=commands[0]+"mysqldump";
	}
	
	//cron : "sec min hr day month Mon-Sun (Year) 마지막 7th Year는 생략 가능: empty, 1970-2099
	//cron : "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
	//cron : "0 * 6,19 * * *" = 6:00 AM and 7:00 PM every day.
	//cron : "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
	//cron : "0 0 0 25 12 ?" = every Christmas Day at midnight
	
	//@Scheduled(fixedRate = 86400000)    //milliseconds 24hr*60min*60sec*1000=86400000 //or @Scheduled(cron=". . ."), fixedDelay
	//@Scheduled(cron="0 30 3,4 * * MON")  //every monday 3 and 4 am
	//@Scheduled(cron="0 30 3,4 * * *")  //every morning 3 and 4 am 
	
	@Scheduled(cron="0 30 3 * * *")  //every morning 3 am
	public void backup(){
		switch(backup_frquency){
		case 0:
			//만약 시간이 3시면 backup 실
			break;
		case 1:
			break;
		case 2:
			break;
		default:
			break;
		}
		log.info("The time is now {}", dateFormat.format(new Date()));
		//if error - send email
		//SysRecord에서 최근 작업 시간을 읽어서, 동작시켜야 할때인지 알아 보고 backup 여부를 결정 한다 		
		
	}
	
	private String findLastBackupFileName(int option){  //from SysRecord Table
		List<SysRecord> sysrecords = new ArrayList<>();
		sysrecords= sysrecordRepository.findFirst5ByTag("Mysql");
		if(sysrecords.size()!=0) return sysrecords.get(0).msg;
		else return null;
	}
	
	
	public void restore(String inputFileName, String databaseName){
		//mysql -u [username] -p [password] < backupFile.sql
		StringBuilder commandBuilder= new StringBuilder();
		commandBuilder.append("mysql -u ").append(sql_user).append(" -p").append(sql_password)
			.append(" < ").append(inputFileName);
		if(databaseName.contentEquals("")){
			
		}
		runCommand(commandBuilder.toString()); //이렇게 다시 rebuild하면 시간이 오래 걸린다 ?
	}
	
	private void sendEMail(String receiver,String title,String msg){
		try {
			mail.sendMail(receiver, title, msg);
		}catch(MessagingException e){
			log.error("failed to send email for backup...?restore");
			e.printStackTrace();
		}
	}
	
}
