package com.kewtea.repositories;


import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.kewtea.models.CommentJournal;
import com.kewtea.models.ContentJournal;
import com.kewtea.models.User2;

//사이트가 처음 스타트할때 리스너로 연락받아서 데이터 베이스에 값을 적어 놓는다. 개발중에 편리함. 나중에 application.properties에서 db 자동 삭제를 꺼주여야 함 
@Component
public class DatabaseLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	private static final Logger log = Logger.getLogger(DatabaseLoader.class.getName());
	
	@Autowired
	private UserRepository user2repo;	
	@Autowired
	private ContentJournalRepository content_journal_repo;
	@Autowired
	private CommentJournalRepository comment_journal_repo;
	
	@Autowired
    private PasswordEncoder passwordEncoder;  //아래와 동일 
	//PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		log.info("database loader running....");
		// 개발중 db를 매번 flush하고 다시 저장하기 위해 만든 문구로, production에서 update로 세팅을 바꾸고 제거해야 한다 
		User2 user2_1 = new User2();
		user2_1.email="jungh.lee@kewtea.com";
		user2_1.name="jungh";
		user2_1.uniquename="jungh";
		//user2_1.password="1111";
		user2_1.password=passwordEncoder.encode("1111");
		user2repo.save(user2_1);
		
		User2 user2_2 = new User2();
		user2_2.email="admin@kewtea.com";
		user2_2.name="john";
		user2_2.uniquename="john";
		//user2_2.password="1000";
		user2_2.password=passwordEncoder.encode("1000");
		user2repo.save(user2_2);
		
		User2 user2_3 = new User2();
		user2_3.email="developer@kewtea.com";
		user2_3.name="andrew";
		user2_3.uniquename="andrew";
		//user2_3.password="abcd";
		user2_3.password=passwordEncoder.encode("abcd");
		user2repo.save(user2_3);
		
		
		/*여기서 gson을 실험해 본다 
		Gson gson = new Gson();
		content_1.hgroup =gson.toJson(new Hgroup("이정환","abc","ds","sad"));
		//gson.fromJson(json, classOfT);
		//JsonArray -> List 변환 방식 , 다중 log가 들어올때, 이럴때 쓰면 좋을 듯 
		//Member[] array = gson.fromJson(jsonString, Member[].class);
		//List<Member> list = Arrays.asList(array);
		*/
				
		ContentJournal contjournal_1 = new ContentJournal();
		contjournal_1.hgroup="hgroup title";
		contjournal_1.body="comment1, comment2, comment3 안녕하세요 f";
		contjournal_1.uid=1L;
		contjournal_1.contenttype="date";
		contjournal_1.comments= "234,34, 44, 567,1";
		contjournal_1.uniquename = "20160601";
		contjournal_1.datefrom=20160601;
		contjournal_1.dateto=20160601;
		
		contjournal_1.active = true;
		contjournal_1.draft = false;
		contjournal_1.flag = false;
		contjournal_1.createdby=1L;
		contjournal_1.createdwhen=System.currentTimeMillis();		
		content_journal_repo.save(contjournal_1);
		
		ContentJournal contjournal_2 = new ContentJournal();
		contjournal_2.hgroup="hgroup xx title";
		contjournal_2.body="this is second day 입니다 ";
		contjournal_2.uid=1L;
		contjournal_2.contenttype="date";
		contjournal_2.comments= "1,2,3";
		contjournal_2.uniquename = "20160602";
		contjournal_2.datefrom=20160602;
		contjournal_2.dateto=20160602;
		
		contjournal_2.active = true;
		contjournal_2.draft = false;
		contjournal_2.flag = false;
		contjournal_2.createdby=1L;
		contjournal_2.createdwhen=System.currentTimeMillis();		
		content_journal_repo.save(contjournal_2);
		
		CommentJournal comtjournal_1 = new CommentJournal();
		comtjournal_1.body="hello comment1";
		comtjournal_1.collections="test";
		comtjournal_1.createdwhen= System.currentTimeMillis(); //long
		comment_journal_repo.save(comtjournal_1);
		
		CommentJournal comtjournal_2 = new CommentJournal();
		comtjournal_2.body="hello comment2";
		comtjournal_2.collections="ad hoc";
		comtjournal_2.createdwhen= System.currentTimeMillis(); //long
		comment_journal_repo.save(comtjournal_2);
		
		CommentJournal comtjournal_3 = new CommentJournal();
		comtjournal_3.body="hello comment3";
		comtjournal_3.collections="test";
		comtjournal_3.createdwhen= System.currentTimeMillis(); //long
		comment_journal_repo.save(comtjournal_3);
		
		/* 
		 * Date -> Long 
		 * SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		 * Date date = (Date)formatter.parse("12-December-2012");
		 * long mills = date.getTime();
		 * 
		 * 
		 * Long -> Date
		 * public String convertTime(long time){
    	 * 		Date date = new Date(time);
    	 *		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
    	 * 		return format.format(date);
		 * }
		 * 
		 * 
		 */
		
		/* security 적용을 위해서, role, user_role table에 기본 값들을 채워 넣는다 
		 * ROLE_JOURNALADMIN
		 * ROLE_JOURNALUSER
		 * ROLE_JOURNALGUEST
		 * 
		 * 아래의 SQL 구문을 쓰지 않고, 위의 것들처럼 하려면 Repository에 Role, User_Role을 만들어야 한다 
		 * user_role이라고 하지 말고, userrole로 바꾸어주어야 jpa subclass naming하고 충돌을 안할것이다 
		 * 
		 * insert into role(id, name) values (1,'ROLE_USER');
+insert into role(id, name) values (2,'ROLE_ADMIN');
+insert into role(id, name) values (3,'ROLE_GUEST');
		 * 
		 * insert into user_role(user_id, role_id) values (1,1);
+insert into user_role(user_id, role_id) values (1,2);
+insert into user_role(user_id, role_id) values (2,1);
+insert into user_role(user_id, role_id) values (3,1);


		 * 
		 * 
		 */
		
		
		//log.log(Level.INFO, "currentTime["+contjournal_1.createdwhen+"],Date["+"]");
		
	}
	

}

/*
 * 
 * 이런 테이블들이 채워져야 한다 
 * +----------------------+
| Tables_in_kew1db     | create database kew1db character set utf8 collate utf8_general_ci;
+----------------------+
| comments             |
| contents             |
| devices              |
| history              |
| key_registry         |
| tickets              |
| tokens               |
| urls                 |
| user_inventory       |
| user_nodes           |
| user_transactions    |
| users                |
| users_sample         |
| usr_activation_links |
+----------------------+
 * 
 */
