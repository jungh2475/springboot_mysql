package com.kewtea.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.kewtea.models.SysRecord;

@RepositoryRestResource
public interface SysRecordRepository extends JpaRepository<SysRecord, Long> {
	
	public List<SysRecord> findByUidAndAid(String uid, String aid);  //application id
	@RestResource(path = "findbyuidsidpage", rel = "findbyuidsidpage") //to hide (exported = false)
	public Page<SysRecord> findByUidAndAid(@Param("uid") String uid,@Param("aid") String aid, Pageable pageable); //@PageDefault
	
	public List<SysRecord> findByLevelAndTimestampBetween(String level, long from, long to);
	//findByTimestampBetweenXAndY(long from, long to) ->Count
	//findByTagAndTimestampBetween(String tag, long from, long to);
	
	
	public List<SysRecord> findFirst5ByTag(String tag);   //=Top5, used interchangeably, //List<Todo> findDistinctByTitle(String title);
	//OrderByCreatedbywhenDsc
	
	//create & update item, 제일 중요한 function 
	public SysRecord save(SysRecord sysRecord);
	
	void delete(SysRecord sysRecord);
	
	long count();
	boolean exists(Long id);
}
