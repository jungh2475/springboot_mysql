package com.kewtea.repositories;

import java.util.List;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.kewtea.models.User;




/*	
	email은 notnull이기때문에 항상 값이 있어야 한다. 안그러면 error를 만들어 낸다.
	people은 동작을 안함, 이 argument가 잘 안동작함, 안되면 원래 table_name을 사용해도 됨 >캐시가 되어서 그런지  오래 쓰면 mapping이 안됨>아직도 한번 쓴 아래의 단어는 또 쓰면 동작을 안한다 ->이 문제는 이제 사라진듯
	
	get: curl http://localhost:8080/peoplx/?page=0&size=3 //size가 안먹는다. 기본으로 20이
	add (notnull부분이 꼭 채워져 있게 하자): curl -i -X POST -H "Content-Type:application/json" -d '{  "name" : "Frodo",  "email" : "abc@abc.com", "password":"2345" }' http://localhost:8080/peoplx
	curl -i -X POST -H "Content-Type:application/json" -d '{ "name":"Frodo4", "email":"abc4@abc.com", "password":"2345", "flag":true, "last_updated":12345678, "access_rights":"{ abc: \"ddd\"}" }' http://localhost:8080/peoplx
	update: curl -X PUT -H "Content-Type:application/json" -d '{ "name": "Bilbo", "email" : "abc@abc.com", "password":"2345" }' http://localhost:8080/peoplx/1
	delete: curl -X DELETE http://localhost:8080/people/5
*/


//@PreAuthorize("hasRole('ROLE_SUPERADMIN')")
//@Transactional
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long>{ //이걸 JpaRepository로 바꾸어 주자 //PagingAndSortingRepository
	
	List<User> findByEmail(@Param("email") String email);
	@RestResource(path = "findbyemailpage", rel = "findbyemailpage")  //to hide (exported = false)
	Page<User> findByEmail(@Param("email") String email, Pageable pageable); //@PageDefault 
	List<User> findByName(@Param("name") String name);
	//Page<User2> findByName(@Param("name") String name,@PageableDefault Pageable pageable);
	List<User> findByUniquename(String uniquename);
	
}

/*
	JpaRepository(List) extends CrudRepository(Iterable) 
	
	query method - http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-introduction-to-query-methods/
	Page<User2> findAll(@PageableDefault Pageable pageable);
	@Query("select x from users x order by x.u_id, x.name where x.u_ud = :id ")
	List<User2> findAllByName();
	@Query("select u from users u where u.emailAddress = ?1")
	User2 findByEmailAddress(String emailAddress);
	
	http://blog.swiggs.co/?p=103 //이걸 쓰려면 java8이 되어야 하는가?
	http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-part-seven-pagination/
	
*/