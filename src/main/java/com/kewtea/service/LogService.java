package com.kewtea.service;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import com.kewtea.models.SysRecord;

/*
 * 
 *  @autowired로 이녀석을 가지고 와서 로그를 첨부해서 보내면 됨. 필요에 따라 알맞는 곳들로 로그를 보내게됨
 *  
 *  destinations
 		- local : screen, file  -> then filebeat
 		- cloud-elasticSearch
 		- plus trace-ID/span-ID for timespan tracking
 * 
 */


@Service
public class LogService {
	
	private static final Logger log = Logger.getLogger(LogService.class.getName());
	
	private final RestTemplate restTemplate;
	
	/*
	@Autowired
	private SysRecordRepository sysrecordRepository;
	@Autowired
	private UserRecordRepository userrecordRepository;
	@Autowired
	private EventRecordRepository eventrecordRepository;
	
	*/
	
	//environment variables에서 값을 읽어와서 대입한다 https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html
	//https://www.mkyong.com/spring3/spring-value-default-value/
	//@PropertySource("classpath:/config.properties}") @Configuration @Value("${mongodb.url:127.0.0.1}")
	//@ConfigurationProperties(locations="classpath:servers.yml", prefix = "prefix") http://blog.codeleak.pl/2014/09/using-configurationproperties-in-spring.html, https://dzone.com/articles/spring-boot-configurationproperties-1
	//https://www.slideshare.net/sbcoba/2015-47137155, https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-multi-profile-yaml
	
	@Value("${logging.mode:1}")
	private int loggingMode;
	@Value("${logging.url:kewtea.com/api/v2/logging}")  //"#{environment.SOME_KEY_PROPERTY ?: 21}", from application.properties
	private String loggingurl;
	
	
	public LogService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
	}
	
	@PostConstruct
	public void init() {
		/* 서버로 보내는 경우(elasticsearch restTemplate), 파일로 저장하는 경우 , mysql에 저장하는 경우 
		
		  1. 내부 mysql table('logs' or 'sysrecords/userrecords')
		  2. 2ndary mysql (다른 model/repository package/dir를 만들고 bean 주입, @EnableJpaRepositories Db2Config)
		  3. rest_api: elasticsearch
		  4. rest_api: flask-mysql(or elasticsearch python client)
		
		
		*/
		final String msg="LoggingService init method called";
		log.info(msg);
		System.out.println(msg);
		
		switch(loggingMode){
			case 1:  //inner mysql
				
				break;
			case 2:
				break;
			default:
				break;
		
		}
		
		
		
	}
	
	//public 
	
	public int log(SysRecord[] sysRecords){
		return 0;
	}
	
	@Async
	public void log(SysRecord sysRecord){
		
		
		switch(loggingMode){
			case 1:
				//xXXrepository.save(sysRecord)
				break;
			case 2:
				//xXXrepository.save(sysRecord)
				break;
			case 3:
				//restTemplate.postForObject(url, request, responseType)
				break;
			case 4:
				//restTemplate.postForObject(url, request, responseType)
				break;
			default:
				log.info("logfailed:"+String.valueOf(sysRecord.id)+sysRecord.toString());
				break;
		
		}
		
		//restTemplate.postForObject(loggingurl, request, responseType)
		//RestTemplate restTemplate = new RestTemplate();		
		//AsyncRestTemplate restTemplate2 = new AsyncRestTemplate();
	
	}
	
	public void log(String msg){
		//timestamp, tzone을 현재 서버의 값을로 정하고, type, category로 server.name에서 추출해서 입력 
		//log level 및 debug 경우의 class 이름은? 
		
	}
	
	public void log(String msg, long timestamp, int tzone){
		
	}
	
}
