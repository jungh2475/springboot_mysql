package com.kewtea.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


/*
*  https://www.youtube.com/watch?v=9DLX8PMXaw0
*  http://java.ihoney.pe.kr/295  //naver, daum mail보내기 
*  
*  UTF-8 지원 문제 있음 반드시 header에 message.setHeader("Content-Type", "text/html; charset=UTF-8"); 적용해야함 
*  new String(msg.getBody().toByteArray(), StandardCharsets.UTF_8)
*  
*  html email templating 을 추가하고, async로 이메일을 전송해서 전체 시스템이 lock이 안걸리게 한다 
*  추후에는 html templating도 async로 해야 하나?
*  
*  <p th:text="#{home.welcome}">Welcome to our grocery store!</p>   #{...} : Message (i18n) expressions.
*  th:text="${today}", <span th:text="${today}">13 february 2011</span>
*  
*/

@Service
public class MailService {
	
	private static final Logger log = Logger.getLogger(MailService.class.getName());
	
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	//이걸로 일괄적으로 메일을 보낼수가 있다 msgs => (변수명 : 값 )
	public int sendMailBatch(List<String> emails, String subject, String template, List<HashMap<String, String>> msgslist) throws MessagingException{
		log.info("email msgs size:"+emails.size()+msgslist.size());
		if(emails.size()!=msgslist.size()) return -1;  //크기가 같지 않으면 전송 안함. 		
		for (int i = 0; i < emails.size(); i++) {
			sendMail(emails.get(i),subject,template, msgslist.get(i)); 
		}
		
		return 1;
	}
	
	//using thymeleaf template
	public int sendMail(String to, String subject, String template, HashMap<String, String> msgs) throws MessagingException{
		String body_text = htmlMsgBuild(template, null); ///여기 고쳐야함 
		if(body_text.isEmpty()) return -1;
		sendMail(to,subject,body_text,1);
		return 1;
	}
	
	@Async  //baseline sendmail with header format setting: html or text
	public void sendMail(String to, String subject, String body, int type) throws MessagingException {
	    
        MimeMessage message = javaMailSender.createMimeMessage();
        message.setHeader("Content-Type", "text/html; charset=UTF-8");  //default
        if(type==0) message.setHeader("Content-Type", "text/plain; charset=UTF-8");
        MimeMessageHelper helper;
        helper= new MimeMessageHelper(message,true);  //true indicate multipart message
        helper.setSubject(subject);
        helper.setTo(to);
        //sskwnddp durldp  replyTo 등도 넣을 수 있다 
        helper.setText(body,true);  //true indicates html
        //in helper , you can add attachment etc...
        
        try{
        	javaMailSender.send(message);
        } catch (MailException e){
        	log.log(Level.WARNING,"mailService send failed");
        }
        
    }
	
	@Async  //baseline sendmail plain text
	public void sendMail(String to, String subject, String body) throws MessagingException {
		sendMail(to, subject, body, 0);
	}
	
	public String htmlMsgBuild(String htmlTemplateName, HashMap<String, String> msgs){
		
		String result=null;
		
		final TemplateEngine tempEngine= new TemplateEngine();
		
		//Context context= jsonString2Context(null, jsonString);
		Context context = new Context();
		for(Map.Entry<String, String> msg : msgs.entrySet()) {
			String key = msg.getKey();
		    String value = msg.getValue();
		    context.setVariable(key, value);
		}
		
		try {
			result=tempEngine.process(htmlTemplateName,context);  //with path/filename
			
		} catch (Error e) {
			result=null;
		}
		return result;
	}
	
	/*
	private Context jsonString2Context(Object objClass, String jsonString){
		Context context=new Context();
		context.setVariable("modelNameX", "object.nested");
		
		return null;
	}
	
	*/
}

/*
 
  메일에 첨부 파일이 있는 경우 new MimeMessageHelper(MimeMessage mimeMessage, boolean multipart, String encoding) 중 
  multipart 값을 true 로 변경 하고, 
  MimeMessageHelper.addAttachment(attachmentFilename, inputStreamSource) 메소드를 사용 하자. 
  단 파일 명을 MimeUtility.encodeText(String text) 하지 않으면, 한글이 께져서 발송 된다.
  
*/