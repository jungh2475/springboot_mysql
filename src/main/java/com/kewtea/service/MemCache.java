package com.kewtea.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/*
 *  read 일때는 그냥 동작하고, write(put)데면 synchronized 되도록 하면 좋을듯, synchronized (): cacheMap, hitMap
 * 	new ConcurrentHashMap을 사용할것 
 */

public class MemCache<K,T> {  // key는 long, int, string, T는 jsonString, any object
	
	private long time2Live; // default 1분 
	private int cacheSize;   //default =20
	private Map<K, T> cacheMap;   //컨텐츠는 여기다가 담아둠 
	private Map<K,CacheHit> hitMap;  //통계 
	private int numAccess, numHit, numExternal;  //TODO: 통계   여기가 synchronized()되어야 할듯함  
	private static final int numMax = 1000000;
	private MemCacheExternal<K, T> mcExternal;  //@Autowired repository or Rest Api supporting interface
	
	
	public void write(K key, T value){
		//cache안에 이미 형식이 있으면 update하고, 없으면 새로 만들어서 두고,그리고 서버에도 저장한다 
		CacheHit ch;
		//T t=(T) cacheMap.get(key);
		T t=cacheMap.get(key);		
		if(t==null){ 
			ch= hitMap.put(key, new CacheHit());
			
		}else{	ch= hitMap.get(key);}
		ch.updated=System.currentTimeMillis();
		cacheMap.put(key, value);		
		mcExternal.write(key, value);			
	}
	
	public T getFromExternal(K key){
		T t = mcExternal.read(key);// null; //TODO: must implement 이안에 객체를 어떻게 넣는가? restApi or @Autowired repository  	
		//return (T) this.theClass;
		return t;
	}
	
	
	public T get(K key){ //캐시에 내용이 없으면 repository에서 가져와서, 새로 put하고 늙은것들을 제거한다
		//getFromExternal  => put => findOlds => cacheMap.remove(key);
		numAccess++;
		T t=cacheMap.get(key);
		if(t==null){
			t=getFromExternal(key);
			numExternal++;
			if(t==null) {return null;}
			int g=cacheMap.size()-cacheSize;
			if(g>=0){ cleanup(g+1);}
			cacheMap.put(key, t);
			hitMap.put(key, new CacheHit());
		}else{  numHit++;
				hitMap.get(key).lastAccess=System.currentTimeMillis();hitMap.get(key).hitCount++;
				} 	

		return t;
	}

	public int size(){
		return cacheMap.size();
	}
	
	@SuppressWarnings("unchecked")
	public void cleanup(int num){  //n개를 가장 덜 중요한것 부터 제거한다, -1일때 auto, 아마도 평소에는 한개씩만 지우면 되니 1이면 될듯함   
		List list = new LinkedList(cacheMap.entrySet());
		Collections.sort(list, new Comparator(){
			@Override
			public int compare(Object o1, Object o2) {
				//compare logic :일단 가장 오래된것(lastAccess)순으로 제거 그리고 * (if option==1) inclusive hitCount
				K key1 = (K) ((Map.Entry) (o1)).getKey();
				K key2 = (K) ((Map.Entry) (o2)).getKey();
				CacheHit ch1= hitMap.get(key1);
				CacheHit ch2= hitMap.get(key2);
				if(ch1.lastAccess>ch2.lastAccess){return 1;}
				if(ch1.lastAccess<ch2.lastAccess){return -1;}
				return 0;
			}
		});
		
		for(int i=0; i<num ;i++){
			K key=(K) list.get(i);
			cacheMap.remove(key); //remove(K key)할때는 두개다 지운다 : 	cacheMap.remove(key);hitMap.remove(key);
			hitMap.remove(key);
			
		}
		// log.info - 이제 크기는 num숫자만큼 줄어져 있음 
		
	}
	
	public void updateCache(long aging){
		//정해진 시간 이상된 데이터는 다시 갱신한다 - updated시간을 보고 확인하자
		//for-loop, long1-long2>aging : get new data from external Source, cacheMap, hitMap
		T value;
		for(Entry<K, MemCache<K, T>.CacheHit> entry: hitMap.entrySet()){
			if(System.currentTimeMillis()-entry.getValue().updated>aging){
				value=mcExternal.read(entry.getKey());
				cacheMap.put(entry.getKey(), value);
				entry.getValue().updated=System.currentTimeMillis();
			}
		}
		
	}
	
	
	
	
	//constructor  - cache크기 및 정책, 구조체?, 그리고 External Access 정의함. 
	public MemCache(MemCacheExternal<K, T> mcExternal){
		this(60*1000L,20, mcExternal); //1분 1000L *60
	}
	
	@SuppressWarnings("unchecked")
	public MemCache(long time, int size, MemCacheExternal<K, T> mcExt){
		mcExternal = mcExt;
		this.time2Live =time;
		cacheSize =size;
		numAccess=0; numHit=0; numExternal=0;
		cacheMap = (Map<K, T>) new ConcurrentHashMap<String,String>();
		//cacheMap = (HashMap<K, T>) new HashMap<String, String>();
		hitMap = (Map<K, MemCache<K, T>.CacheHit>) new ConcurrentHashMap<String, CacheHit>();
				
	}
	
	class CacheHit {
		public long created;
		public long updated;
		public long lastAccess;
		public int hitCount;
		
		public CacheHit() {
			//createdWhen=a;
			created=System.currentTimeMillis();
			updated=0L;
			lastAccess=0L;
			hitCount=0;
		}
	}
	
}

/*
 * 예전에는 interface로 외부 저장소를 클래스 내부로 가져와 데이터 입출력을 했는데, 그것보다도, 이제는 사용하는 class에서 
 * method로 잡아서 번거롭지만 구체적으로 구현해서 put으로 캐시에 직접 넣어 주는 것이 좋겠다  
 
  public interface MemCacheExternal<K,T> {  //key=long, int, String, valueObject
	
	public T read(K key);
	public void write(K key, T value);

}
  
  
 
 */



