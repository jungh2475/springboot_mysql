package com.kewtea.service;

public interface MemCacheExternal<K,T> {  //key=long, int, String, valueObject
	
	public T read(K key);
	public void write(K key, T value);

}
