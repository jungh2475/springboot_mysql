package com.kewtea.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

/*
	backup Server - mysql backup file & transfer to ftp,gStorage, 
	backup Read Serevr - mysqlmulti
	rest-cloud registry inquery
	ssh-remote server는 외부의 python 프로그램을 통해서 값을 얻어오고 실핼할수 있게 하자 
	local pc python call
	local pc java call
*/
@Service
public class RemoteProcessService {
	
	/*
	 * 
	 	최근 n개 버젼, 혹은 기간(from, to에 의한 backup객체들을 가져온다 , 
	 	이를 위해서 mysql backup file들 목록을 가져온다 (registry에 저장된 backup 기록들을 참조함 )
	 	remote mysql_multi server에 해당 파일을 필요시에 loading하고 하나씩 가져 온다 
	 	 
	 */
	
	public int isMysqlMultiAvailable(){
		//열린 포트번호들을 다 return하고 가지고 있는 backup 버젼 파일명도 알게 하자 ....
		return 0;
	}
	
	@Async
	private Future<String> restartExtMySqls(String backupfile, int port){
		//ssh 연결? 아니면 같은 machine내에서 mysql_multi를 동작시킬까? -외부에 있어서 rest로 데이터를 가져오는 것이 좀 더 cpu load부담이 없음 
		//그런경우네는 외부에서 rest+mysql을 제공하여야 함. mysql_http_plugin
		/*
		 * https://www.infoq.com/news/2014/09/MySQL-REST
		 protocol://host:port/crud/database/table/primaryKey
		 curl -XGET 'http://remoetURL:3306/crud/kew2db/content/35'
		 
		 * 
		 */
		
		//mysql -h localhost -u root -p1234 databaseName < backup.sql
		//
		//sendLinCommand("restart mysql");
		
		return new AsyncResult<String>("hello world !!!!");
	}
	
	public String buildPythonCommand(String fileName, String arguments) throws IOException{
		
		/*
		 파일을 하나 작성해 보자 
		String prg = "import sys\nprint int(sys.argv[1])+int(sys.argv[2])\n";
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
		out.write(prg);
		out.close();
		*/
		String result = "sudo python3 "+fileName+" "+arguments;  //or not sudo, not python3 
		return result;
	}
	
	public String runCommand(String command){
		String result="";
		try {
			Process p = Runtime.getRuntime().exec(command);
			/*
			 	ProcessBuilder pb = new ProcessBuilder("python","test1.py",""+number1,""+number2);
				Process p = pb.start();
				BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			 */
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			//int ret = new Integer(in.readLine()).intValue();
			result=result+new String(in.readLine()).toString();  //여기를 forloop를 돌려서 다 받아 와야 함 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//"python test1.py "+number1+" "+number2
		
		
		
		return result;
	}
	
	/*
	 *  remote shell
	 *  http://java.ihoney.pe.kr/379 있음 
	 *   compile("org.springframework.boot:spring-boot-starter-remote-shell")
		 반드시 spring-security와 같이 사용하여야 함 ,리모트쉘의 기본포트는 2000번
		ssh로 접속이 가능한데, 반드시 이것이 필요한가? 할수 있는 작업은 ...beans, endpoints,shell,

	 */
	
	
}
