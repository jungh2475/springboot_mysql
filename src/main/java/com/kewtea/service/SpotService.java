package com.kewtea.service;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *	2016-08-15 버젼   
   
   0. Setting and overview
   		
   		- loadCsv->load_gSheet(url) : ga_tracking_id, 
   
   1. Measurements
   		:type: gSheet+appsScript via form, api, device(gateway,iot_sub) -> spring-elastic-log conversion 
   		- addDevice(android, client_id, shopId), addGSheet(gSheetUrl,range,adapter)
   		- attachGaAdapter, getGaAdapter=MappingValue(to use measurement_api)
   		- getRecentLogs(deviceId)
   		- /$SHOPNAME/admin/recorder(수동자료입력)
   		- 자동 gSheet import(url, range-headers) using AppsScript
   		- searchMeasurements(검색 질의)
   		
   		- addSurvey(gForm)->including addGSheet
   		
   2. Analytics
   		//
   		- getGaPathList(SHOPID) : e.g. /visit/reception, crudGaPaths
   		- visitorReport
   		- salesReport (e-commerce)
   		- trackingReport(optional-competitor/mySNS) : crudProducts(getProductList-SKU)
   		+ gSheet(url,chartName) using gChart
   
   3. Contents
   		//
   		:type: video/audio-bgMusic/html(url)
   		- logicalConditioning(loop, exit, transEffect-slide/fade)
   		- import(getRecommendations)
   		
   4. Casting
   		:type: chrome_cast().getId, raspberry_pi().getId, android.putId
   		- getStationList, crud
   		- attachDevice(deviceId), detach, getAttachedList
   		-/#SHOPNAME/#STATION/json, /#DISPLAYDEVICE/json
   		.raspberryPi-autoStart
   
   5. 모든 로그 수집
   /device/collect (json: device_id, ....)
   
 
 *  2016-07-01 버젼 
 	[methods]
 	T.getServiceStatus(shopid)
 		alert
 		get
 	
 	0.setting
 		create/editBrand (only ceo만)
 		loadCsv
 		addUser(Team) - type: group_userid
 		getShopIds(set...)
 	1.measurements
 		addInMyShop(=in my analytics)  //그리고 기본으로 나의 장치들은 다 등록된다(e.g. display, campaign) 
 		create/update/delete/"search" measurement entities & getDetails (searchByLocation)
 		getDataLog(measurement_device_id, timePeriod)
 		:registerMeasurements() //kewtea에 등록된 다른 디바이스들도 검색해서 등록함,사용여부 권한은 deviceProfile안에 있음(public이면 누구나다 사용가능, shopId,userId등으로 제한둠  
 	2.analytics
 		addView (dimension, metric) -to shop (아래 api로 측정 가능한 소스들을 알아내고 분석 리포트를 등록함)
 		[AnalyticViewResolver] //현재 상황에서 가능한 view 추천 목록을 만들어냄 
 		getPlot(viewName,period) <= getData(viewName,period) 
 	2.X IFTT event-vector-creation //나중에 구현할 부분 : 각종 측정 값과 조건 방정식에 의해서 vector 행렬 만들기 
 		IFTT(event<=measurements, action<=dataWrite or functionCall)
 		
 	3.getCampaign/create/update/delete - Rest Data 사용할것?
 		create/update/read/delete Campaign
 		findDisplaysUsingThisCampaign
 		(items - youTube_url, video_url, slackNote, facebook_note....)
 	4.get/set display
 		getDisplays(shopid) or(ids[], timePeriod)  //포함내역 : 서비스상태, 현재 play campaign(id,time-mark), get url-format
 		addDisplay/update/remove //
 		setDisplayChannel // 어떤 campaign을 언제 수신할 것인가? (timePeriod, campaign_id)
 		
 ------------------
 	5.추후에 넣을 기능들 ....
 		add SlackChannel, FacebookPage
 		slack : putCommentSpot: readout(command msg in chatroom channel), putCommentSpot: typein(string to chatroom channel)
 				
 				register commands with IFTT ("commandName", arguements[[]
 				register default command pack
 				unregister ...all,  ....some
 	
 
 */


@Service
@Transactional
public class SpotService {
	
	private static final Logger log = Logger.getLogger(SpotService.class.getName());
	private static final String serviceId ="spot";
	
	//@Autowired
	
	public long setMeasurementRegistry(String shopid, String name){
		return 0L;
	}
	
	//get
	
	public void setAnalyticView(String from, String to){
		
	}
	
	public long setAnalytics(String shopid, String name, String source_url){
		return 0L;
	}
	
	public long setAnalytics(String shopid,String name,String gSheet_url, String gChart_url){
		return 0L;
	}
	
	//get
	
	public long setContentProgram(String shopid, String json){
		return 0L;
	}
	
	//getList or one
	
	public long setCastingStation(String shopid, String json){
		return 0L;
	}
	
	//get
}
