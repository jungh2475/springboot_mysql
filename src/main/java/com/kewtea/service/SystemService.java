package com.kewtea.service;

import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.kewtea.models.SysRecord;
import com.kewtea.repositories.SysRecordRepository;

/*
 * newly added
 	1. backup(snaphot) & restore model(table items)
 	   추가되는 table은 snapshot (registry) : id, tableObjectName, originId, timeStamp(복사시간),status(active/deactive) 
 	2. retrive model from external (mysql_multi:machine ip and ports)
 	3. mergeUser, 
 	
 	
 	여기서 구현해 놓고 다른 곳으로 옮길것은? modelVersion, snapshot(true or false) changeIndicator(for push service)
 * old version (동시접속 확인)
 	1.getConcurrentAccessStatus(urls[], mode=r/w) 
  	2.(record) 필터를 써서 자동으로 기록할까? 아님은 controller에서 이 service를 써서?
  		.save() , thread.start(clear)
  	3. compareTimestamp(urls[])
  	4. getCookieInit, deleteCookieRegistry, clearCookieExpired,
  	
  	5. 
  
 */

@Service
public class SystemService {
	
	private static final Logger log = Logger.getLogger(SystemService.class.getName());
	
	@Autowired
	private SysRecordRepository syslogRepository; //이것 말고 다른 저장소를 써야하나?
	@Autowired
	private RemoteProcessService remoteProcess;
	
	//여기에 모든 Repository를 등록 해야 한다 
	
	public long[] findSnapshots(String model, long id){
		long[] list = new long[10];
		return list;
	}
	
	public long saveToSnapshot(String str){
		return 0L;
	}
	
	public int saveFromSnapshot(long target, long source){
		return 0;
	}
	
	public int importToSnapshot(long snapshot_id){
		//외부의 mysql_multi에서 원하는 조건의 자료를 가져와서 snapshot에 저장한다  
		return 0;
	}
	
	private void sendLinCommand(String command){
		String result=remoteProcess.runCommand(command);
		//.... 이런식임 
	}
	
	
	
	
	public long reportLog(SysRecord record){
		
		return 1L;
	}
	
	/*
	 * 다음의 두개의 method는 동시접속이 진행중인 사용자를 알아내고, 필요시에 사용자젒ㄱ 클라이언트의 device ip를 등록해서, 앞의 method에서 분석해서 쓸수 있도록 하게 해준다 
	*/
	public boolean getConcurrentAccessStatus(String aid,String userid){
		
		ArrayList<SysRecord> syslogs = (ArrayList<SysRecord>) syslogRepository.findByUidAndAid(userid, aid);
		//알고리즘 부분 - 최근 5회의 기록에서 두군데 이상에서 동시접속한것이 있는지 찾아 내기 
		//ip + cookie value로 구분? 현재 버젼은 단순히 ip주소만으로 비교하기때문에 놓치거나, 불완전한 경우들이 있을수가 있음. 나중에 향상시켜야함 
		//TODO: imporve detection algorithm for concurrentAccess(write)
		
		return false;
	}
	
	public boolean pushSysLogClientIp(String sid, String uid, String type, String msg){
		//request.
		
		Long timeStamp = System.currentTimeMillis();
		//type="client-ip"
		//type="cookie-kew"
		//type="client-id" or "device-id"
		
		/*
		 Controller에서 넣어야 할 코드 
		 1)method(HttpServletRequest request) or 2) In Spring MVC, you can @Autowired the HttpServletRequest directly.
		 or 3)HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
         4)method(@RequestHeader(value="User-Agent",defaultValue="foo") String userAgent)
         --------------
         String ip = request.getHeader("X-FORWARDED-FOR");
         if (ip == null)
            ip = request.getRemoteAddr();
         
         request.getHeader("user-agent"); //"User-Agent" : "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
		 request.getHeader("from"); //"From" : "googlebot(at)googlebot.com"
		 //@RequestHeader(value="Accept-Language")
		 //@CookieValue(value = "foo", defaultValue = "0") Long hitcounter  =>method(HttpServletResponse response), response.addCookie(new Cookie("foo", hitCounter.toString()));
		 //device detection : see https://spring.io/guides/gs/device-detection/ using device.isMobile()/Normal/Tablet
		 */
		return true;
	}
	
	public String getUniqueCookie(){
		
		//값을 만든 후에 저장소에서 중복이 없음을 확인하고 보낸다  userName을 넣어서 hashing으로 만든다?  
		/*
		 	@CookieValue("foo") String fooCookie => equals ...below
		 	Cookie[] cookies = request.getCookies();
		 	if (cookies != null)
		 	if (cookies[i].getName().equals(cookieName)) {....value = cookies[i].getValue();
		 	
		 	
		 	...HttpServletResponse response) {
		 	Cookie cookie = new Cookie(cookieName, value);
    		cookie.setMaxAge(maxAge);
    		response.addCookie(cookie);
		  
		 */
		return null;
	}
	
	public int[] compareTimestamps(int[] urls){
		int[] results = new int[urls.length];
		int i=1;
		//parase url[i]=> [0]=repositoryName,[1]=tableName, [2]=id
		//long valueUnderTest = repository.findById(id).lastUpdatedWhen
		//만약에 return 값이 없다면 ? 
		//if (>) result[i]=0, -1, +1;
		//int is not null, it may be 0 if not initialized.
		return results;
	}

}
