package com.kewtea.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.concurrent.Future;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class AsyncService {
/*
 * 
 	@Async method가 inner로는 동작을 안해서, 여기에서 선언을 해서 상위의 서비스가 가져가서 쓰도록 한다 
 */
	
	private final RestTemplate restTemplate;
	
	@Async
    public void log(String msg){
		
		//final RestTemplate restTemplate = new RestTemplate();
		String url="";
		String request="";
		
		try {
			
			//String result= (String) restTemplate.postForObject(url, request, String.class);
			URI location = restTemplate.postForLocation(url, request, String.class);
			
		} catch(HttpClientErrorException e){
			
		} catch(Exception e) {
			
		}
		
	}
	
	@Async
	public Future<String> runCommand(String command) throws InterruptedException {
		String result="";
		try {
			Process p = Runtime.getRuntime().exec(command);
			/*
			 	ProcessBuilder pb = new ProcessBuilder("python","test1.py",""+number1,""+number2);
				Process p = pb.start();
				BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			 */
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			//int ret = new Integer(in.readLine()).intValue();
			result=result+new String(in.readLine()).toString();  //여기를 forloop를 돌려서 다 받아 와야 함 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//"python test1.py "+number1+" "+number2
		
		
		
		//return result;
		return new AsyncResult<>(result);
	}
	
	
	public AsyncService(RestTemplateBuilder restBuilder){
		this.restTemplate=restBuilder.build();
	}
	
}


/*
@Async
public Future<User> findUser(String user) throws InterruptedException {
	
	User results = restTemplate.getForObject(url, User.class);
	return new AsyncResult<>(results);
}
*/