package com.kewtea.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@PropertySource(value="classpath:extra.properties")  //없으면 default is application.properties
@Service
public class ConfigService {
/*	
	1. build_type : dev-local, dev-gcloud, production-stag1, production-stag2...
	2. logging
		
		Field Injection takes place after construction or public LoggingService(@Value("#{'${loggingservice.urls}'.split(',')}") List<String> urls){
	3. 
	4. 
	
*/	
	
	@Value("#{'${loggingservice.urls}'.split(',')}")
	public List<String> loggingUrls;
	
	@Value("#{'${loggingservice.url}'}")
	public String loggingUrl;

}
