package com.kewtea.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.kewtea.models.CommentJournal;
import com.kewtea.models.ContentJournal;
import com.kewtea.repositories.CommentJournalRepository;
import com.kewtea.repositories.ContentJournalRepository;


/*  
 *  이미 repository에 method가 있는 것은 그것을 활용하실것 바로 direct return, 없는 것으로 추가하는 것은  
 *  getList  
 *  
 *  1.1 crud_user
 *  2.1 crud_content(datePage, summaryPage, urlPage)
 *  2.2 crud_comment
 *  1.2 crud_inventory(collection, keyword)
 *  
 */



@Service
@Transactional
public class JournalService {
	
	private static final Logger log = Logger.getLogger(JournalService.class.getName());
	private static final String serviceId ="journal";
	
	
	@Autowired
	private ContentJournalRepository contentRepository;
	@Autowired
	private CommentJournalRepository commentRepository;
	
	/*
	 * - memCache(id,jsonString[])
	 * - List<ContentJournal> getDatePages : 완료 
	 * - longId create/update/deleteDatePage
	 * - List<String> collectCollections, collectKeywords(id[])
	 *   (autocomplete str.startWith("a")
	 * - String[] updateUserCollections, updateUserKeywords(uid, from, to)
	 * - Map<String,List<CommentJournal>> sortByCollections(List<CommentJournal>)
	 * - ContentJournal create(update)SummaryPage(uid, from, to, option)
	 * - longId deleteSummaryPage
	 * -
	 * 
	 * 
	 */
	
	
	public List<ContentJournal> getDatePages(long userid, int from, int to, int option){
		//option==-1이면, summaryPage를 제한 나머지것들을 출력해 준다. 
		//cList = contentRepository.findByContenttypeAndCreatedby(serviceId, user.getId());		
		List<ContentJournal> cList;
		cList = contentRepository.findByUidAndDatefromBetween(userid, from, to);
		if(option==-1 && cList.size()>0){
			log.info("contentDatePage["+cList.size()+"]");
			for(ContentJournal cj: cList){ //remove existing summaryPage
				if(cj.contenttype.contains("summary")){
					cList.remove(cj);
					log.log(Level.FINEST,"removing summarypage from list");
					}
			}
		}
		return cList;
	}
	
	public Map<String, List<CommentJournal>> sortByCollections(List<CommentJournal> comments){
		Map<String,List<CommentJournal>> commentsbycollections = new HashMap<>();
		
		
		
		
		return commentsbycollections;
	}
	
	protected long[] checkNeighbours(long id){
		long[] longlist = {1L, 2L}; 
		return longlist; 
	}
	
	public long insertDatePage(ContentJournal input){  //update
		
		//새로이 추가하면 주변에 영향을 미치는 summaryPage(update 실행 시킴)
		
		return 1L;
	}
	
	public long[] deleteDatePage(long id){  //하나를 지워도, 영향 받는 것들이 있어서, 이를 id로 표시해 준다 
		
		ContentJournal cj = contentRepository.findById(id);
		if(cj==null){ return null;}
		//주변 관련된 위치의 것들을 삭제하고 정리 한다= summaryPage면 좌우의 것들찾아 숫자 조정, 
		//datePage면, 보통은 그냥 삭제하고, 이것이 표함된 summaryPage및 종속된 comment들은 일단은 수정을 안하기로 한다 
		//
		if(cj.contenttype.contentEquals("summaryPage")){
			
			long next=0, prev=0; //Primitive can't be null, only reference to object can hold null value
			if(cj.next>0) {next=cj.next;}
			if(cj.prev>0) {prev=cj.prev;}
			if(next <1L || prev <1L){ return null;}
			ContentJournal cj_next=contentRepository.findById(next);
			ContentJournal cj_prev=contentRepository.findById(prev);
			
		}
		contentRepository.delete(id);//진짜로 삭제함 
		
		long[] longlist = {1L, 2L}; 
		return longlist;
	}
	
	public long[] createandinsertSummaryPage(ContentJournal summaryPage){  //여기에 id대신에 객체를 넣자 
		//makeSummaryPage -> get contentJournal object  not id
		//add to repository and modify pre/next position values
		long[] longlist = {1L, 2L}; 
		return longlist;
	}
	
	protected long[] getCommentsIds(List<ContentJournal> contentPages){
		List<Long> idList = new ArrayList<>();
		for(ContentJournal content: contentPages){
			List<String> commentsStr=null;
			if(content.comments!=null && !content.comments.isEmpty()){
				log.log(Level.FINEST,"coments{"+content.comments+"}");
				commentsStr=Arrays.asList(content.comments.replaceAll("\\s","").split(","));
			}
			for(String commentStr: commentsStr){
				log.info(commentStr);
				log.info(":"+Long.parseLong(commentStr));
				idList.add(Long.parseLong(commentStr));
			}
		}		
		if(idList.size()>0){
			long[] ids = new long[idList.size()];
			int count=0;
			for(Long id: idList){
				ids[count++]=id.longValue();
			}
			return ids;
		} 
		return null;
	}
	
	protected Map<String,String> sortByCollectionName(List<CommentJournal> comments){
		//collectionName, commentId(date,title,url....)
		//이거 그냥 comments repository에서 처리 가능함? - 그것으로 대치하자
		//repository.findByCollectionnameUidAndDateBetween()
		//TODO:
		Map<String,String> collections = new HashMap<>();
		collections.put("collectionName", "12,13,15");
		return collections;
		//return null;
	}
	
	public ContentJournal makeSummaryPageNew(long userid, int from, int to){
		ContentJournal summaryPage= new ContentJournal("summaryPage");

		//1.default properties
		/*
		summaryPage.contenttype
		summaryPage.active
		summaryPage.createdwhen
		summaryPage.datefrom
		summaryPage.dateto
		summaryPage.prev
		summaryPage.next
		summaryPage.body
		
		*/
		summaryPage.body="";  //TODO:여기에 htmlcode를 넣어야 한다 
		
		//2.getContentPages
		Map<String,String> collections = new HashMap<>();
		List<ContentJournal> contentPages= getDatePages(userid,from,to,-1); //onlyDatePages
		if(contentPages.size()==0){ return null;}
		//3.getComments from 2. & sortBy CollectionName
		long[] commentIds = getCommentsIds(contentPages);
		if(commentIds != null){
			List<CommentJournal> comments= getComments(commentIds);
			collections=sortByCollectionName(comments);
		} else{ return null;}
		
		//3.complete the body of contentSummaryPage
		summaryPage.body+="collectionNames Table";
		
		//4.set position between datePages
		summaryPage.prev=1L;
		summaryPage.next=2L;
		
		
		return summaryPage;
	}
	
	public ContentJournal makeSummaryPage(long userid, int from, int to){
		/*
		 *  1. getPages and remove summaryPage
		 *  2. sort and make summaryPage for that period
		 *  
		 */
		
		List<CommentJournal> comments=null;
		
		//1.getPages and remove summaryPages if exists
		List<ContentJournal> cList= getDatePages(userid,from,to, 0);
		if(cList.size()>0){
			log.info("contentDatePage["+cList.size()+"]");
			for(ContentJournal cj: cList){ //remove existing summaryPage
				if(cj.contenttype.contains("summary")){
					cList.remove(cj);
					log.log(Level.FINEST,"removing summarypage from list");
					}
			}
		} else{	return null; }
		
		//2.collect commentIds from datePages
		List<Long> idList = new ArrayList<>();
		for(ContentJournal content: cList){
			List<String> commentsStr=null;
			if(content.comments!=null && !content.comments.isEmpty()){
				log.log(Level.FINEST,"coments{"+content.comments+"}");
				commentsStr=Arrays.asList(content.comments.replaceAll("\\s","").split(","));
			}
			for(String commentStr: commentsStr){
				log.info(commentStr);
				log.info(":"+Long.parseLong(commentStr));
				idList.add(Long.parseLong(commentStr));
			}
		}
		
		//3. getComments
		if(idList.size()>0){
			long[] ids = new long[idList.size()];
			int count=0;
			for(Long id: idList){
				ids[count++]=id.longValue();
			}
			comments=getComments(ids);
		}
		
		//4. sort comments by collection names
		
		ContentJournal content=null;
		return content;
	}
	
	//getUrlPages
	public List<ContentJournal> getUrlPages(long userid, String[] url){
		return null;
	}
	
	//getComments (id array로 여러개를 질의해서 받을수 있음 )
	public List<CommentJournal> getComments(long[] ids){
		
		List<CommentJournal> cList = new ArrayList<>();
		CommentJournal cj;
		
		for(long id : ids){
			log.info("get["+id+"]");
			cj=commentRepository.findOne(id);
			if(cj!=null){cList.add(cj);}
		}				
		return cList; 
	}
	
	//1.2 crud_Inventory(collection, keywords)
	
	/*  http://docs.spring.io/spring-boot/docs/current/reference/html/howto-logging.html
	 * 	http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-logging.html
	 *  JDK (Java Util Logging)
	 *  
	 * 	private static final Logger log = Logger.getLogger(JournalService.class.getName());
	 * 	log.info("msg");
	 * 	log.log(Level.FINEST, "log debug turn off testing.....");  //OFF
	 *  SEVERE (highest value),WARNING,INFO,CONFIG,FINE,FINER,FINEST(lowest value) plus OFF
	 *  
		application.properties =>	logging.level.root=WARN
									logging.level.org.springframework.web=DEBUG
									logging.level.org.hibernate=ERROR
									logging.file=myapplication.log
									logging.path
	*			Log Level — ERROR, WARN, INFO, DEBUG or TRACE
	*
	*/
	
	
	
}
