package com.kewtea.services;



import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.kewtea.models.SysRecord;


@Service
public class LogService {
/*
 * 
 	@autowired loggingService -> lgs.log(level,type,tag,msg);
 	springboot는 slf4j(simple logging facade for java, logback)을 사용함 
 	old)apache.Log logger = LogFactory.getLog(getClass());log.debug();....
 	springbot)slf4j.Logger logger=LoggerFactory.getLogger(getClass()); logger.info(msg,source)
 	JournalController.class.getName()
 	
 	level: ERROR, WARN, INFO, DEBUG or TRACE
 	destination: console, file, rest_apis[n]
 	logging.file(sp.kew.log) or logging.path(/var/log)
 	logging.level.root=WARN
	logging.level.org.springframework.web=DEBUG
	logging.level.org.hibernate=ERROR
 	//logging.maxsize //Log files will rotate when they reach 10 MB 
 	logging.samplingrate = 0.01
 	logging.url
 	
 	http://dveamer.github.io/java/SpringAsync.html
 	http://www.baeldung.com/rest-template
 	http://cakas.tistory.com/9
 	
 */
	//private static final Logger log = Logger.getLogger(LoggingService.class.getName());
	private static final Logger logger=LoggerFactory.getLogger(LogService.class);
	
	@Autowired
	private ConfigService configService;
	@Autowired
	private AsyncService asyncService;
	
	private final RestTemplate restTemplate;
	
	public void config(String paramter){
		
	}
	
	//반드시 public에서만 사용해야함... 	@EnableAsync, 같은 객체내의 method끼리 호출시 async method는 동작하지 않습니다.
	public void error(String msg){
		//asyncService
	};
	public void warn(String msg){};
	public void info(String msg){};
	public void debug(String msg){};
	
	public void log(String level, String type, String tags, String msg){
		
		
	}
	public void log(Level warning, String type){}
	
	@Async  
	public void post(String msg){
		String url="";
		String request="";
		
		
		try {
			
			String result= (String) restTemplate.postForObject(url, request, String.class);
			
			
		} catch(HttpClientErrorException e){
			
		} catch(Exception e) {
			
		}
		
		//ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
		//RestTemplate restTemplate = new RestTemplate(requestFactory);
		//HttpEntity<SysRecord> request = new HttpEntity<>(new SysRecord("bar"));
		//String response = restTemplate.postForObject(url, request, String.class);
	}
	
	//Spring Boot automatically provides a RestTemplateBuilder
	public LogService(RestTemplateBuilder restTemplateBuilder){
		this.restTemplate=restTemplateBuilder.build();
	}
	
}
