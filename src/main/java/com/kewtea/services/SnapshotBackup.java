package com.kewtea.services;

import com.kewtea.models.Content;

public interface SnapshotBackup {
	
	public long backup(Content content);
	public int restore(long target_id); 
	//여러개의 버젼이 있는 경우에 순서를 정해 주어야 한다. 그리오 외부에서 자료를 가져 오는 경우는? 
	//pull(long target_id), push(
	public int restore(long target_id, Content contentSnapshot);
	// -1 fail, 0=noSuchBackup +1 success
	public int clear(long target_id); //이것과 관련된 모든 snapshot 제거 

	public int count(long target_id, int inclusive); //how many snapshots has?
	
}
