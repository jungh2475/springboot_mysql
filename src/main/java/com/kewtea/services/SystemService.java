package com.kewtea.services;

import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemService {
/*
 * 
 *  
    시간이 오래 걸리는 일들이 많으므로 반드시 ticket_id를 발급하고(이건 그냥 sync 메소드 구현), @aysnc로 작업을 수행한다(future로 return은 디버깅용이고, 실제로는 필요 없음, 결과물은 mysql_db에 저장되므로...) 
 	1)backup mysql file -> upload to gStorage bucket?
 	2)service status report
 		2-1) checkLogs/DbSize(count)
 		2-2) api call log
 		2-3)concurrentAccess
 	3)snapshot size 이내로 유지하면서 청소 remove_olds
 	
 	runCommand
 	
 */
	@Autowired
	private AsyncService asyncService;
	
	public Future<String> backup() throws InterruptedException{
		String command ="";
		Future<String> result = asyncService.runCommand(command);
		
		return null;
	}
	
	
	public void applyTransactions(){
	/*
	 * 밖에서 webhook으로 연락온 결제 내역을 내부 db에 적용하고, 사용자 상태값을 변경한다 	
	 */
	
		
		
	}
	
	
}
