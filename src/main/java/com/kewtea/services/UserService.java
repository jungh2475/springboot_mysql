package com.kewtea.services;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kewtea.models.ActivationLink;
import com.kewtea.models.Content;
import com.kewtea.models.User;
import com.kewtea.repositories.UserRepository;




@Service
public class UserService implements SnapshotBackup, UserDetailsService {
/*
 * 
 	
 	mergeUser
 */
	@Autowired
	UserRepository userRepository;
	@Autowired
	MailService mail;
	@Autowired
	LogService log;
	
	//@Autowired userRepository; mail
	
	//buildUser
	
	//create
	//delete
	//update -> snapshot
		//->deactivate, activate, flag, 
	//findBy
	
	//TODO: 이메일을 변경하는 경우에는 이메일 체크 확인 이메일을 다시 보내야 한다
	//TODO : 사용자 email id를 다 tolower()로 해서 일괄 적용 할것 , 대소문자 구분을 하지 말자 
	//TODO : @gmail.com @google.com @google.co.kr등을 같은것으로 처리 할것 
	//findall (with/without page)
	
	//2.add (create)
	public long createUser(User user, boolean bypass){  //사용자 등록하고  id를 return함 
			//find like ("a") or lower, 이미 같은 사용자가 있는 지 확인점검  
			if(userRepository.findByEmail(user.password.toLowerCase())!=null) { 
				return 0;  //user already exists, so no creation
				}				
			try {
				user.active=false;
				user=userRepository.save(user);
				//TODO: 여기에 role값도 저장해 두어야 한다. 아니면 email verification이 되면 그때 할당해 넣을 수도 있다 
				if(bypass == false){
					//sending account confirm email with activationlink
					String title = "Confirm email link to activate journal.kewtea.com";			
					String content= registerActLink(user.id);
					mail.sendMail(user.email, title, content);
				}			
			} catch(Exception e) {
				log.log(Level.WARNING, "failed to create user in db");
				return -1;
			}		
			return user.id;
	}
	
		
		
	public int deleteUser(long uid){
		userRepository.delete(uid);
		if(activationMap.containsKey(uid)){activationMap.remove(uid);}//이전에 다른 activationlink가 등록되서 미사용중인것이 있을 수도 있으니, 안전하게 체크해서 삭제 
		return 1;
	}	
		
	public int mergeUsers(User target, User source){
		return 1;
	}
	
	//등록하고 20분(1시간?) 지난 링크들은 삭제 한다 아니면 지정된 사용자관련 링크를 삭제 한다 
	//등록하고 activation을 기한내에 못한 경우에는 기존 것을 삭제 하고, 신규 발행 해야 한다 
	public void deregisterActLink(long userid){  //if userid =-1이면 지난 시간의 것들을 삭제 함 
		
		activationMap.remove(userid);
		//cleaning old(time-out) ones. if over 5minutes since the creation, remove the link
		if(userid ==-1) {
			for(Map.Entry<Long, ActivationLink> entry: activationMap.entrySet()){
				if((entry.getValue().createdTime+1000*60*20)<System.currentTimeMillis()){
					activationMap.remove(entry);
				}
			}
		}
	}
	
	//질의하는 링크가 있으면 연결된 사용자를 active 상태로 바꾸어 준다(이메일에서 링크를 누른 경우) 
	public boolean activateUser(String testurl){
		for(Map.Entry<Long, ActivationLink> entry: activationMap.entrySet()){
			if(testurl.equals(entry.getValue().url)){
				//log.log(level, type, tags, msg);
				//log.log(Level.INFO, "activationlink matched");
				try {
					User user = userRepository.findOne(entry.getValue().uid);
					user.active=true;
					userRepository.save(user);
					return true;
				} catch(Exception e){
					//log.log(Level.WARNING, "failed to find matching user");
					e.printStackTrace();
				}	
			}
			return false;  //url은 찾았는데 db에서 사용자를 못찾은 경우 
		}
		return false;  //no match
	}
	
	//TODO: 마져구현하자 
	
	
	public int sendMail(User user, int option){
		//resend activationlink, send password reset email
		return 1;
	}

	@Override
	public long backup(Content content) {
		return 0;
	}

	@Override
	public int restore(long target_id) {
		return 0;
	}

	@Override
	public int restore(long target_id, Content contentSnapshot) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int clear(long target_id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int count(long target_id, int inclusive) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		
		
		
		return null;
	}
	
public int resetPassword(long uid){
		
		User user;//repo.findOne(uid) if null exit otherwise
		String newpassword = UUID.randomUUID().toString().replaceAll("-", "");
		
		try {
			user=userRepository.findOne(uid);
			user.password=newpassword;
		} catch(Exception e){
			//log.log(Level.WARNING, "failed to get user, no such existing");
			return 0;
		}
		String title = " your reset password here from journal.kewtea.com";
		String content = "your new password is such..[";
		try {
			mail.sendMail(user.email, title, content+newpassword+"]");
			//송신 후에 실제로 db에 값을 변경하자 
		} catch (MessagingException e) {
			//log.log(Level.WARNING, "failed to send email for resetPassword");
			e.printStackTrace();
			return -1;
		}
		return 1;
	}
	
}
