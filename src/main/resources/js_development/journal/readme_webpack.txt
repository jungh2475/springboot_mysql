
########### install  ########
npm webpack -g



########## webpack.config.json  ##########



########## ngi18n messages #############

//angular-tranlsate library 추가 , 번역 파일 위치 정하기  app/resources/locale-{{locale}}.json : en-US
{
	"abc":"abbb"
}

var app = angular.module('app',[
	'pascalprecht.translate','tmh.dynamicLocale'  //angular-translate and dynmaic-locale
]);

// 사용자 설정값이 있으면 그것으로 locale 설정하고 없으면  브라우져 헤더로 결정, 그리고 setting이 바뀔때마다 $.on으로 설정 변경을 해준다 - 다시 그리기 
app.constant('LOCALES',{'locales':{'kr_KR':'Korean','en_US':'English},'preferredLocale':'en_US' });

app.config(function($translateProvider){
	$translateProvider.translations('en',{
		TITLE: 'Hello',
		FOO: 'This is foo'
	});
	/*  1) 직접 입력 방법 
	$translateProvider.translations('kr',{});
	$translateProvider.translations('fr',{});
	*/
	//파일 import 방법
	$translateProvider.useStaticFilesLoader({ prefix:'resources/locale-',suffix:'.json'});
	$translateProvider.preferredLanguage('en');
	$translateProvider.useLocalStorage(); //saves selected language to localStoagre or Session Storage?
});

app.config(function(tmhDynamicLocaleProvider){
	tmhDynamicLocaleProvider.localeLocationPattern('angular-i18n/angular-locale_{{locale}}.js');
});

app.controller/or service('MainCtrl', function($scope,$translate,LOCALES, tmhDynamicLocale){
	'use strict'
	
	var localesObj=LOCALES.locales;
	//............
	var currentLocale=$translate.proposedLanguage();  //get From LocalStorage or setDefault
	$translate.use(locale);
	//  <span ng-bind-html="'add_card-title' | translate" >hello </span> or ..... <div>{{"value" | translate}}</div>
	
	$rootScope.$on('$translateChangeSuccess',function(event,data){});

});
	