/**
 * 
 * <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
 * html->(body/div) ng-app="" ng-init="init()"  //You can use data-ng-, instead of ng-, if you want to make your page HTML valid
 * 	Using ng-init is not very common,
 * ng-model {{ }}
 
 
 지난 설계의 문제점
 [진행중] view를 string variable로 만들어서 알아 볼수가 없다-에러확인/수정이 너무 힘듬 => html template를 적용(ng-model/bind) + ng-includ="$scope.varUrl"(commentEditor)
 초기화부분/화면크기에 따른 조정등이 필요 (화면 회전 포함) 
 [완료?] 변수명이 다른 library와 충돌이 안나도록 =>angular안에 다 들어가 있음 ...그러나 신규 모듈 function들이 직접 호출되지 못하게 kewClass 밑으로 둘까? 
 너무 큰 덩어리로 되어져 있어서, 이해가 힘들고 다시 디버깅 할수가 없음 - 유지보수/확장 안됨 =>spring의 dependency injection(DI)처럼 의존성 주입하자.
 id대신에 내부 함수들이 uniqueName, url 표현식등으로 정보를 찾고 관리함. (짬뽕이려서 더 헷갈림) => 내부적 참조는 id로만 하자 
 dataServie와 controller 사이의 역할 분담은 어디로(?) 더위로/더아래로?=> dataService는 sessionStorage <-> Server간 동기화만 관리하도록 역할을 낮춘다 
 ($
 sessionStorage Key 및  server api url들이  hardCoding되어져서 유지보수 관리/이해가 힘들다. =>app.config/value/constant로 밖에서 정리하게 
 
 

[controller]
 	1)내부 변수는?
 	
 	
 	2)어떤 method들이 필요한가 
   (upper) - view event에 대응하는 것들
   -leftClick(prev(1)) :shift(move), 하나 추가 getNext(1,direction)From(Id), offset이하면 getNext(n,direction)From(Id)
   -rightClick(next(1)) : ...위하고 같은 방식 임 
   -show Timeline(n)
   -timeline_leftClick(prev(n))
   -timeline_rightClick(next(n))
   -create/update/delete datePage(Content)
   -[crud]ChapterPage(Content)
   -create/update/delete Comment
   -[crud]Inventory(Collection, Keywords)
   -goto Date
   -logout/signin/signup
   -
   (lower) - dataService를 통해 제공해야 할 것들
   - DatePage(read/create/update/delete)
   - Comment(....)
   - Inventory(Collection, Keywords by User)
   
[dataService]
	- getContentByUser(uid)AndIDNext(id,n,direction) :특정 id옆으로 n개를 찾아내서 반환(같은 날이면 이것을 포함해서 찾아내고, 자신을 빼서 계산) 
	- getContentByUser(uid)AndDateBetween(from,to) : 시간 기간동안의 특정 사용자 관련 컨텐츠를 반환 
    
[local/sessionStorage]
    
[springServer-service/repository]
	CRUD - content - get%byUidAndTime, InsertContent->updateOtherContents, getContentsOrderByTime
	CRUD - inventory
	CRUD - comments
 
 
 
 */


var app = angular.module('myApp', []);

/*
 * 
 var mApp = angular.module('mApp', ['ngRoute']);

mApp.config(function($routeProvider){
	$routeProvider
		.when('/',{ templateUrl: 'pages/home.html', controller:'mainController'})
		.when('/about',{ templateUrl: 'pages/about.html', controller:'aboutController'})
		.when('/contact',{ templateUrl: 'pages/contact.html', controller:'contactController',
			resolve: {
	            message: function(messageService){
	                return messageService.getMessage();
	            }
			}
		})
		.otherwise({
	        template : "<h1>None</h1><p>Nothing has been selected,</p>"
	    });
});

//services  생성 - 3가지 방식이 있음, factory, service, provider $get 가장 확장성 높음 
mApp.value("initValue",10);

*/

/*
 * app.constant로 상수 설정값을 만듬 : test, development, production용으로 대응하기 위함임  
 * 
 * app.value(바뀌는 값), app.constant(readonly상수,불변값)

그냥 상수 값만 넣는 대신에 function으로 약간의build를 하고 최종 값을 return 하면 어떨런지?  read: http://www.jvandemo.com/how-to-configure-your-angularjs-application-using-environment-variables/
아니면 window._env에 초기화 (function (window) { window.__env = window.__env || {}; window.__env.apiUrl = 'http://dev.your-api.com';....}(this));
참고로 app.config는 $routeProvider할때 쓰는 것임 

*/
app.constant('app-config',{
	serverHost:'https://kewtea.com:80',
	serverApiStart:'/api/journal', //'api2/journal.....
	localPrefix:'kj_',  //kewteaJournal
	tableObjects:['content', 'inventory', 'comment'],  //subTypes: (datePage -> date, chapter, urlPage), (collection, keyword), (-)
	enableDebug: true
}); 
/*
app.constant('CONFIG',function(){  //안에 k-v로 1대1 하니면  jsonDictionary로 여럿, 심지어  function으로해서 로직으로 변형된 결과값을 넣을수도 있다 
	//Module.value(key, value) is used to inject an editable value, Module.constant(key, value) is used to inject a constant value
	app.value('greeter',{val1:'hello', method1:function(input){}});
	//app.config(function(){});	
});
*/

//app.service('dataService',function($http, CONFIG){});

/* 복합 서비스 예시 
http://odetocode.com/blogs/scott/archive/2014/05/20/using-resolve-in-angularjs-routes.aspx
app.factory("newsControllerInitialData", function(messageService, greetingService, $q) { //내장, custom services들을 넣음
    return function() {
        var message = messageService.getMessage();
        var greeting = greetingService.getGreeting();
 
        return $q.all([message, greeting]).then(function(results){
            return {
                message: results[0],
                greeting: results[1]
            };
        });
    }
});

//app.controller('diaryController',function($scope,dataService){});  


/*
   unit testing
   
   describe -> it -> beforeEach -> assertEqual 
   
*/