/**
 * 
 * 서비스 초기화때 주입할 application.properties 같은 것 : app.config
 * app.constant('settingName', { val1:xx, val2""abc"});  //사용할때는 {{settingName.val1}}, app.service('sName',function($http,settingName){}....
 * 
 * 
 */

app.constant('SVRURLS', (function(){  //한번에 상수들을 가지고 세팅을 만들어서 반환하는 방식 
	var host='http://127.0.0.1:8080';
	
	return {
		
		USERS_API: host + '/users',
		USERS_API2: host + '/users2'
		
	}
})());

app.service('dataService',function($http, $rootScope){
	
	
	this.method1 = function(arg){ return "a"; }
	this.method2 = function(arg){ return new Promise();}
	/*
	 * 만들어야 하는 method들은 ? 앞의 controller를 찾아볼것 
	 * 
	 * 
	 */
	
	this.getContentByTypeAndUserAndTimeBetweenOrderByTime=function(ContentTarget){
		//from getContents
	};
	
	
	
	
});

//////////////// url 상수들 ///////////
app.constant('svrUrl',{
	getContentByUser:"/ac",
	getCommentsByIds:"",
	setComment:"/comment"
	
});


////////////////////////// 중요 부품들 //////////////

function Storage(Adapter){
	this.adapter=Adapter;
	//this.method = adapter.method;
	
}

function KVStorageAdapter(){
	//
	this.getContentByUserAndTimeBetween=function(uid,from,to){
		var key;
		/* 
		 * for (var key in localStorage){  or for (i=0; i<localStorage.length; i++) localStorage.key(i) ->key값 반환 
		 * key = str.indexOf('kew_content') >-1,   localStorage.getItem(key)
		 * 
		 */
		return key;
	}
	
}

/*
		var syncAdapter= new SyncAdapter(mAccount, mLocal, mServer, this);
		syncAdapter.init(mAccount).then(.....) //밖에서 초기화하고, 문제가 있으면 얘한테 또 연락해서 동작하게 해? 
		
*/
//android abstract Syncadapter 참조 바람 
function SyncAdapter(Account, _http_) {  //아니면 (Account, Local, Server, Caller) 이렇게 되어져야 하는 것 아닌감? caller.errorReport 
	//Account - userid,password, or token plus server_url
	this.type="1";
	this.$http=_http_;
	//this.init(Account) load target url mapping with server
	this.lastSyncTime = "NOW"; //configure sync interval
	
	this.needSync = function(){}
	this.performSync = function(){
		
		//$http.get
		
	}
	
	this.compare=function(Local,Server){
		//두개의 값을 비교해서 다르면 Server내용을 Local에 저장한다. (아니면 반대로?)
		return false;
	}
	
	this.init=function(Account){  //초기화나, 계정 업데이트-비번변경등으로...
		//Account -> uid,password, access_token, token_refresh,
		//server_urls
		//return Promise: Promise(ConnectionTrial).then(Caller.report....)
	}
	
	//문제가 생기면 Caller에게 보고하기 (직접 method 호출 혹은 $broadcast)
	
} 

//SyncAdapter.prototpye.[//newly added method] = function ......
		//이녀석들을 Factory 패턴안에 넣어도 되것네 ..조건에 따라 다른 객체들을 만들어서 return...
function SessionStorageAdapter(){}
function SeesionStorageHttpAdapter(Account){}
function GSheetAdapter(Account){}  // for prototype testing
function ChromeSyncAdapter(){}
function ChromeSyncHttpAdapter(Account){}



/*

$broadcast  -$on
	:service : $rootScope.$broadcast('eventName', data);
	:controller: $scope.$on('eventName', function(eventName, data){.....or$rootScope.$on(

$watch ->$apply
	:service :    $rootScope.$apply();
	:controller ;  $scope.$watch('service.getData()', callback(new,old)) or $watch(function(){return service.getData();}, callback...)
		아니면 콘트롤러에서 dataService.methodx().then().bind(this); 사용은 어떤지? : https://www.codementor.io/angularjs/tutorial/keeping-angular-service-list-data-sync-controllers




dependency injection : during conrtsuct only with method (자주처리할려면...)car.setEngine(new V12Engine);
	function Car (options) {  if (options instanceof Object) {    this.setEngine(options.engine);
		var car = new Car({ engine: new V8Engine });
read more : https://angular.io/docs/ts/latest/guide/dependency-injection.html	


singleton(1) var apple = { type:"aaa", method1:function(){ return this.type;};
singleton(20 var apple = new function{ this.type="aaa"; this.medtho1=function(){};};

var p1=new Promise(function(resolve,reject){.....resolve(Result)......reject(errMsg)});
p1.then(function(resp){}).catch(function(errMsg){});
p1에 argument 넣는법  
var p2=function(argument){return new Promise(function(resolve,reject){ if(argument)...resolve(reult)....reject(errMsg)}};
p2(param_value).then(functionSuccess,functionError); or p2(param_value).then().catch();
	Promise.all([p1,p2(arg2)]).then();
	연속된 chaining에서는 꼭 중간에 catch를 넣어서 대응  .then().cath().then().catch...
	
	
공부할것 : https://developer.mozilla.org/en-US/docs/Mozilla/Projects/Rhino/Scripting_Java

*/