/**
 * 
 * 
 * 
   어떤 method들이 필요한가 
   (upper) - view event에 대응하는 것들
   -leftClick(prev(1))
   -rightClick(next(1))
   -show Timeline(n)
   -timeline_leftClick(prev(n))
   -timeline_rightClick(next(n))
   -create/update/delete datePage(Content)
   -[crud]ChapterPage(Content)
   -create/update/delete Comment
   -[crud]Inventory(Collection, Keywords)
   -goto Date
   -logout/signin/signup
   -
   (lower) - dataService를 통해 제공해야 할 것들
   - DatePage(read/create/update/delete)
   - Comment(....)
   - Inventory(Collection, Keywords by User)
   
    
    
    
 */

app.controller('diaryController',function($scope, dataService){
	   //$rootScope, $watch.$on
	
	$scope.value
	$scope.method1 = function(){
		//.....
		//return $scope.value
	}
	
	// 1) init 초기화 
	
	$scope.MainViewPager=[]; //prev, current, next
	$scope.Contents=[];  //datePage array as temp
	
	//서버로 처음에 전달받은 객체들(datePage + comments) 을 Storage에 저장해둠 -> dataService.init()
	init();
	
	// 2) broadcast callback  대응 하는함수들 
	$scope.$on
	
	// 3) view click event 에 대응 하는 함수들 
	$scope.clickGotoBtn = function(){};
	
	
	//$scope.MainViewPager[index]=data;
	//$scope.$apply(function(){$scope.TimelineViewPager[index]=data;});
	//위의 경우는 3rdparty jQuery등을 사용하면 자동 binding이 동작을 못하여서 강제로 $apply로 적용하게 된다 //$( "#btn2" ).click( function(){$scope.text=..
	//$scope.$watch('',function(new,old){},true);
	
	function init(){}
	////// ui event functions
	
	//////// private inner functions
	
	//////////////// $scope.$on
	
	/* 구현해야할 service 기능들 
	 * getNextContentByUserAndTypeWithExtra(uid,type,direction,n)
	 * getContentByUserAndTypeAndTimeBetweenOrderByTime(uid,type,from,to)
	 * getCommentsByUserAndDate
	 * getInventoryByUserAndType(uid,type)
	 * 
	 * 어떤 데이터가 바뀌었는지에 따라 먼져 내 $scope 변수안에 해당되는것이 있는지 확인하고, 있으면 다시 해당 요소를 그린다 
	 * msg: objectType-id : content-23, comment-34, comment/inventory가 신규로 생긴경우는?  
	 * 
	 * function scopeSerach(targetObject) { return true or false - if true then where?}
	 * 해당되는 데이터가 있고, 그 경우에 화면을 다시 그려야 하는 경우 확인해 보자 
	 *   content 바뀐경우 -> datePage, timeline item
	 *   comment 바뀐경우 (내용변경 혹은 신규 생성되어 관련 content와 mapping 될 경우)  -> datePage안의 내용, timeline item안의 갯수표시(3)
	 *   inventory 바뀐 경우(titleStringChange혹은 신규 생성)  -> datePage-> comment의 내용 업데이트 
	 * 
	 * 
	 * 오랫동안 out-tab되어져 있다가 다시 refocus된 경우 : dataService에게 refresh를 요청한다 
	 * (그래서 변동사항이 있으면 controller로 broadcast -> $on으로 전달되어져서 다시 그리게 된다.
	 * 
	 * refresh view를 하는 경우는 dataService에서 가져다가 다시 그리고 이것이 $http fetch를 해서 변동사항이 있으면 연락이 온다 
	 * 화면 resize일 경우도 w를 다시 업데이트하고, 화면 요소들을 다시 계산하게 된다. 
	 * 
	 */
	
	
});


//////////// 중요 부품들 /////////////////

function DiaryViewFactory(){
	
	function buildMainViewPage(Head,Content_DatePage,Comments){}
	function buildTimelineItem(Head,Content_DatePage){}
	function buildCalendar(Contents_DatePage){}
	
}






/*
 * 
 * 
 * 
 * 
*/
