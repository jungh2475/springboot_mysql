/**
 * 
 */

var app = angular.module("myApp", []);
//add $routeProvider, $locationProvider

app.controller('diaryController' ,function($scope,DataService) {
	$scope.
		viewPages=[];
		currentPageIndex
		pageObject.date
			.comments[] -> collectionId[],keywordId[],ref.imgUrl,ref....
		timeline=[]
		
	/*
	 *  좌우이동 : next(), prev()
	 *  메뉴보이기
	 *  gotoday(day) 
	 *  show/hideTimeline
	 *  addComment
	 *  observer에서 제거 
	 */
	
	DataService.registerObserver(this);
			
	this.method1 = function($scope,DataService){
	
				DataService.methodPromise.then(
						$scope.value1=input;
				);
	};		
	
	//DataService observer call
	this.update = function(url,data){
		//$scope의 값들을 update해주면 된다 
	};
			
			
	//$scope.watchCollection(['variableArray','obj.a','obj.b'], function(oldvalue,newvalue,scope){ .....  });
});

function DataService(){
	observers=[];
	this.addObserver()=function(){};
	this.removeObserver(id)
	this.notifyObservers()
	
	this.getServer(url)=function(){}
	this.getLocal(url)=function(){}
	
}




/**
 *  
 		1)angular 선언 
 		var app = angular.module('myApp', []);
 		//app.constant('magicNumber', 42);
 		
 			- bootstrap, jQuery 장착 
 				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
				<!-- @todo To use Bootstrap components from the AngularJS app, switch to UI Bootstrap. -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.js"></script>
				<script src="appmain.js"></script>
 			- .....
 
 		2)$routeProvider : controller, templateUrl or resolve로 값을 넣어 주어서 각각의 페이지들을 만든다 
 		lazyLoading을 고민할 필요는 있음
 		
 		 	- html5.history api on with $locationProvider
 		
 		3)service를 만들어서 controller에 주입한다 
 		app.controller('FooController', function($http,$scope,ExtendedService){
 			$scope.contacts = ExtendedService.list();
 			$scope.selectUser = function(user) {
        		// @todo Send user to UserDetailController.
    		};
    		
 		app.service('UserService', function(){
			this.users = ['John', 'James', 'Jake'];
			this.method1 = function() {
				//..
			}  //아니면 이부분을 통째로 function name을 넣어주는 것이 더 깔끔, ('NgUserService', UserService) 
		});
		app.service('ExtendedService',function(UserService){.....
		
		
 		
 		4)observer pattern :controller.update(url)을 만들어서, 객체모델과 뷰를 연동시킨다 
 		 	targetUrl이 정해진다음에 어떻게 view객체들을 찾아서 넣어주어야 하는가? 
 		 		observers=[];  //ng-23
 		 		observerUrls=[]; // comments,inventorys/uid=x,cType=collection
 		 	ng-model
 		 	controller안에서 구현?
 		 	service안에서 구현?
 		 	<script>안에서 구현 -view_id를 알아야 함. 
 		
 			source: http://egidiocaprino.it/posts/angularjs-passing-data-to-a-controller-with-the-observer-pattern
 		
 		5)global variable,objects in angular javascript: custom + built-in
 		
 */




angular.module('app', [])
    .factory('TestService', function () {
    var _subscribers = [];

    setInterval(function () {
        // every 1 second, notify all subscribers
        console.log(_subscribers);
        angular.forEach(_subscribers, function (cb) {
            cb('something special @ ' + new Date());
        });
    }, 2500);

    return {
        subscribe: function (cb) {
            _subscribers.push(cb);
        }
    };
})
    .controller('testController', function ($scope, TestService) {
    $scope.notifications = ['nothing yet'];

    TestService.subscribe(function (notification) {
        $scope.$apply(function () {
            $scope.notifications.push('got ' + notification);
        });
    });
});