/*

groovysh
groovyc MyClass.groovy -> MyClass.class as bytecode
-------------
Spring Boot CLI can be installed manually; using SDKMAN! (the SDK Manager) or using Homebrew

sdk install springboot
brew tap pivotal/tap
$ brew install springboot
spring run app.groovy

*/

@RestController
class ThisWillActuallyRun {
    @RequestMapping("/")
    String home() {
      println  "Hello World!"
      }
}


/*


*/
