
package com.kewtea.groovytest
//package groovy.test.groovy.201707
import groovy.text.SimpleTemplateEngine
//import org.codehaus.groovy.text.SimpleTemplateEngine
/*

  groovy example.groovy
  class compile: groovyc Hello.groovy, the Groovy runtime library must included in the Java classpath
  spring run Example.groovy (gvm install springboot, list springboot , current - curl -s get.gvmtool.net | bash)

  curl -s get.sdkman.io | bash => source "/Users/jungh/.sdkman/bin/sdkman-init.sh"
  sdk list(install) springboot sdk list groovy , sdk use scala 2.12.1, sdk current java, sdk install java

  brew install groovy , (sudo apt-add-repository ppa:groovy-dev/groovy ..update...sudo apt-get install groovy or sdkman), groovy -version
  maven
      <groupId>org.codehaus.groovy</groupId>
      <artifactId>groovy</artifactId>
      <version>2.4.5</version>

  path in .profile
    export GROOVY_HOME=/usr/local/homebrew/opt/groovy/libexec
    export PATH=$GROOVY_HOME/bin:$PATH

  p = new Person(firstName: "Peter", lastName:"Mueller"); //자동으로 named matching 실행
    p.setFirstName("Lars"), def address, p.address = ("Homestreet 3");

*/


class Example {
   static void main(String[] args) {
      // Using a simple println statement to print output to the console
      def x = 5;
      //def x = (y > 1) ? "worked" : "failed"
      def range = 5..10;  //range.get(2)
      def sum = { a, b -> println a+b }; //sum(2,4)
      //def someMethod(parameter1, parameter2 = 0, parameter3 = 0)
      println('Hello World '+x);
      println("hihi $x");

      //def engine = new SimpleTemplateEngine()
/*
import groovy.text.SimpleTemplateEngine

      String templateText = '''Project report:

We have currently ${tasks.size} number of items with a total duration of $duration.
<% tasks.each { %>- $it.summary
<% } %>

'''
def list = [
    new Task(summary:"Learn Groovy", duration:4),
    new Task(summary:"Learn Grails", duration:12)]
def totalDuration = 0
list.each {totalDuration += it.duration}

def engine = new SimpleTemplateEngine()
def template = engine.createTemplate(templateText)

def binding = [
duration: "$totalDuration",
tasks: list]

println template.make(binding).toString()
*/
   }
}


/*

  file read & write
      new File("E:/Example.txt").eachLine {line -> println "line : $line";}
      File file = new File("E:/Example.txt"), println file.text
      new File('E:/','Example.txt').withWriter('utf-8') {  writer -> writer.writeLine 'Hello World'



*/
