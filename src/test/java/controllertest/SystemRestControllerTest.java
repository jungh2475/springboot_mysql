package controllertest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.kewtea.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class SystemRestControllerTest {  //extends AbstractJUnit4SpringContextTests

	
	MockMvc mockMvc;
	
	//@Autowired
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Before
    public void setup() throws Exception {
		//mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
		//load data
	}
	
	@Test
	public void test() throws Exception{
		//moved here
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
}
