package servicetest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;

import static org.hamcrest.CoreMatchers.*;  //이것을 넣어야 is() function을 쓸수있음 

import com.kewtea.Application;

/*
 *  http://aoruqjfu.fun25.co.kr/index.php/post/1020  ---- 이것을 꼭 읽어 볼것 
 *   @MockBean  public UserService userService;,,, given(userService.findOne(1L)).willReturn(new User(1L,,,,
 *   mvc.perform(get("/findOne/{id}", 1L).accept.....andExpect(jsonPath("$.id", is(1)))
 *  @DataJpaTest
 *  
 *  testing는  mvn clean install로 실행을 해보고,  running은 그냥 mvn spring-boot:run으로 한다 
 *  
 *  TO-DO : rest api 테스팅 케이스 추가 restTemplate.exchage method 사용 , restassured를 활용한스팅 
 *  나중에 com.jayway.restassured:rest-assured POM에 추가하여, 
 *  when().get(urlString).then.statusCode(HttpStatus.OK.value()).body("property",is(object.property)).body...
 *  when().post("").then().statusCode().body().body().....
 *  given().body(object).contentType(ContentType.JSON).and().when().post("url")
 *  .then()......
 *  
 *  

예전 방식은 다른 포트를 골라내서(0) 통신을 주입하도록 되어져 있었는데, 이를 쓰지 않는 간편한 방식으로 테스트 한다 
예전방식 
@RunWith(SpringJUnit4ClassRunner.class)  //
@WebAppConfiguration  //@AppConfiguration?, @IntegrationTest("server.port:0")  //0번포트 사용이 아닌 현재 비어있는 포트로 테스트 수
@Value("0")//("${local.server.port}")  //lombok이아니라 spring annotation임 
int port;
ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:"+port,String.class);
restTemplate.getObject...

@WebAppConfiguration("src/test/webapp") // used to build a MockMvc object, src/main/webapp
@ContextConfiguration(classes = WebConfig.class)    //@EnableWebMvc extends WebMvcConfigurerAdapter
@Autowired  private WebApplicationContext webAppContext;
  
 */

@RunWith(SpringRunner.class)  //SpringJUnit4ClassRunner.class
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)//@SpringApplicationConfiguration(classes = Application.class)  //
public class ApplicationTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	TestRestTemplate restTemplate = new TestRestTemplate();
	String apiEndpoint;  //before나 test에서 준비시킨다 
	
	@Before
	public void setup() throws Exception{}
	
	@Test//to-by-pass use @Ignore
	public void testHome() throws Exception {
		
		/*
		 * 기본적인 구성을 확인함 
		 * Jpa Data Rest로 load한 데이터가 잘 있는지,
		 * 
		 * 
		 * 
		 * 
		 */
		
		
		//rest end points
		//http://localhost:8080/resources/abc or http://localhost:8080/resources/users
		//http://localhost:8080/test/ 
		
		ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:",String.class);
		
		
		
		//항상 header값과 응답값을 같이 검사한다 
		
		//assertEquals(A,B), assertNotNull/True/False(condition_object)
		//assertThat(optional_reason, actual, is()_matcher)
		assertThat(response.getStatusCode(), is(HttpStatus.OK));
		//assertThat(response.getBody(), is("Hello World!"));  //testing용 failure
		
		/*
		 * 옛날 방법
		 * Assert.assertEquals(expected, actual);
		Assert.assertNotNull(str);  //assert를 쓰지 말자 
		 */
		
	}
	
	@Test
	@Ignore  //skipped로 표시됨 
	public void testWrite(){
		
		Assert.assertTrue(true);
		assertTrue(true);
		
	}
	
	@After
	public void tearDown(){}

}
