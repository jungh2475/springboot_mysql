package servicetest;


import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.kewtea.Application;
import com.kewtea.models.ContentJournal;
import com.kewtea.services.JournalService;

@RunWith(SpringJUnit4ClassRunner.class)  //
@SpringApplicationConfiguration(classes = Application.class)  //
@WebAppConfiguration  //@AppConfiguration?
@IntegrationTest("server.port:0")
public class JournalTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JournalService journalService;
	
	
	@Value("${local.server.port}")  //lombok이아니라 spring annotation임 
	int port;
	RestTemplate restTemplate = new TestRestTemplate();  //RestTemplate에서 확장, 오류처리기능 밑 인증설정이 좀 더 용이 
	
	@Test//to-by-pass use @Ignore
	public void serviceTest(){
		
		//서비스 검사 
		List<ContentJournal> cjList=journalService.getDatePages(1, 20160601, 20160602, -1);
		if(cjList !=null) {logger.info("items["+cjList.size()+"]");}
		logger.info("journalService test completed!");
		//Assert.assertTrue(true);
		
		long[] deleted = journalService.deleteDatePage(2L);
		logger.info("deleted["+deleted+"]");
		
		//Repository Rest Api Testing - getcontents, 앞에서 삭제 했으면 목록이 줄어 들었는가?
		/*
		 * 	http://www.baeldung.com/rest-template
		 * 	HttpEntity<Foo> request = new HttpEntity<>(updatedInstance, headers);
		 * 	postForObject(url,request,class)/postForLocation(url,request), exchange(url,HttpMethod.POST,request,class)
		 */
		
		String url="/api/journal/";
		//Example: ContentJournal cj=restTemplate.getForObject(url,ContentJournal.class);
		//restTemplate.getForObject(url, responseType, urlVariables) 
		
		//journalrestcontroller rest api 검
		
	}

}
