package servicetest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.kewtea.Application;
import com.kewtea.repositories.DatabaseBackupRestore;

@RunWith(SpringJUnit4ClassRunner.class)  //
@SpringApplicationConfiguration(classes = Application.class)  //
@WebAppConfiguration  //@AppConfiguration?
@IntegrationTest("server.port:0")
public class SystemTest {
	
protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DatabaseBackupRestore dbBackupRestore;
	
	@Value("${local.server.port}")  //lombok이아니라 spring annotation임 
	int port;
	//RestTemplate restTemplate = new TestRestTemplate();
	//String apiEndpoint;  //before나 test에서 준비시킨다
	
	@Before
	public void setup(){}
	
	@Test//to-by-pass use @Ignore
	public void myTest(){
		
	}
	
	@Test//to-by-pass use @Ignore
	public void mySecondTest(){
		
	}
	
	@After
	public void tearDown(){}
	
}
