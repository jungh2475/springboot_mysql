package unittest;

import org.junit.Test;

import junit.framework.TestCase;

public class SimpleTest extends TestCase {
	
	//@Before, @Aftrer, 
	// @Ignore, @Test(timeout=100) Fails if the method takes longer than 100 milliseconds.
	//@Test (expected = Exception.class) Fails if the method does not throw the named exception
	
	@Test  //Methods that are not annotated with @Test are not executed by the test runner
	public void testSomething(){
		int expected =7;
		SubjectClass sC= new SubjectClass("hi");
		sC.printMessage();
		int actual= 7; //sC......
		assertEquals("as expected...", expected, actual);
	}
	
	/*
	 예외발생이 되게 하는 코드의 테스트는.....
	 try {
      calc.divide(2, 0);
      fail("Should have thrown an exception!");
    }
    catch (ArithmeticException e) {
      // Good, that's what we expect
    }
	*/
}

/*  To use JUnit in your Maven build, Eclipse IDE ships with a version of JUnit, no download required
	<dependency>
	    <groupId>junit</groupId>
	    <artifactId>junit</artifactId>
	    <version>4.12</version>
	</dependency>
*/