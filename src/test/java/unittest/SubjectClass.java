package unittest;

public class SubjectClass {
	
	private String message;
	
	public SubjectClass(String msg){
		this.message=msg;
	}
	
	public String printMessage(){
		System.out.println(message);
		return message;
	}

}
