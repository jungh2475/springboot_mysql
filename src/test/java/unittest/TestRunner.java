package unittest;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

//import com.kewtea.models.json.SimpleTest;

/*
 * Testing methods
 * 1)eclipse junit testing - classpath error , not found, build project
 * 2)java run
 * 		2-1)javac -cp .:junit-X.Y.jarTestBasic.java -> java org.junit.runner.JUnitCore TestBasic  
 * 			- download junit.jar export $CLASSPATH : /absolute/path/for/compiled/classes
 *     export CLASSPATH="$CLASSPATH:$JUNIT_HOME/junit-X.Y.jar:..."
 * 		2-2)java Testrunner (testrunner.java를 compile해서 그 안에서 테스트 수행)
 * 3)mvn clean test or mvn -Dtest=TestApp1 test :maven으로 build/compile하고 테스트 실행해서 편하나, 하위에 maven 프로젝트/폴더들이만들어짐 (비추)
 *    오히여 -Dtest는 spring boot에서 전체 테스트 하면 너무 오래 걸릴때 해보자 ...
 *     mvn install -Dmaven.test.skip=true 
 * 4)spring cli: spring run or spring test app.groovy test.groovy
 	add echo $CLASSPATH
 		export
 	javac SubjectClass.java SimpleTest.java TestRunner.java
 	java TestRunner
 * 
 */

public class TestRunner {
	
	public static void main(String[] args){
		Result result = JUnitCore.runClasses(SimpleTest.class);
		
		for(Failure failure: result.getFailures()){
			System.out.println(failure.toString());
		}
		
		System.out.println(result.wasSuccessful());
	}
	

}
