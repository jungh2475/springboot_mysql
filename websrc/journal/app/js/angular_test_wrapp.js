//angular test wrapper

//require('angular');  //<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
//require('./myangular.js');

//var appang=require('angular');


describe('JavaScript addition operator', function () {
    it('adds two numbers together', function () {
        expect(1 + 2).toEqual(3);
    });
});

var angular = require('angular');  //npm install angular-node
//see https://medium.com/@dickeyxxx/best-practices-for-building-angular-js-apps-266c1a4a6917#.1f2n6yvbz, https://www.sitepoint.com/using-requirejs-angularjs-applications/
require('./myangular.js');

/*
angular.module('myMod', [
require('angular-animate'),
require('angular-mocks/ngMock'),
require('angular-mocks/ngAnimateMock'),
require('angular-i18n/en-us')
]);

*/

describe('wrapped angular testing', function () {
    it('acalling wrapped angular', function () {

        //
        expect(1 + 2).toEqual(3);
    });
});
