//node,


/*
  최종적으로 구현하고 싶은 표현방식은 ., url-http service
  var result = someService.getX(parma1, param2);
  someService.getX = httpService['aaa'].setParam(['a','b']).build();
  너무 많은 switch/if 문들을 줄여 보기

*/

//system.config

var app = angular.module('app', []);
app.constant('url', urlsmap);  //or app.value('movieTitle', 'The Matrix');
app.service('DataService',['url',function DataService(url){
    //url.->access Values....
}]);

var httpService=function(headers){
  var headers=headers;
  var startingUrl= "http://abc.com/apiv2"//sys.config("domainUrl");
  var tailUrl ="";  //"/";
  return {
    setParam: function(param){
      tailUrl=tailUrl+param;
      return this;
    },
    setParam2: function2(param){
      tailUrl=tailUrl+param;
      return this;
    },
    build:function(url_i){
      return {
        url_called: startingUrl+tailUrl,
        result:$http.get(url,)
      };
    },
    build2: function(number) {
      return new Legion(leader, adjutants, number);
    }
  }
};

var result = httpService('aaa'); //

//.success(....or_error(function(data, status, headers, config)

$http.get(url,{headers:{'Authorization': 'Basic'}}).success(function(data){ return data;});
//$http.post(url,dataObject)

var urlsmap = {
  aaa:'http://abc.com/url1',
  bbb:'http://abc.com/url2'
};

function DataService(name){
  this.name=name;
  this.method1=function(url){
    console.log(url);
  };
  this.method2=function(url){
    console.log(url);
  };
}

var DS = new DataService('apple');

//module.exports = DS;
//var DS = require("builderpattern.js");

//사용할때는  node -> var TT= require('./builderpattern.js');
module.exports = {
  DS:DS,
  urlsmap:urlsmap
};

//module.exports = 'Hello';

function BuilderOut(url, param1, param2){

}


/*
    webpack과 같이 사용하기
    npm install -g webpack
    npm install --save style-loader css-loader
    npm install --save node-sass sass-loader
    npm install --save babel-loader babel-core babel-preset-es2015  //구버젼 호환성
    npm install --save eslint eslint-loader  //정적 오류 찾기

    // hello.js
      module.exports = 'Hello';
    // entry.js
      var hello = require('./hello');
      var world = require('./world');
      require('!style!css!./style.css');
      require('!style!css!sass!./style.sass');
      or ....import hello from './hello';

    webpack entry.js bundle.js  혹은 아래 파일을 만들고 해당 폴더에서 webpack 명령어만 실행
    (require('./style.sass');도 단순화해짐 )
    // webpack.config.js
        module.exports = {
          entry: './entry.js',
          output: {
            path: __dirname,
            filename: 'bundle.js'
          },
          module: {
            loaders: [
              { test: /\.sass$/, loader: 'style!css!sass' }
            ]
          }
        };

*/
