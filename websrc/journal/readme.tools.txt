

nodejs+npm
jasmine
selenium
webpack


1) test하는 방식 
	1.1 html <script> including jasmine + mock 
	1.2 jasmine-cli with jasmine.json : 이 방식을 선호함
	1.3 webpack : npm test 명령어 : release전에 일부(a few) 테스트만 수행
	
	unit_test with jasmine spyon. jasmine.createSpyObject()
	selenium unittesting with PageObjectModel로 영역들을 class로 나누어서 합치기  

2) js/css build & compile : webpack
	- bundle_[chunkhasn].js, style_[chunkhash].css

3) compliance checklist
	- google page insight
	- google analytics 포함되어져 있나?
	- loggingService가 장착되어져 있는가 ? 그 안에, error/performance 항목들을 수집하는가?   


https://jasmine.github.io/2.0/introduction.html

install

spec
xdescribe, xit

https://volaresystems.com/blog/post/2014/12/10/Mocking-calls-with-Jasmine
http://ng-learn.org/2014/08/Testing_Promises_with_Jasmine_Provide_Spy/

외부 모듈(dataService, ajax등을 흉내내고 싶을때 사용) 
spyOn(foo, 'setBar').and.returnValue(745); .and.throwError("quux"); .and.callFake(function(arguments, can, be, received) {
      return 1001; });....and.callThrough();
foo.setBar(123);
expect(foo.setBar.calls.count()).toEqual(0);

var whatAmI = jasmine.createSpyObj('whatAmI',['method1','method2','method3']);
spyOn(whatAmI,'method1').and.reurnValue(12);
그리고 이것들을 async promise관계로 호출하게 해주어야지 실제로 쓸모가 있다 
-------------
spyOn(mockService, 'one').and.returnValue(deferred.promise);

spyOn(myOtherService, "makeRemoteCallReturningPromise").and.callFake(function() {
        var deferred = $q.defer();
        deferred.resolve('Remote call result');
        return deferred.promise;
    });

spyOn(Contact, 'retrieveContactInfo').and.callFake(function() {
      return {
        then: function(callback) { return callback(user); }
      };
    });

--------------
var JasmineHelpers = function () {
 
    var deferredSuccess = function (args) {
        var d = $.Deferred();
        d.resolve(args);
        return d.promise();
    };
 
    var deferredFailure = function (args) {
        var d = $.Deferred();
        d.reject(args);
        return d.promise();
    };
 
    return {
        deferredSuccess: deferredSuccess,
        deferredFailure: deferredFailure
    };
};

describe("Testing spies", function () {
    var jasmineHelpers = new JasmineHelpers();.....
    	spyOn(myApp, "testAsync").and.callFake(function () {
            return jasmineHelpers.deferredSuccess();
        });
    
    myApp.testAsync().always(function(result){
    	expect(result).toEqual("The async call succeeded");
    });
    