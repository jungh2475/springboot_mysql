/**
 * 
 */
'use strict';

describe('calculator1', function () {
		it('1 + 1 should equal 2', function() {
			expect(calculator.sum(1, 1)).toBe(2);
		});
});
	

describe('calculator2', function(){
	//beforeEach(angular.mock.module('calculatorApp'));  //calculatorApp
	describe('sub_group1',function(){
		it('sub_test1', function(){
			expect(calculator.sum(3, 4)).toBe(7);
		});
	});
});

//https://docs.angularjs.org/guide/unit-testing
describe('calculator3', function () {
		
		//beforeEach(module('myCalculatorApp'));
		beforeEach(angular.mock.module('myCalculatorApp'));//여기가 무슨 문제지?
				//angular.module('calculatorApp'); // i.e. getInstance
				//while angular.module('calculatorApp', []); // i.e. createInstance

		var $controller;

		beforeEach(angular.mock.inject(function(_$controller_){
		  $controller = _$controller_;
		}));
		
		describe('sum', function () {
			it('1 + 1 should equal 2-2', function () {
				var $scope = {};
				var controller = $controller('CalculatorController', { $scope: $scope });
				$scope.x = 1;
				$scope.y = 2;
				$scope.sum();
				expect($scope.z).toBe(3);
			});
		});
		
});

describe("dataService api test", function(){
	var CalculatorService, httpBackend;
	beforeEach(angular.mock.module('myCalculatorApp'));
	
	beforeEach(inject(function (_calculatorService_, $httpBackend) {
		CalculatorService = _calculatorService_;
	    httpBackend = $httpBackend;
	  }));
	
	it('cal_square', function(){
		//httpBackend.whenGet....
		expect(CalculatorService.square(2)).toBe(4);
	});
	
});	

//dataService의 testcase들을 나열해 보자 
/*
	1.getUser
	2.getDatePage
	3.getTimeline
	4.create...
	5.update....
	6.delete....net
	


*/