/*
 * 
 * to run on commandline
  osx
  brew install node  //노드까지만 brew로 깔고, 그러면 npm도 같이 설치됨. brew install jasmine
  npm install -g jasmine  //그리고 jasmine을 설치함 
  cd "source folder" //해당 폴더로 이동 
  jasmine init
  jasmine testspec/testspec1.js
  
  //기타 옾션들 
  jasmine  //이걸로 하면 jasmine.json을 가지고서 설정을 한다 
  jasmine spec/appSpec.js
  
  * 
 * 
 * ubuntu-debian
 */
  
  //### Customize spec/support/jasmine.json
  //jasmine JASMINE_CONFIG_PATH=relative/path/to/your/jasmine.json
  //https://jasmine.github.io/2.4/node.html#section-Configuration
  
  //jasmine.json
  //{"spec_dir": "spec","spec_files": ["**/*[sS]pec.js"],"helpers": ["helpers/**/*.js" ],stopSpecOnExpectationFailure: false, random: false}
  
 /* 
  
  예전에 하던 방식 
  
  npm install jasmine-node -g --save
  //npm install express --save ...request
  ./node_packages/.bin/jasmine-node testspec
  
  jasmine-node --autotest //to let the specs run on every change automatically
  
  npm init ->answer questions to build package.json file
  package.json:  { ..."main": "index.js", "scripts": { "test": "./node_modules/.bin/jasmine-node testspec"  }...}
  or "main": "app.js","dependencies": {"express": "^4.13.3","request": "^2.65.0" },
  "devDependencies": { "jasmine-node": "^1.14.5" },"scripts": {"test": "jasmine-node spec" },
 
  npm test
 
 * 
 * 
 * 
 */


var app = require("../app.js");
//var app = require("../src/app.js");

//testGroup
describe( "Convert library", function () {

	//testCase 1
	describe('JavaScript addition operator', function () {
	    
		//test_point(spec)1
		it('adds two numbers together', function () {
			//app.xxxx
	        expect(1 + 2).toEqual(3);
	    });
		
		it("contains spec with an expectation", function() {
		    expect(true).toBe(true);
		  });
		it("2nd fail contains spec with an expectation", function() {
		    expect(true).toBe(true);
		  });
		
	});

});