/**
 * 
 * 
 * 셀레니움을 써서 end user browser 테스트도 자동화하고, 서로다른 브라우져들(chrome, safari, firefox, ms edge)에서도 자동으로 반복 테스트하게 할수가 있다 
 * 
 * source from 
 	http://www.techinsight.io/review/devops-and-automation/browser-automation-using-selenium-webdriver-and-nodejs/
 	https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Your_own_automation_environment
 * 
 * 
 *  install from mac osx
 brew info nodejs or node or npm
 테스트 폴더가 없으면 만들고, 그 안에 스크립트를 넣고, npm init
 npm install --save selenium-webdriver  //java도 설치되어져 있어야 하나? (필요없어 보임) 
 자스민이 없다면 설치할것 npm install --save jasmine, jasmine init
 
 
 */


//filename.js -> to run : at command line : node filename


var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

var driver = new webdriver.Builder()
    .forBrowser('firefox')
    .build();

driver.get('http://www.google.com');
driver.findElement(By.name('q')).sendKeys('webdriver');
driver.findElement(By.name('btnG')).click();

driver.sleep(2000).then(function() {
  driver.getTitle().then(function(title) {
    if(title === 'webdriver - Google Search') {
      console.log('Test passed');
    } else {
      console.log('Test failed');
    }
  });
});

driver.quit();


//jasmine을 통한 테스트 방법 jasmine tech.review/spec/integration-test.js

var selenium = require('selenium-webdriver');

describe('Selenium Tutorial', function() {

    // Open the TECH.insight website in the browser before each test is run
    beforeEach(function(done) {
        this.driver = new selenium.Builder().
            withCapabilities(selenium.Capabilities.chrome()).
            build();

        this.driver.get('http://www.techinsight.io/').then(done);
    });

    // Close the website after each test is run (so that it is opened fresh each time)
    afterEach(function(done) {
        this.driver.quit().then(done);
    });

    // Test to ensure we are on the home page by checking the <body> tag id attribute
    it('Should be on the home page', function(done) {
        var element = this.driver.findElement(selenium.By.tagName('body'));

        element.getAttribute('id').then(function(id) {
            expect(id).toBe('home');
            done();
        });
    });

    // Test the navigation bar by clicking on the 'REVIEW' link and checking the URL changes to '/review'
    it('Has a working nav', function(done) {
        var element = this.driver.findElement(selenium.By.linkText('REVIEW'));

        element.click();

        this.driver.getCurrentUrl().then(function(value) {
            expect(value).toContain('/review');
            done();
        });
    });
});


/*
 * journal webDiary와 crx(chrome extension)에서 테스트할 시나리오들 
 * 
준비해야할 데이터들
userX(jungh)의 데이터들 : 2016-02-10, 2016-02-11, ChapterPage(2016/02/10-11), 2016-02-15

webDiary
	- 사용자-방문자사이트들을 바꾸어가면서 동일 페이지 접속하는 시나리오 
		(anoymous -> jungh 2-11페이지 방문) (sangsoo->-> jungh 2-11페이지 방문) (jungh->-> jungh 2-11페이지 방문)
	- jungh, from 2016-02-15에서 menu goto : 2016-02-10
	- sangsoo or anoymous, add comment on jung's page => error
	- jungh , add comment on jung's page =>success, add past Page => success
	- ***이거 하나만할까? remove comment => remove past Date => show timeline => got to another date page

crx 

준비할 데이터 
userX(jungh)의 데이터들 : bbc.com/abc =>comment 2개  xyz.com/aaa => comment 1개 collection 1개, 키워드 2
userY(sangsoo)의 데이터들 : bbc.com/abc => comment2개, xyz.com/aaa => comment 2개 collection 2개, 키워드 3

시나리오
  userX(mode2)로 bbc.com/abc 방문하여 sidenote 및 summary note 확인
  userX로 bbc.com/abc에서 comment 추가, collection, keyword 추가하여 summaryPage에서 내용 확인
  userX가 mode 1<->2로 왓다 갔다해도 mode2의 데이터가 그대로 있는 지 확인 하기 
  (mode2에서 mode1으로 가서 comment를 추가하여 생성 확인하고, -> mode2로 변경하여 내용이 없어진것을 확인 )
   

selenium에서 html dom에서 조작하는 법  
var element = this.driver.findElement(selenium.By.tagName('body'));  //selenium.By.linkText('REVIEW')
element.getAttribute('id').then(function(id) {
    expect(id).toBe('home');
    done();
});
element.click();
this.driver.getCurrentUrl().then(function(value) {....
 
 
 */
