/* 
 * 
 * [installation]
 * npm init --y  //creates package.json
 * npm install webpack -g
 * npm i -D webpack
 * 
 * npm install --save-dev style-loader css-loader
 * require("css!./stylesheet.css"); require("!style!css!./style.css");
 * import css from 'file.css';
 * import css from 'style-loader!css-loader!./file.css';
 * 
 * [other config in js/html]
 * dev.js: require("./alert.js"); document.write(require("./content.js"));
 * <script src="build/bundle.js"></script>
 * 
 * 
 * or jasmine cli : npm install -g jasmine,jasmine init,jasmine examples, jasmine.json, ->jasmine spec/appSpec.js
   https://jasmine.github.io/2.1/node.html
 * 
 * npm test -> https://semaphoreci.com/community/tutorials/getting-started-with-node-js-and-jasmine
 * 
 * package.json -> "scripts": {
	    	  "test": "jasmine-node spec", //"test": "echo 'Error: no test specified' && exit 1",
	    	  "test2": "./node_modules/.bin/jasmine-node spec",
	    	  "start": "webpack && open index.html"
	    	},  -> npm start or npm test
	
	npm install jasmine-node --save-dev
	...at spec/testspec.js....var request = require("request");...describe
	
 * 	
 * 
 * [run webpack]
 * webpack ./entry.js bundle.js
 * webpack test
 * webpack start
 * webpack --watch
 * 
 */

module.exports = {
		entry: {   //entry: './src', or entry: ["./global.js", "./app.js"],
	        bundle: "./posts",
	        post: "./post",
	        main: "./src/index.js"
	    },
	    output: {
	    	path: path.join(__dirname, 'build'),
	    	filename: "[name].[chunkhash].js",  //main.1555676.js ..or bundle.js?cache=170315
	        chunkFilename: "[id].js"
	    },
	    
	    
		module: {
			loaders: [
	            { test: /\.css$/, loader: "style!css" }
	        ],
		    rules: [
		      {
		        test: /\.css$/,
		        use: [ 'style-loader', 'css-loader' ]
		      }
		    ]
		}
}

