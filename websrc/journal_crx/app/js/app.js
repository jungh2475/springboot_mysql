/**
 *   
 	ng-repeat: $index(=i from 0), $first, $last(=boolean)
 	구역을 감싸고 싶으면 <p ng-repeat-start="a in as|orderBy:"years"'>....<p ng-repeat-end>
 	angular filter {{ regDate | date:'yyyy-MM-dd'}}, 
 	객체를 이용한 필터 :ng-repeat='member in members |filter:search|orderBy:criteria'
 	$http.jsonp, $http.post/put/get/delete
 	$resource
 	ngAnimate는 CSS3 기반 키프레임방식임 (ngView, ngRepeat,ngIf, ngModel에 선택적 적용 가능) 
 	ng-enter, ng-leave
 	<div ng-view class="container {{pageClass}}">
 	.pageClassA.ng-enter { animation:animationName 2s;} //duration
 	.pageClassA.ng-leave {....}
 	.pageClassB.ng-enter { animation:animationName 2s;} //duration
 	.pageClassB.ng-leave {....}
 	css3: @keyframes(or @-webkit-keyframes) slideinName { from{property1:100%;property2:100%} to{....}}
 	.class { animation-duration:3s; animation-name:above;
 */

var app=angular.module("app", ['ngRoute','ngAnimate']);
	app.config(function($routeProvider,$locationProvider){
		$routeProvider
			.when('/abc', {templateUrl:'', controller:,
				resolve:{}})  //resolve를 쓰면 controller동작전에 값을 구해서 주입, 더 느려지나? contoller에 data, $routeParams argument 필요  
			.when()
			.otherwise({redirectTo:'/abc'});
		$locationProvider
			.html5Mode(true); //hashPrefix('!');
	});

	app.service('dataService',DataService);
	//app.controller('scrapbookController',function($scope,dataStorage){});//두개로 쪼개고 싶으면, 아니면 그냥 한개 controller도 가능함 
	//controller에는 간단한 로직들만 넣어둔다, 서비스 ui 행동들에 대한 대응 정도 ....
	app.controller('sidenoteController',function($scope,dataService){   //각종 클릭에대한 대응 및 dataObserverUpdate
		//variables:  userid,accessToken은 따로 저장해 둘까? 
		
		$scope.dataService
		$scope.variables;//예시 
		//page상태 - 현재 페이지 url, 
		dataSservice.getdataPromise.then(
				$scope.keywords=response;
		).catch();
		
		$scope.collections
		$scope.comments
		
		//methods: click, next/prev, ....
		$scope.add=function(){};
		$scope.openTab=function(url){}; //이미 열린곳이 있으면 그곳을 highlight한다 
		
		
		
		//observer callback from dataService
		$scope.update=function(url,data){
			$scope.variables=data// or =dataStorage.getData
		};
		$scope.watch('dataService.data',function(oldvalues,newvalues){
			
		});
		$scope.watchCollection([], function(oldvalues,newvalues){  //$scope.watch()
			
		}); 

	});  
	 
see more at https://angular-tutorial.quora.com/Make-a-Todo-Chrome-Extension-with-AngularJS-1	
--------------
<script>

//데이터 초기화 및 저장소 생성 후에 page관리자 만들고 해당 페이지 등록함. 
var url1="test.json";
var div_id = 'sample';
var oldValue ="old value here";
chrome.storage.synch.set(url1,oldValue);

var myData = new Data(1);
chrome.storage.onChanged.addListener(function(changes, namespace) {  //changes={key:value.old/new,...}, namespace ("sync", "local" or "managed")
	for(key in changes) {  
		myData.notifyObservers(key);  //key is url // or changes[key].oldValue or .newValue
	} 
});

var pageManager= new PageManager();
var myPage = new Page(div_id);
pageManager.registerView(url1,myPage);

myData.addObserver(pageManager);  //observer로 pageManager를 등록해 둔다 

//화면 그리기 시작 !!!! 
myData.getDataPromise(url1)
	.then(function(result){
			myPage.setAdapter(result);//myPage.setAdapter('hello hihi');
			myPage.draw();
	}).catch(function(error){
		console.log('Error occurred!', error);
	});

//데이터 변화와 화면 자동 업데이트 확인 TEST!  
var newValue ="newvalue added";
var jsonfile = {};
jsonfile[url1] = JSON.stringify(newValue);
chrome.storage.synch.set(jsonfile,function(){console.log("new data updated");});

//화면에서 사라지면  observers에서도 제거
pageManager.unregisterView(myPage);
//myPage도 없앤다? 
document.

</script>
	
	