/**
 * 
 */

function DataService(){
	var observers=[];
	var observerurls=[];
	var syncurls=[];
	var lastSyncTime,syncInterval,syncMinTime=20000, syncMaxTime=30*60*1000;
	
	this.addObserver=function(){
		//TODO: 중복 확인 this.observers.forEach();//???이미 있으면 추가안해도 됨.
		this.observers.push(observer);
	};
	this.removeObserver=function(id){
		var index = this.observers.indexOf(observer);
	      if (~index) {
	        this.observers.splice(index, 1);  //if(~index){this.views.splice(index,1); this.view_urls.splice(index,1);}
	      }
	};
	this.notifyObservers=function(url){
		var data=getData(url)
		for (var i = this.observers.length - 1; i >= 0; i--) {
	        this.observers[i].update(url);
	      };
	      
	    //this.data.update(url);//초기화init_data할때 PageManager 객체를 넣으면, addObserver/remove빼고, 이것만으로 호출가능함  
	
		//for-loop
			observer.update(url,data);
	};
	
	function getLocal(url){
		promise=new Promise();
		return promise;
	};
	function setLocal(url,data){
		promise=new Promise();
		return promise;
	};

	
	
	this.getDataPromise=function(key){
		//chrome.storage.sync.get(key,callback);
		return new Promise(function(resolve,reject){
			chrome.storage.sync.get(key,function(result){
				if(result!=null) resolve(result);
				else reject(result);
			});
		});
	};
	
	this.setDataPromise=function(mkey,mvalue){
		return new Promise(function(resolve,reject){
			var key = mkey,value = JSON.stringify(mvalue);
			var jsonfile = {};
	    	jsonfile[key] = value;
			chrome.storage.sync.set(jsonfile,function(result){
				if(result!=null) resolve(result);
				else reject(result);
			});
		});
	};
	
	function performSync(){
		//for-loop with syncUrls
		//notifyObservers
	}
	
}

angular.module('app').service('todoStorage', function ($q) {
    var _this = this;
    this.data = [];
    this.findAll = function(callback) {
        chrome.storage.sync.get('todo', function(keys) {
            if (keys.todo != null) {
                _this.data = keys.todo;
                for (var i=0; i<_this.data.length; i++) {
                    _this.data[i]['id'] = i + 1;
                }
                console.log(_this.data);
                callback(_this.data);
            }
        });
    }
    this.sync = function() {
        chrome.storage.sync.set({todo: this.data}, function() {
            console.log('Data is stored in Chrome storage');
        });
    }
    this.add = function (newContent) {
        var id = this.data.length + 1;
        var todo = {
            id: id,
            content: newContent,
            completed: false,
            createdAt: new Date()
        };
        this.data.push(todo);
        this.sync();
    }
    this.remove = function(todo) {
        this.data.splice(this.data.indexOf(todo), 1);
        this.sync();
    }
    this.removeAll = function() {
        this.data = [];
        this.sync();
    }
});



////////
/*
 *   라이브러리로 외부 파일로 만들어서 가져 올 부분임 
 * 
 */
function DataService(mode){
	this.mode=mode; 
	this.ob_id=0;
	this.observers=[];
	addObserver:function(observer){
		//this.observers.forEach();//???이미 있으면 추가안해도 됨.
		this.observers.push(observer);
		ob_id++;
		return id;
	},
	
	removeObserver:function(observer){
		//객체를 찾아서 제거해도되고, id로 찾아서 제거할수도 있게 하자
		if(observer instanceof 'Integer'){ }
		
		var index = this.observers.indexOf(observer);
	      if (~index) {
	        this.observers.splice(index, 1);
	      }
	},
	
	notifyObservers(){
		for (var i = this.observers.length - 1; i >= 0; i--) {
	        this.observers[i].update();
	      };
	},
	
	getUrlPage:function(url,obj){  //obj hasOwnProperty('setDataAdapter') or instanceof ViewObject
		//url =>key_url로 변환 
		if(typeof obj.setDataAdapter !=== 'function') return;  //'undefined', //아니면 이렇게 테스트해본다 if(obj.hasOwnProperty('setDataAdapter')), if(obj instanceof ViewObject)
		chrome.storage.synch.get(url,function(response){  //=>function(response){}
			obj.setDataAdapter(response);
		});
		if(mode==2){
			//서버에서 새로운 데이터를 가져와 봐서 비교해보고 필요시 다시 저장하여 알림. 
		}
	}
} 


function ViewObject(pageType,url,div_id){
	this.div_id=div_id;
	this.div=document.getElementById(div_id);
	this.url=url;
	this.dataSet;
	this.pageType=pageType;
	
	function setDataAdapter(response){
		if(pageType=='notePage'){
			dataSet="aaabbb"; //convert response to dataSet that you want
			makePage(dataSet); // or super.makePage(dataSet);  //이것이 동작할까? 
		}
	}	
	function makePage(dataSet){
		//console.log("Hihi, I am now displayed"); //실제로 화면을 그리는 객체임
		$('div_x').innerHtml(dataSet); //div.innerHtml(dataSet);
		
	}
	
	function clear(){
		$('div_x').innerHtml("");  //div.innerHtml("");
	}
	
	function update(){  //observer용 인터페이스임 		 
		Data.getUrlPage(this.url,super);  //something ="asvdvsdvs"; //????다시 데이터를 가져와서 그린다
		super.setDataAdapter(something);
	}
} 

/* main 실행 */ 

//초기화 
url1="test.json";
var oldValue ="old value here";
chrome.storage.synch.set(url1,oldValue);
var Data = new DataService(1); 
var NotePage = new ViewObject('notePage');
chrome.storage.onChanged.addListener(function(changes, namespace) {
	Data.notifyObservers();
	/*
	for (key in changes) {
		var storageChange = changes[key];  //storageChange.oldValue, storageChange.newValue
	}
	*/
});

//화면 그리기 시작 
Data.getUrlPage(url1,NotePage); 

//데이터 변화와 화면 자동 업데이트 확인  
var newValue ="newvalue added";
chrome.storage.synch.set(url1,newValue);

//화면에서 사라지면  observers에서도 제거
Data.removeObserver(NotePage);
NotePage.clear();