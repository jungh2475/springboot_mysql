/**
 * 
 * 
 	local chrome.storage (key, {})
 		- content_[id], {type=urlPage or datePage, url, title, .....}
 		- comment_[id], {bodytext, collectionids,keywordids, createdWhen, last..}
 		- inventory_[id], {type=keyword or collection, title,.....}
 		- user_[id],{type=user or userGroup, groupmember:}
 */


function DataService(){
	
	var observers=[];
	var observerurls=[];
	var syncurls=[];
	
	this.addObserver=function(observer){
		this.observers.push(observer);
		//return the id number
	};
	
	this.removeObserver=function(id){
		
	};
	
	this.notifyObservers=function(url,data){
		
	};
	
	/////////////// 내장 기본 함수들 ////////////
	
	/*
		{Local with Cache}
		- getBy(keyString, properties, values) ://getCommentsByUrl, getCollectionsByUser, getKeywordsByUser, getById-comment,collection,keyword, getUsersByUser (user<->userGroup) 
			lastAccessTime, hitCount 값들을 업데이트한다  
		- setBy(key,value)
			각각의 용기가 차있는지보고, 순위가 낮은것을 지우고 저장한다 
		{Server}
		- get
		- set
	*/
	function setLocalByTypeAndId(type,id, object){
		var key=type+"_"+id;
		//if unique keyName->create(post) otherwise overwrite(update)
		chrome.storage.sync.set(key,object);
	}
	
	//TODO: 진행중  cache관리 기능을 추가하자 
	function getLocalByKeyAndIds(keyhead,ids){ //keyTypeHead 이름이 매칭되는 객체들을 다 가져온다 
		//ex: inventory_78, collection_23, keyword_44, comment_146 :=> typeName+"_"+[unique_id]
		//만약 id=empty or null이면 그 타입전체를 가져 오게 된다 
		var promise= new Promise(function(resolve,reject){
			resultList=[];
			//if null -> return all			
			chrome.storage.sync.get(null,function(storageAll){//전체 저장소 가져오기 
				if(ids.length==0) {//if-else로 분기하자 ,//1) ids empty : 전체 목록 찾아 오기
					var allKeys=Object.keys(storageAll); 
					for (key in allKeys) {
						if(-1<key.indexOf(keyhead+"_")){  //keyheadname matched!
							resultList.push(storageAll[key]); //이렇게 비동기로도 동작하나? 
						}
					}
				} else { //2) ids not empty : 해당 id들의 객체 value들 가져오기
					for (x in ids){
						key=keyhead+"_"+x;
						resultList.push(storageAll[key]);
					}
				}
				resolve(resultList);  //Promise.all(promiseList).then(function(items){resolve(items);});  //promiseList안의 결과들이 다 확정되면 값을 반환하려면 promise.all사용해야함 
				if(resultList.length==0){reject(null);}  //Promise.reject(null);
			});
		});
		return promise;
	}
	
	
/////////////// 외부 view 서비스용 함수들 ////////////
	
	function getCommentsByUrl(url,option){  //option 0이면 그냥 객체값으로만, 1이면 inventory(collection,keyword)에 text값을 넣어서 전달함 
		
		var promise=new Promise(function(resolve,reject){
			results=[];
			inventory_ids=[];
			inventorys=[];
			getLocalByKeyAndIds("comment",null)  //모든 comments 추출  
				.then(function(comments){
					for(comment in comments){
						if(-1<comment.url.indexOf(url)){    // 해당 url 가진 코멘트만 추출하여 push 
							results.push(comment);
						}					
					}
					if(option==0) resolve(results);  //Promise.resolve(resultList)?  결과값 반환 
				});  //TODO: implement .catch();
			if(option==1){ //add+fill inventories_name, 1)comments에서 id추출  2)inventory객체들 가져오기 3)값을 넣음  
				  for (comment in results){
					  for (x in comment.collectionids){
						  getLocalByKeyAndIds("collection",x.id).then(function(object){comment.collectionnames.push(object.name);});
					  }   //TODO: 이 부분도id따로 nameString 따로하지 말고 같이 묶어서 유사객체 배열로 저장해 둘까? 
					  for (x in comment.keywordids){
						  getLocalByKeyAndIds("keyword",x.id).then(function(object){comment.keywordnames.push(object.name);});
					  }
				  }
			}
			
		});
		return promise;
	}
	
	
	
	//function getCollectionsByUser(user_id) =getByKeyAndUser("collection",user_id)
	//function getServerCollectionsByUser(user_id)
	//function getCollectionsByIds(ids,option)=getLocalByKeyAndIds("inventory",ids)
		
	
	
}