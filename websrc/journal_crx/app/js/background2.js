//background2 (version 0.2) to load type  chrome://extensions/, make sure using manifest2.json

var toggle = true;

//google analytics 코드

//unique user_id set
var user_id;

/*  쓰지 않는 코드 임 
function getRandomToken() {
    // E.g. 8 * 32 = 256 bits token
    var randomPool = new Uint8Array(32);
    crypto.getRandomValues(randomPool);
    var hex = '';
    for (var i = 0; i < randomPool.length; ++i) {
        hex += randomPool[i].toString(16);
    }
    // E.g. db18458e2782b2b77e36769c569e263a53885a9944dd0a861e5064eac16f1a
    return hex;
}
*/

//https인 경우 조정이 필요, ga.js대신에 analytics.js는?

// Standard Google Universal Analytics code
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); // Note: https protocol here

 
ga('create', 'UA-73648835-1', 'auto');
ga('set', 'checkProtocolTask', function(){}); // Removes failing protocol check. @see: http://stackoverflow.com/a/22152353/1958200
ga('require', 'displayfeatures');
ga('send', 'pageview', '/comment.html');  //comment page, scrapbookpage일떄 마다 다르게 하고, comment일때는 어떤  url인지 표시 하자 /comment.html/{url} 

console.log("ga is ready & fired!");

/* official tutorial by google using ga.js (old version)을 쓰지 않음 그러나, id button click은 사용함
   https://developer.chrome.com/extensions/tut_analytics
*/

function trackEvent(e) {
    var input;
    //버튼 클릭인 경우 
    input = {method:'event', id:e.target.id, action:'clicked'};
    gaTracking(input);
  };

function gaTracking(input){
    //msg를 받아서 여기에 넣어 주면 된다. comment+title, sub_url(comment/sub_url)
    //if ga not ready 아래의 command들은 queue에 들어가 있다가 나중에 loading 되면 이로갈 fire 될것임 
    switch (input.method) {
                case 'hitTopPage':
                     ga("set","title",input.content_title);
                     ga("send","pageView", input.content_url);
                     break;
                case 'hitSubPage':
                     ga("set","title","comment"+input.content_title);
                     ga("send","pageView", input.content_url);
                     break;
                case 'event':
                     //
                    // ga('send', 'event', 'Button', 'click', 'label_id');
                    // ga('send', 'event', 'Link', 'click', 'url...');
                    // ga('send', 'event', 'Video', 'play', 'cats.mp4'); //eventCategory,eventAction, eventLabel
                     break;
                case 'error':
                     ga('send', 'exception', {'exDescription': input.content_title, 'exFatal': false});
                     break;
                case 'setPage':
                     ga("set","field","value");
                     break;
                case 'setUserId':
                     ga(function(tracker) {
                        user_id = tracker.get('clientId');
                        chrome.storage.sync.set({userid: user_id}, function() {});
                     });
            //get UserId ->chrome.storage.sync.get('userid', function(items) {var userid = items.userid;}
                     //다른 userId를 사용하고 싶으면 login/auth뒤에 다음 코드를 넣어서 ga로 보낸다
                     ga('set', 'userId', user_id); // Set the user ID using signed-in user_id.
                     //please see custom dimention hit case 1
                     ga('set', 'dimension1', 'dimensionValue');
                     break;
                case 'test':  //실험용 샘플 코드임
                     ga('send', 'pageview', '/comment3.html'); 
                     console.log("ga fired:/comment3.html");
                     break;
                default:
                     alert("ga: nothing matched");
            
             }
             
    return true;
}

//sidebar를 보여주기 위해서, 관련 script를 loading하고, 메시지를 보낸다
function showSidebar(tab){
    //읽어보세요 https://developer.chrome.com/extensions/content_scripts#pi
    //https://developer.chrome.com/extensions/manifest/web_accessible_resources
    chrome.browserAction.setIcon({ path: "img/icon19.png"});
    console.log("scripts injecting");
    chrome.tabs.executeScript(tab.id, {file: 'js/jquery-2.1.4.min.js'}, function(){
        console.log("jquery script injected");
    }); //jQuery injection
    chrome.tabs.insertCSS(tab.id, {file: 'css/bootstrap.min.css'}, function(){}); //css injection
    chrome.tabs.executeScript(tab.id, {file: 'js/bootstrap.min.js'}, function(){});
    
    
    chrome.tabs.executeScript(tab.id, {file: 'js/inject2.js'}, function(){
        chrome.tabs.sendMessage(tab.id, { method:'showSidebar'}, function(response){
            console.log(response); //이 response를 받아서 원하는 작업을 여기서 처리한다 
            return true;
        });
    });
    
    toggle = !toggle;
    return true;
}

//sidebar를 숨기고 제거하면서, 관련 script도 unloading 하면 좋겠다. 
function hideSidebar(tab){
    chrome.browserAction.setIcon({ path: "img/moleskine_icon_128.png"});
    chrome.tabs.sendMessage(tab.id, { method:'hideSidebar'}, function(response){
            console.log(response); //이 response를 받아서 원하는 작업을 여기서 처리한다 
            return true;
        });
    toggle = !toggle;
    return true;
}


function checkData(){
    //url이 바뀐것이면, 새로 다른 데이터를 load하여, 이 페이지 관련 자료가 있는지 확인하고
    //있으면 icon, badge를 변형하고, 없으면 default값으로 돌려 놓는다. 
    
    //1.url이 바뀌었나?  activeUrl?
    //2.url이 바뀌었으면, 새 페이지 관련 데이터가 있는 지 확인 하라, 도 hideSidebar하고 철수한다 
    //(3.데이터가 local에 없으면 cloud에서 가져온다)
    //4.데이터가 있으면 icon, badge 모양을 있는 것으로 바꾸고, 없으면 없는 것으로 바꾼다.
    //
    console.log("checking url changes...");
    
    //무언가 검사하고 이것을 호출하면 된다 ....
    
    //이 tab에 자료가 있으면 그 값을 적어 둔다 
    
    chrome.browserAction.setBadgeText({text:"4"});
    chrome.browserAction.setBadgeText({text:""});  //이렇게 하면 badge가 사라진다 , 자료가 없을땐0 이걸쓸것 
    chrome.browserAction.setBadgeBackgroundColor({color:[255,0,0,255]});
    
    

}

//when activeTab changed - check if data available
//무언가 상황이 바뀐것이므로, 데이터 변동이 있는지 확인해 보고, 적절한 하위작업을 수행한다
chrome.tabs.onUpdated.addListener(checkData);
chrome.tabs.onCreated.addListener(checkData);
chrome.tabs.onActivated.addListener(checkData);



//listener for msg from content script, msgType안의 문자열로 구분하여 처리한다 
// 1. msgType='simple' icon, badge, toggle값등을 바꾸어 준다
// 2. msgType='restApi' 외부 ajax reques - 밖에서 데이터를 가져오고, 다되면 observer에서 notify한다 
// 3. 
chrome.runtime.onMessage.addListener(function(msg,sender,sendResponse){
    switch(msg.msgType){
        case 'ajx':
            //load(restapi)......
            break;
        default:
            console.log("nothing matched in bgScript");
            break;
    }
});

// 리스너의 반대로 어떤 메시지를 contentScript에 보내려면 
// chrome.tabs.query{ active:true, currentWindow:true },function(tabs){ 
//      -> chrome.tabs.sendMessage(tabs[0].id, object, responseCallback)..로 보낸다 
                                     
//click하면 시작하는 main function임
chrome.browserAction.onClicked.addListener(function(tab){
    
    
    if(toggle){
        //chrome.browserAction.setIcon();  //아이콘 모양을 바꾼다 
        showSidebar(tab);
        
        //google analytics test code
        gaTracking({method:'test'});
        
        
    } else{
        hideSidebar(tab);
    }
    
    
});



/*
  injecting script http://stackoverflow.com/questions/9515704/building-a-chrome-extension-inject-code-in-a-page-using-a-content-script 

*/