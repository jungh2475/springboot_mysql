//background.js injects scripts according to instruction
//default popup.html이 json에 선언되면 onClicked가 호출 안됨-주의 할것 (아니면 popuphtml에서 버튼에서 호출 하던가)

var activeUrl = '';
var sitesCached;  //url, timeCreated, 
var toggle = false;
var userId;
var password;
var access_token;
var refresh_token;
var gSheetUrl;

function showSidebar(tab){
    
    console.log("showSidebar");
    //file을 inject하고 callback function 호출- 여기서 메시지를 보냄 
    //여기서 bootsrap,jQuery script, css도 inject해야하는 것이 아닌지? 고민...
    chrome.tabs.executeScript(tab.id, { file: 'js/inject.js' }, function() {
        //method에 호출하고 싶은 function 이름을 넣는다 
        chrome.tabs.sendMessage(tab.id, {method:'showSidebar'},function(response){
            //response.data를 뽑아내서 사용하고,
            return true;
        });
    });
    
    return true;  //이것이 있어야지 listener가 계속 async에도 살아 있어 callback이 실행될수 있게 된다네요
}

function hideSidebar(tab){
    
    //hide하라고 메시지를 보낸다 
    console.log("hideSidebar");
    return true;
}


function checkData() {
    //alert("tab changed"); tabUrl이 바뀌었는지 체크해보고, 달라지면, 이 주소의 저장값을 클라우드에서 가져다가 놓는다. max 20개정도로 저장 공간을 제약을 둔다. 그 이상이 되면 siteCached에 저장했던 tabUrl들을 제거한다?
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        if(activeUrl != tabs[0].url){
            activeUrl = tabs[0].url;
            //siteCached에 자료가 없으면 클라우드에서 자료를 가지고 온다. 도착하면 viewUpdate를 해준다 
            if(sitesCached.indexOf(activeUrl)==-1) {
                //get Data from cloud
            }
        } else { return; }
    });
    
    //만약에 저장된것이 있으면 아이콘 모양을 바꾸던가, badge에 기록 갯수를 표시 한다 
    //load from storage.sync(local)
    
    
}

//현재 activeTab으로 msg보낸다. msg={key:value}, response.txt or response only
function sendMessageTab(msg){
//function sendMessgae(msg,mCallback)
// var callback, if(callback==''){....}
    chrome.tabs.query({ active:true, currentWindow:true },function(tabs){
        chrome.tabs.sendMessage(tabs[0].id, msg,function(response){
            console.log(response);  //callback
        });
    });
}

//외부로 연결해서 자료를 받아서 storage에 저장함. request안에 action="xhttp" for cross-origin, method="GET/POST", url(꼭있어야), data (POST형식일때 있어야 함)
//testing url : http://130.211.153.135:8080/ or http://130.211.153.135:8080/get-by-email?email=admin@kewtea.com
function xSend(request,sender,callback){
    if(request.action=="xhttp"){  //전체 기본 체크 사항
        var xhttp=new XMLHttpRequest();
        var method=request.method ? request.method.toUpperCase() : 'GET';
        xhttp.onload=function(){
            callback(xhttp.responseText);
        }
        xhttp.onerror=function(){
            alert("xhttp error");
            callback();
        }
        xhttp.open(method,request.url,true);
        if(method=='POST'){
            xhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        }
        xhttp.send(request.data);
        return true; //prevents the callback from being called too early on return
    }
}



// When the browser action is clicked, manifest.json에 popup.html이 정의되어져 있으면 이 리스너가 동작 하지 않음 (주의할것!)
chrome.browserAction.onClicked.addListener(function(tab){
    console.log("clicked!");
    toggle = !toggle;  //한번 누르면 showSidebar, 다시 누르면 hideSidebar
    if(toggle){
        //chrome.browserAction.setIcon();
        showSidebar(tab);
    } else{
        hideSidebar(tab);
    }
});

//showSidebar

//when activeTab changed - check if data available
chrome.tabs.onUpdated.addListener(checkData);
chrome.tabs.onCreated.addListener(checkData);
chrome.tabs.onActivated.addListener(checkData);

chrome.runtime.onMessage.addListener(function(msg,sender,sendResponse){
    switch(msg.actionType){
        //contentScript에서 못해ㅓ 대신 처리해 주는 기타 요청 사항들
            
        //restapi로 데이터 가져다 주기, 
        case '':
            xSend(a,b,c);
            break;
        default:
            alert("nothing matched in bgScript");
    }
});