//inject2.js



var injected = injected || (function(){
    
    var methods = {}; 
    var toggle = false;
    
    
    methods.showSidebar = function (){
        
        toggle=true;
        //test code
        console.log("I am now called within inject contentscript");
        document.body.appendChild(document.createElement('div'));
        $("div").append("<b>Appended text!!!!</b>");
        console.log("injectd text"+$("#status").text());
        //debugger;
        
        return true;
    };
    
    methods.hideSidebar = function (){
        
        
        toggle=false;
        //debugger;
        return false;
        
    };
    
    //1. 저장은 chrome.storage.sync.get/set
    //2. 주소는 window.location.protocol+'//'+window.location.host+'/'+window.location.pathname
    //window.location.href 
    //chrome.extension.getUrl();
    //(만약에 hostname is '', ////삼중복을 방지해야 할 것임. port는 무시해도 될듯함)
    // also check - window.history.state, chrome.tabs.url from backgroudn script
    //3.  img inject 하면 src는 
    //  var imgURL = chrome.extension.getURL("images/myimage.png");
    //  document.getElementById("someImage").src = imgURL;
    
    
    
    //listener 
    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse){
            var data = {};
             switch (request.method) {
                case 'showSidebar':
                     data = methods['showSidebar']();
                     break;
                case 'hideSidebar':
                     data = methods['hideSidebar']();
                     break;
                default:
                     alert("nothing matched");
            
             }
            
        // Send the response back to our extension. callback function    
        sendResponse({ data: data });
        return true;
    });
    
    //messge sender to background script 
    //msgSend={ msgType:'ajx'}  //여기에  msgType을 잘 적어서 bgscript에서 맞는 function이 호출되도록 함
    //chrome.runtime.sendMessage(msgSend,callback);
    
    
    //adding debugger to access this script (Debugger Tool F12이 열려 있으면 보이게 됨)
    //들여다보길 원하는 코드자리에 아래의 것을 놓고, view -> developer -> developer console을 열어두면 break처럼 멈춤    
    //debugger;
    
    
})();