//inject.js

var toggle=false;
var sidebar;

var injected = injected || (function () {
    
    var methods = {};
    
    var myUrl;
    var myComments;
    
    methods.init = function () {
        
        myUrl = document.location.href;
        //document.location.host   domain
        //document.location.path   path
        //document.location.hash   #
        console.log("tab_inited:"+myUrl);
        return 0;
    };
    
    methods.showSidebar = function () {
        //show or hide sidebar
        
        if(!toggle){  
            //2. inject div & animate showup
        console.log("I am now called");
        var t = document.createTextNode("This is a paragraph.");  
        sidebar = document.body.appendChild(t);
        
        document.body.appendChild(document.createElement('div'));
        
        //callback으로 넣어 주어야지 재대로 동작할것임.진짜로 loading후에 동작 시켜야 함 
        $("div").append("<b>Appended text</b>");
        $("p").append(" <b>Appended text2</b>.");
        
            
        } else{
            //remove
            //getParent
            //document.removeChild();
            //sidebar
        }
        
        
        
        //return data
        return 0;
    };
    
    
    methods.reloadData = function () {
        
        chrome.storage.sync.get({
            url: myUrl,
            comments:'d'
        },function(items){
            myComments=items.comments;
        });
        
    };
    
    methods.saveData = function () {
        //저장버튼을 누르면, 먼져 storage에 저장된것과, 현재 메모리에 있는 것을 비교하고, 차이가 있으면 그때만 storage에 저장하고, 메시지를 보내서 클라우드로도 저장하게 한다 
        chrome.storage.sync.get({},function(items){
            //compare
            //만약에 차이가 있으면 그때 저장하고, 아니면 그대로 둔다 
        });
        chrome.storage.sync.set({
           url:myUrl,
           comments:myComments 
        },function(){
            //observer패턴으로 데이터가 바뀌었다고 연락한다 
        });
    };
    
    //listener 
    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            var data = {};
            
            //switch by request.method
            switch (request.method) {
                case 'showSidebar':
                    //loadscript(methods.showSidebar);
                    console.log("request.method:"+request.method);
                    data = methods['init']();
                    data = methods['showSidebar']();
                    data = methods['reloadData']();
                    break;
                case 'reloadData':
                    data = methods['reloadData']();
                    break;
                default:
                    alert("nothing matched");
            }    
            
            // Send the response back to our extension.
            sendResponse({ data: data });
            return true;
        });
    
    return true;
})();

function sendMessage(msg){
//sendMessage(mMsg, mCallback)    
    var msgSend;
    var callback = function (responseText) { 
            console.log(responseText);
    };
    
    switch(msg.type){
    case 'textMsg':
        msgSend={msg:msg.text};
        break;
    case 'restapi':
        msgSend={
            method: 'POST',
            action: 'xhttp',
            url: 'http://www.stackoverflow.com/search',
            data: 'q=something'
        };
        break;
    default:
        alert("nothing matched");
    }
    
    if(msgSend!='') {
        chrome.runtime.sendMessage(msgSend,callback);    
    }
    
}

/* 여기는 동작을 안해서 그냥 이렇게 저장만 해둔다. 현재는manifect.json에서 강제로 넣고 있음 아니면showSidebar같은 background init function에서 초기화때에 jquery, bootstrap등을 inject하게 해주는 것이 좋을 것 같음 
function loadscript(callback){
    var s = document.createElement('script');
    s.src = chrome.extension.getURL('js/jquery-2.1.4.min.js');
    s.type="text/javascript";
    s.onload=function(){
        console.log("callback is called");
        callback();
        //http://stackoverflow.com/questions/23597323/why-is-the-script-tag-removed-in-javascript-injection-from-a-google-chrome-exten, async로 즉시 리턴되므로 삭제해서 깔끔하게 해도 좋다
        //this.parentNode.removeChild(this);         
    };
    (document.head || document.documentElement).appendChild(s);
}

function test(){
    $("div").append("<b>Appended text</b>");
    $("p").append(" <b>Appended text2</b>.");   
}
*/

//아니면 밖의 fuction을 호출 할수가 있다. 대신에 돌려 보낼 데이터를 넣을 수가 없다?