/**
 * http://usejsdoc.org/
 * 
 * will be compiled within bundle.js or appview.js
 * 
 * node slidecontent.js
 * 
 */


function appmain(){
	
	var viewer1;
	var viewer2;
	var viewer3;
	
	var path='./scripts/';
	
	var order=[1,2,3];
	var divName=['div1','div2','div3'];
	
	//외부에서 호출할 click function들 나열 
	
	this.moveNext = function (){
		var actionNum = order.shift(); //맨처음것을 return하고 array에서 제거함
		order.push(actionNum);
		var content=this.loadContent();
		this.loadView(actionNum,content);
		//slide animate
		this.start(order[0]); //현재 맨앞의 것이 animation 시작한다?
	}
	this.movePrev = function(){}  //현재는 사용 계획이 없다  
	this.clear = function(){}
	
	
	//내부에서 사용할 함수들 ....
	this.loadContent = function(){
		
		//소스에서 값을 가져옮 - 이부분은 나중에 ...dataService에서 가져 오는 부분임  
		content={a:"abc", b:"bbb", c:"cccc"};
		return content;
	}
	
	this.loadView = function(view,content){  //divname,data
		var viewer;
		switch(view) {
			case 1:
				viewer=viewer1;				
				break;
			case 2:
				viewer=viewer2;
				break;
			case 3:
				viewer=viewer3;
				break;
			default:
				console.log("wrong inputs!!!");
				return -1;
				break;
		} 
		
		viewer=require(path+content.script);
		viewer.setViewFrame(divName[view]); //이것이 맞는 표현인가? 
		viewer.load(content.data);
		
		return 1;
		
	}
	
	
	this.start = function(viewer){
		switch(viewer){
			case 1:
				viewer1.start();
				break
			case 2:
				viewer2.start();
				break;
			case 3:
				viewer3.start();
				break;
			default:
				//TO-DO
				break;
		}
		return 1;
	}
	
	
}


var app= appmain();

app.init("helloSample");
