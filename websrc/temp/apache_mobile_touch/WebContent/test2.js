/**
 * 
 */

var app=angular.module('myApp', []);
app.controller('myAppController',['$scope', function($scope){
	$scope.yourName="Jungh Lee";
}]);

window.onmouseover=function(e) {
    console.log(e.target.className);
};

function add(x,y){
	return x+y;
}

var mouseDown = false, right;
var xi, xf, leftX = 0;
var nPages = $(".kew_page").size();
var pageSize = $(".kew_page").width();
var threshold = pageSize/2;
var currentPage = 0;

$(".kew_page").on("click", function (e) {
	console.log("cliked:"+e.target.className);
});

$(".kew_page").on("mousedown", function (e) {
    console.log("mouse_down");
	mouseDown = true;
    xi = e.pageX;
});

$(".kew_page").on("mouseup", function (e) {
	console.log("mouse_up");
	if (mouseDown) {
        mouseDown = false;
        xf = e.kew_pageX;
        leftX = parseInt($(".kew_viewpager").css("left").split("px")[0]);
        if ((e.kew_pageX - xi) < -threshold || (e.kew_pageX - xi) > threshold) {
            setFocusedPage();
        } else {
            restore();
        }
    }
});

$(".kew_page").on("mouseleave", function (e) {
	console.log("mouse_leave");
	if (mouseDown) {
        mouseDown = false;
        xf = e.pageX;
        leftX = parseInt($(".kew_page").css("left").split("px")[0]);
        if ((e.pageX - xi) < -threshold || (e.pageX - xi) > threshold) {
            setFocusedPage();
        } else {
            restore();
        }
    }
});

$(".kew_page").on("mousemove", function (e) {
	console.log("mouse_move");
	console.log("moving(leftX, e.pageX,xi):"+leftX+","+e.pageX+","+xi);
	if (mouseDown) {
        $(".kew_page").css({
            "left": (leftX + (e.pageX - xi))
        });
        right = ((e.pageX - xi) < 0) ? true : false;
    }
});

function restore() {
    $(".kew_viewpager").stop().animate({
        "left": -(currentPage * pageSize)
    }, 200, function () {
        leftX = parseInt($(".kew_viewpager").css("left").split("px")[0]);
    });
}

function setFocusedPage() {
    if (leftX >= (-threshold)) { // First Page
        currentPage = 0;
    } else if (leftX < (-threshold) && leftX >= (-(nPages + 1) * threshold)) { // Second to N-1 Page
        (right) ? currentPage++ : currentPage--;
    } else if (leftX < -((nPages + 1) * threshold)) { // Third Page
        currentPage = nPages - 1;
    }
    $(".kew_viewpager").stop().animate({
        "left": -(currentPage * pageSize)
    }, 200, function () {
        leftX = parseInt($(".kew_viewpager").css("left").split("px")[0]);
    });
}