// see also https://script.google.com/a/kewtea.com/d/1iAHyhz9QHXDaCPbI8x_BJ8DPypqWefT-Iod69wElOWaGvFR_VoH0g8Qx/edit?usp=drive_web&folder=0BwMUQ54Dp45KUFVVVWNncVZFdXM&splash=yes
// 1. check Urls & if exists send email alert
// file>manage versions: get new version id, (예전 버젼을 지우고), 버젼 업데이트를 publish with new version id 해야만 적용이 된다. go menu Resources -> set Triggers (everyday (9:20 am))

//file location drive>webProjects/app01, path = https://script.google.com/d/1kK0w9CHFLIL4czO2XzlJT_zElc06wSVdwlMdSyA5dS1zcP1yJd8laTFv/edit?usp=sharing


function createAndSendReport(msgs){
  //msgs가 array로 여러개의 api link들이 들어 있다
  
  var now = new Date();
  
  //var id = 'abc';
  var emails=["jungh.lee@kewtea.com","jungh.leei@gmail.com"];  //'admin@kewtea.com',
  var subject="report of apitesting ["+now.toString()+"]";
  var body;
  
  body="failed urls["+msgs.length+"]"+"\n"+msgs.toString();
  
  
  //try catch
  /* var doc = DocumentApp.openById(id); //DocumentApp.create(subject);
  doc.getBody().appendParagraph(body);
  var url = doc.getUrl();
  
  body = 'Link to your doc: ' + url;
  */
  
  for(x in emails){
    GmailApp.sendEmail(emails[x], subject, body);  
  }
  
  
}

function checkUrls(){
  // input urls then returns urls failed
  // 
  
  var urlsFailed=[];
  var urls=["http://146.148.32.67:8080/test/get-by-email?email=admin@kewtea.com",
            "http://www.kewrator.com",
            "http://www.kewtea.com",
            "http://spirithouse34.com",
            "http://130.211.153.135:8080/get-by-email?email=jungh.lee@kewtea.com",
            "http://stritti.github.io/log4js/" ];
  
  var failCount=0;
  
  for(x in urls){
    Logger.log("url:"+ urls[x]);
    if(failCount>=2){
      Logger.log("too many failure so we exit");
      return urlsFailed;
    }
    try{
      
      //여기에 응답 시간을 측정하는 코드를 넣어둠, 나중에 활용하도록
      var start = new Date().getTime();  //or .getTime();
      var response = UrlFetchApp.fetch(urls[x]);
      var end = new Date().getTime();
      var diff = end - start; //difference in milliseconds
      Logger.log("responseTime:"+diff);
      var responseNumber = response.getResponseCode();
      // 나중에 내용물에도 문제 없는지 체크해 보려면 response.getContentText()
      // read JSON and write to sheet https://gist.github.com/mhawksey/1442370 
      Logger.log(responseNumber);
    } catch(err){
      responseNumber=501;
      Logger.log(err);
    }
    if(parseInt(responseNumber) != 200){
      urlsFailed.push(urls[x]);
      failCount++;
    }
  }
  
  Logger.log("failedUrls["+urlsFailed.length+"]");
  return urlsFailed;

}


function myFunction(){    
  var urlsFailed = checkUrls();
  Logger.log("i am called1");
  if(urlsFailed.length !=0){
    Logger.log("i am called2");
    createAndSendReport(urlsFailed);
  }
  return ContentService.createTextOutput('The Api checking service completed: error['+urlsFailed.length+']');
}



function doGet() { // <- if called via web app
  //createAndSendDocument();    // run this function that exists already
  return myFunction();
  //return ContentService.createTextOutput('Hello, world!');
}



