
// https://developers.google.com/apps-script/guides/rest/quickstart/target-script

/**
 * Return the set of folder names contained in the user's root folder as an
 * object (with folder IDs as keys).
 * @return {Object} A set of folder names keyed by folder ID.
 */
function getFoldersUnderRoot() {
  var root = DriveApp.getRootFolder();
  var folders = root.getFolders();
  var folderSet = {};
  while (folders.hasNext()) {
    var folder = folders.next();
    folderSet[folder.getId()] = folder.getName();
  }
  return folderSet;
}

function createAndSendDocument() {
  // Create a new Google Doc named 'Hello, world!'
  var doc = DocumentApp.create('Hello, world!');

  // Access the body of the document, then add a paragraph.
  doc.getBody().appendParagraph('This document was created by Google Apps Script.');

  // Get the URL of the document.
  var url = doc.getUrl();

  // Get the email address of the active user - that's you.
  var email = Session.getActiveUser().getEmail();

  // Get the name of the document to use as an email subject line.
  var subject = doc.getName();

  // Append a new string to the "url" variable to use as an email body.
  var body = 'Link to your doc: ' + url;

  // Send yourself an email with a link to the document.
  GmailApp.sendEmail(email, subject, body);
}

function checkMyMail() {
  
  var query='jungh';
  var messages = GmailApp.search(query,0,20)
  Logger.log('msg num:'+messages.length);
  
}

/**
 * Return a list of sheet names in the Spreadsheet with the given ID.
 * @param {String} a Spreadsheet ID.
 * @return {Array} A list of sheet names.
 */
function getSheetNames(sheetId) {
  var ss = SpreadsheetApp.openById(sheetId);
  var sheets = ss.getSheets();
  return sheets.map(function(sheet) {
    return sheet.getName();
  });
}


function myFunction(){
    
  return ContentService.createTextOutput('Hello, world!');
}

function doGet(){ // <- if called via web app
  //createAndSendDocument();    // run this function that exists already
  myFunction();  
}




/**
 * 
 * https://developers.google.com/apps-script/guides/sheets/functions
 * 
 * Show the title and date for the first page of posts on the Google Apps
 * Developer blog.
 *
 * @return Two columns of data representing posts on the Google Apps
 *     Developer blog.
 * @customfunction
 * gSheet 셀에다가 아래의 =getBlogPosts() 넣는다 
 */
function getBlogPosts() {
  var array = [];
  var url = 'http://googleappsdeveloper.blogspot.com/atom.xml';
  var xml = UrlFetchApp.fetch(url).getContentText();
  var document = XmlService.parse(xml);
  var root = document.getRootElement();
  var atom = XmlService.getNamespace('http://www.w3.org/2005/Atom');
  var entries = document.getRootElement().getChildren('entry', atom);
  for (var i = 0; i < entries.length; i++) {
    var title = entries[i].getChild('title', atom).getText();
    var date = entries[i].getChild('published', atom).getValue();
    array.push([title, date]);
  }
  return array;
}
  
