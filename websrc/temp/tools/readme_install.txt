
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew update
brew upgrade node
brew install node -g

#spring elasticsearch는 2.4까지만 현재(17.02.15)지원되므로, 이경우에는 brew switch로 하위버젼을 골라서 쓰도록 한다 

brew tap homebrew/versions   //homebrew-versions
#brew versions elasticsearch
brew search elasticsearch
brew install elasticsearch@2.4
brew install homebrew/versions/elasticsearch24
brew unlink elasticsearch
brew install elasticsearch
brew switch elasticsearch 2.4

brew services list
brew tap homebrew/services

elasticsearch error on 5.x  => 다음 파일을 copy 할것 
cp /usr/local/etc/elasticsearch/elasticsearch.yml.default /usr/local/etc/elasticsearch/elasticsearch.yml

############################### node 개발 환경 
package.json
var express = require("express");  //외부 모듈 가져 오기 
module.exports = userRepository;   //만든 funciton 을 모듈로 등록 하기 
var userRepository = require("userRepository.js");
var userRepositoryInstance = new userRepository();  



############################# webpack은 nodejs>npm을 brew를 설치하지 말고 설치하는 것이 안정적임 .
http://blog.teamtreehouse.com/26017-2



############ webpack : http://blog.teamtreehouse.com/26017-2
https://webpack.github.io/docs/tutorials/getting-started/

npm install webpack -g  //brew와 npm을 섞어쓰면 잠재적인 위험이 있다 -
brew install node then webpack
https://gist.github.com/DanHerbert/9520689
npm update npm -g


######## compile at Ubuntu(debian linux)  필요 없음? 주로 맥에서 개발하니?

//sudo apt-get install npm
//sudo npm install -g grunt-cli 


npm init -y
npm install angular webpack --save-dev


############# webpack (webpack.config.js)  그 디렉토리로 이동 ########
module.exports = {
    entry: "./entry.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};

######### 한개 파일 합치기 app.bundle.js  #########
const path = require('path');
const webpack = require('webpack');
module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    app: ['./home.js', './events.js', './vendor.js'],
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
  },
};

####### 여러개의 파일 그러나 각각안에서 import가 합쳐져는 있음 
const path = require('path');
const webpack = require('webpack');
module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    home: './home.js',
    events: './events.js',
    contact: './contact.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
  },
};

############## loader: "babel-loader?", "style-loader!css-loader"  ###########

module: {
	loaders: [
		{
			test: /\.js[x]?$/,
			exclude: /node_modules/ ,
			loaders: ["babel-loader?" + babelPresets.map((preset) => `presets[]=${preset}`).join("&")]
		},
		{
			test: /.css$/,
			loader: "style-loader!css-loader"
		}
	]
},


################# grunt task runner : 
https://nasireee.wordpress.com/2015/01/04/how-to-concatenate-and-minify-multiple-css-and-js-files-using-grunt/


폴더를 만들고, 이동해서 아래의 두개 파일을 만든다 
touch package.json Gruntfile.js
npm init
Installing Grunt plug-in: npm install grunt --save-dev, npm install grunt-contrib-concat --save-dev, 
npm install grunt-contrib-uglify --save-dev, npm install grunt-contrib-cssmin --save-dev
npm install grunt-contrib-jasmine --save-dev

package.json
{
  "name": "project name",
  "version": "0.0.0",
  "description": "",
  "main": "index.js",
  "dependencies": {
    "grunt": "~0.4.5",
    "grunt-cli": "~0.1.13",
    "grunt-contrib-concat": "~0.5.0"
  },
  "devDependencies": {
  	"grunt": "~0.4.1",
  	"grunt-contrib-sass": "~0.3.0",
    "grunt-contrib-watch": "~0.4.4"
    "grunt-contrib-jasmine": "~"
    "grunt-contrib-concat": "~0.5.0",
    "grunt-contrib-uglify": "~0.7.0",
    "grunt-contrib-cssmin": "~0.11.0"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": ""
  },
  "author": "Nasir Uddin",
  "license": "BSD-2-Clause"
}


Gruntfile.js(task runner)
/*
 * grunt-cli
 * http://gruntjs.com/
 *
 * Copyright (c) 2012 Tyler Kellen, contributors
 * Licensed under the MIT license.
 * https://github.com/gruntjs/grunt-init/blob/master/LICENSE-MIT
 */
 
'use strict';
 
 
module.exports = function(grunt) {
 
    grunt.initConfig({
    	jasmine : {
                       src : 'src/**/*.js',
                       options : {
                               specs : 'specs/**/*.js'
                       }
               },
        watch: {
            files: '**/*.js',
            tasks: ['jasmine']
        },
        sass: {
			dist: {
				files: {
					'style/style.css' : 'sass/style.scss'
				}
			}
		},
        concat: {
            css: {
                src: ['assets/css/bootstrap.min.css', 'assets/css/bootstrap-responsive.css', 'assets/css/styles.css'],
                dest: 'assets/css/main.css'
              },
 
            js: {
                src: ['assets/js/libs/bootstrap/bootstrap-dropdown.js', 'assets/js/libs/bootstrap/bootstrap-transition.js', 
                'assets/js/libs/bootstrap/bootstrap-alert.js', 'assets/js/libs/bootstrap/bootstrap-collapse.js', 
                'assets/js/libs/bootstrap/bootstrap-modal.js', 'assets/js/libs/bootstrap/bootstrap-tab.js', 
                'assets/js/script.js' ],
                dest: 'assets/js/main.js'
              }
            },
        uglify: {
            js: {
                src: 'assets/js/main.js',
                dest: 'assets/js/main.min.js'
                }
            },
        cssmin: {
            css: {
                src: 'assets/css/main.css',
                dest: 'assets/css/main.min.css'
                }
            }
        });
 
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('build', ['jasmine','watch','concat', 'uglify', 'cssmin']);
    //or grunt.registerTask('default', ['jasmine','watch','concat', 'uglify', 'cssmin']);
};

grunt dev, grunt build, grunt (default) .....grunt.registerTask('name'

grunt concat	
grunt uglify
grunt cssmin
grunt build
grunt watch
grunt jasmine

npm install grunt-contrib-watch --save-dev  //파일변화를  watch하여 실행 

1. jasmine unit testing : Install grunt-contrib-jasmine : npm install grunt-contrib-jasmine --save-dev
  => 테스트만 하려면 grunt jasmine
2. build style.css from scss_es  : or sass --update scss:css
3. build bundle.js using webpack

############ scss to css

https://github.com/sindresorhus/grunt-sass

npm install --save-dev grunt-sass

require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

grunt.initConfig({
    sass: {
        options: {
            sourceMap: true
        },
        dist: {
            files: {
                'main.css': 'main.scss'
            }
        }
    }
});

grunt.registerTask('default', ['sass']);

